# Create your models here.
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django_extensions.db.models import TimeStampedModel

from panel.accounts.models import User

class Notification(TimeStampedModel):
    user = models.ForeignKey(User,related_name='user_notify',verbose_name='Usuario', null=True, on_delete=models.CASCADE)
    subject = models.CharField(max_length=200,verbose_name='Asuont',blank=True)
    message = models.CharField(max_length=200,verbose_name='Mensaje',blank=True)
    is_seen = models.BooleanField(verbose_name='Visto', default=False)

    class Meta:
        db_table = 'notifications'
        verbose_name = 'Notificación'
        verbose_name_plural = 'Notificaciones'

    def __unicode__(self):
        return self.user.get_full_name