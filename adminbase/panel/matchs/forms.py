from django import forms

from .models import Match, Note, NOTE_OPTIONS
from panel.commodities.models import Commodity
from panel.equipments.models import Equipment
from panel.customers.models import Customer

from cities.models import City, Region

class MatchForm(forms.ModelForm):

	commodity = forms.ModelChoiceField(
		label='Mercancia',
		error_messages = {'required' : 'Debe capturar la mercancia'},
		queryset = Commodity.objects.filter(status=True),
		empty_label = 'Seleccione la mercancia...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	equipment = forms.ModelChoiceField(
		label='Equipamiento',
		error_messages = {'required' : 'Debe capturar el equipamiento'},
		queryset = Equipment.objects.filter(status=True),
		empty_label = 'Seleccione el equipo...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	customer = forms.ModelChoiceField(
		label='Cliente',
		error_messages = {'required' : 'Debe capturar el cliente'},
		queryset = Customer.objects.filter(status=True),
		empty_label = 'Seleccione al cliente...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	source_path = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_path = forms.ModelChoiceField(
		label='Ciudad destino',
		error_messages = {'required' : 'Debe capturar la ruta destino'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	source_region = forms.ModelChoiceField(
		label='Estado origen',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_region = forms.ModelChoiceField(
		label='Estado destino',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	class Meta:
		model = Match
		fields = ('commodity', 'equipment', 'customer', 'source_path', 'target_path')

class NoteForm(forms.ModelForm):
	options = forms.ChoiceField(
        label = "Observaciones",
        error_messages = {"required": "Debe capturar el tipo de observacion"},
        choices = NOTE_OPTIONS,
        widget = forms.Select(attrs={"class": "form-control",}),
    )
	note = forms.CharField(
        label="Comentarios",
        required = False,
        widget=forms.Textarea(attrs={"rows": 4, "class": "form-control",}),
    )

	class Meta:
		model = Note
		fields = ('note', 'options')

