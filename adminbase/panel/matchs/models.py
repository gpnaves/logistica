from __future__ import unicode_literals

from panel.equipments.models import Equipment
from panel.commodities.models import Commodity
from panel.employees.models import Employee
from panel.accounts.models import User
from panel.carriers.models import Carriers
from panel.customers.models import Customer
from panel.notifications.models import Notification

from cities.models import City

from django.db import models
from django_extensions.db.models import TimeStampedModel
# Create your models here.

match_status = [
    ('proccess', 'En proceso'),
    ('completed', 'Completado'),
    ('canceled', 'Cancelado'),
]

NOTE_OPTIONS = [
    ('no equipment', 'No tiene equipo'),
    ('paid problem', 'Problemas de pago'),
    ('disponibility check', 'Revisa disponibilidad'),
    ('no route', 'No maneja esa ruta'),
    ('put equipment', 'Coloca equipo'),
    ('other', 'Otros')
]
class Match(TimeStampedModel):
    commodity = models.ForeignKey(Commodity, related_name='commodity_match', verbose_name='Mercancia', on_delete=models.DO_NOTHING)
    equipment = models.ForeignKey(Equipment, related_name='equipment_match', verbose_name='Equipo', on_delete=models.DO_NOTHING)
    customer = models.ForeignKey(Customer, related_name='customer_match', verbose_name='Cliente', on_delete=models.DO_NOTHING)
    source_path = models.ForeignKey(City, related_name='source_path_match', verbose_name='Ruta origen', max_length=100, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name='target_path_match', verbose_name='Ruta destino', max_length=100, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, related_name = 'user_match', verbose_name = 'Usuario', on_delete=models.DO_NOTHING)
    progress = models.PositiveSmallIntegerField(verbose_name='Progreso', default=0)
    
    status = models.BooleanField(verbose_name='status', null=True)

    class Meta:
        db_table = 'matchs'
        verbose_name = 'Match'
        verbose_name_plural = 'Matchs'

    # def __str__(self):
    #     return self.title

class Note(TimeStampedModel):
    match = models.ForeignKey(Match, related_name='match_note', verbose_name='Nota', on_delete=models.CASCADE)
    carrier = models.ForeignKey(Carriers, related_name='carrier_note', verbose_name='Transportista', on_delete=models.CASCADE)
    note = models.CharField(verbose_name='Nota', max_length=1000, null=True)
    options = models.CharField(max_length=100, choices=NOTE_OPTIONS, default='', verbose_name='Opciones')
    mail = models.ForeignKey(Notification, related_name='mail_match', verbose_name='Mensaje', null=True, on_delete=models.DO_NOTHING)
    
    class Meta:
        db_table = 'match_notes'
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'
