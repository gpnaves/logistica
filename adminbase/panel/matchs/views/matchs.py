# -*- coding: utf-8 -*-

from ..models import Match, Note
from ..forms import MatchForm, NoteForm

from panel.core.forms import MailForm
from panel.carriers.models import Carriers, InterestRoutes
from panel.routes.models import Route

from cities.models import City

from django.db.models import Sum, Count
from django.db.models.functions import TruncMonth, TruncYear

from django.core.exceptions import MultipleObjectsReturned 

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item, sendmail
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from django.contrib.gis.measure import D
from panel.core.decorators import access_for_user, has_permissions

import datetime

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def match_list(request):
	context = {}
	match_list = []

	matchs = Match.objects.filter(user=request.user)
	for match in matchs:
		dicc = {}
		dicc['match_detail'] = match
		dicc['match_notes'] = Note.objects.filter(match_id = match.pk).order_by('created')
		match_list.append(dicc)

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['match_list'] = match_list
	return render(request, 'matchs/match_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def match_add(request):
	context = {}

	if request.method == 'POST':
		context['match_form'] = MatchForm(request.POST)

		if context['match_form'].is_valid():
			match_obj = context['match_form'].save(commit=False)
			match_obj.user = request.user
			match_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Match creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Match creado satisfactorimente')
			
			return redirect('matchs:match_detail',str(match_obj.pk))
	else:
		context['match_form'] = MatchForm()
	
	return render(request, 'matchs/match_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)	
def match_edit(request, pk):
	context = {}

	context['match_obj'] = get_object_or_404(Match, pk=pk)

	if request.method == 'POST':
		context['match_form'] = MatchForm(request.POST,request.FILES, instance=context['match_obj'])

		if context['match_form'].is_valid():
			match_obj = context['match_form'].save(commit=False)	
			match_obj.save()

			msg = messages.success(request, 'Match modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Match modificado satisfactorimente')

			return redirect('matchs:match_list')
	else:
		context['match_form'] = MatchForm(instance=context['match_obj'], \
			initial={
				'source_region': context['match_obj'].source_path.region_id, 
				'target_region': context['match_obj'].target_path.region_id})

	return render(request, 'matchs/match_form.html', context)

@login_required
def match_detail(request, pk):
	context = {}
	match_obj = get_object_or_404(Match, pk=pk)

	interest_routes = InterestRoutes.objects.filter(source_path=match_obj.source_path, target_path=match_obj.target_path)
	# --

	# Obtiene los transportistas que hallan hecho esa ruta
	routes = []
	routes_filter = Route.objects.filter(source = match_obj.source_path, target = match_obj.target_path).values('carrier', 'source', 'target').annotate(travels=Count('*'))
	for item in routes_filter:
		dicc = {}
		try:
			carrier = Carriers.objects.get(pk=item['carrier'])
		
			dicc['pk'] = carrier.pk
			dicc['business_name'] = carrier.business_name
			dicc['alias'] = carrier.alias
			dicc['source_path'] = City.objects.get(pk=item['source'])
			dicc['target_path'] = City.objects.get(pk=item['target'])
			dicc['travels'] = item['travels']
			dicc['city'] = carrier.city
			dicc['region'] = carrier.region
			dicc['contact'] = carrier.operative_contact
			routes.append(dicc)
		except MultipleObjectsReturned:
			pass
	# --



	# Obtiene los transportistas con una ruta cercana 
	carriers_filter = []
	routes_filter = []

	near_cities_target = City.objects.filter(
					location__distance_lte=(match_obj.target_path.location, D(km=100))).exclude(id=match_obj.target_path.id).order_by('name')
	near_cities_source = City.objects.filter(
					location__distance_lte=(match_obj.source_path.location, D(km=100))).exclude(id=match_obj.source_path.id).order_by('name')

	for near_city in near_cities_target:
		result = InterestRoutes.objects.filter(
						source_path=match_obj.source_path.id, 
						target_path=near_city.id)

		result2 = Route.objects.filter(
						source=match_obj.source_path.id,
						target=near_city.id) \
					.values(
						'carrier__alias', 
						'source', 
						'target') \
					.annotate(travels=(Count('*')))

		if result:
			carriers_filter.append(result)
		if result2:
			routes_filter.append(result2)
	
	for near_city in near_cities_source:
		result = InterestRoutes.objects.filter(
						source_path=near_city.id, 
						target_path=match_obj.target_path.id)
		
		result2 = Route.objects.filter(
						source=near_city.id,
						target=match_obj.target_path.id) \
					.values(
						'carrier__alias', 
						'source', 
						'target') \
					.annotate(
						travels=(Count('*')))
		if result:
			carriers_filter.append(result)
		if result2:
			routes_filter.append(result2)
	# --

	# -- Estas variables son las que iran en el contexto. Se busca obtener el id del transportista
	near_carriers = []
	near_routes = []

	if carriers_filter:
		for items in carriers_filter:
			for item in items:
				try:
					carrier = Carriers.objects.get(pk=item.carrier.pk)
					dic = {}
					dic['carrier'] = carrier
					dic['source_path'] = City.objects.get(pk=item.source_path.pk)
					dic['target_path'] = City.objects.get(pk=item.target_path.pk)
					dic['contact'] = carrier.operative_contact
					near_carriers.append(dic)
				except MultipleObjectsReturned:
					pass
	if routes_filter:
		for items in routes_filter:
			for item in items:
				try:
					carrier = Carriers.objects.get(alias=item['carrier__alias'])
					dic = {}
					dic['pk'] = carrier.pk
					dic['alias'] = item['carrier__alias']
					dic['source_path'] = City.objects.get(pk=item['source'])
					dic['target_path'] = City.objects.get(pk=item['target'])
					dic['travels'] = item['travels']
					dic['contact'] = carrier.operative_contact
					near_routes.append(dic)
				except MultipleObjectsReturned:
					pass
		
	# --

	context['match_obj'] = match_obj
	context['carrier_list'] = interest_routes
	context['routes_list'] = routes
	context['near_carriers'] = near_carriers
	context['near_routes'] = near_routes

	return render(request, 'matchs/match_detail.html', context)

@login_required
def match_note(request, pk, carrier_pk):
	context = {}
	data = dict()

	match = get_object_or_404(Match, pk=pk)
	carrier = get_object_or_404(Carriers, pk=carrier_pk)

	context['note_form'] = NoteForm(request.POST or None)

	if request.is_ajax and request.method == 'POST':

		if context['note_form'].is_valid():
			note_obj = context['note_form'].save(commit=False)
			note_obj.match = match
			note_obj.carrier = carrier
			note_obj.save()

			if match.progress < 90:
					match.progress = match.progress + 10
					match.save()
					
			messages.success(request, 'Nota registrada')

			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/matchs/detail/' + str(pk)
		else:
			data['form_is_valid'] = False

	data['html_form'] = render_to_string('matchs/match_notes.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
def match_sendmail(request, pk, carrier_pk):
	context = {}
	data = dict()

	match = get_object_or_404(Match, pk=pk)
	carrier = get_object_or_404(Carriers, pk=carrier_pk)

	context["from_mail"] = request.user.email
	context["to_mail"] = carrier.operative_contact.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)

			if carrier.user:
				mail_obj.user = carrier.user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				note = Note()
				note.match = match
				note.carrier = carrier
				note.mail = mail_obj
				note.save()
				

				if match.progress < 90:
					match.progress = match.progress + 10
					match.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/matchs/detail/' + str(pk)
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
@has_permissions('can_delete_' + app_name)
def match_delete(request, pk):
	match = get_object_or_404(Match, pk=pk)

	data = delete_record(request, match, '/panel/matchs/', 'matchs:match_delete')
	return JsonResponse(data)