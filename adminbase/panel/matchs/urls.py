from django.conf.urls import url
from .views import matchs

urlpatterns = [
    url(r'^$', matchs.match_list, name='match_list'),
	url(r'^add/$', matchs.match_add, name='match_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', matchs.match_edit, name='match_edit'),
	url(r'^detail/(?P<pk>[0-9]+)/$', matchs.match_detail, name='match_detail'),
	url(r'^delete/(?P<pk>[0-9]+)/$', matchs.match_delete, name='match_delete'),
	url(r'^note/(?P<pk>[0-9]+)/carrier/(?P<carrier_pk>[0-9]+)/$', matchs.match_note, name='match_note'),
	url(r'^sendmail/(?P<pk>[0-9]+)/carrier/(?P<carrier_pk>[0-9]+)/$', matchs.match_sendmail, name='match_sendmail'),
]