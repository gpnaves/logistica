# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
	#-- List Panel Data
    url(r'^data/$', views.panel_list, name='panel_list'),
	#-- Add Panel Data
    url(r'^data/add/$', views.panel_add, name='panel_add'),
	#-- Edit Panel Data
    url(r'^data/edit/(?P<pk>[0-9]+)/$', views.panel_edit, name='panel_edit'),

	#-- List Menu Items
    url(r'^menu/$', views.menu_list, name='menu_list'),
	#-- Add Menu Item
    url(r'^menu/add/$', views.menu_add, name='menu_add'),
	#-- Edit Menu Item
    url(r'^menu/edit/(?P<pk>[0-9]+)/$', views.menu_edit, name='menu_edit'),
    #-- Delete Menu Item
    url(r'^menu/delete/(?P<pk>[0-9]+)/$', views.menu_delete, name='menu_delete'),

	#-- List SubMenu Items
    url(r'^submenu/$', views.submenu_list, name='submenu_list'),
	#-- Add SubMenu Item
    url(r'^submenu/add/$', views.submenu_add, name='submenu_add'),
	#-- Edit SubMenu Item
    url(r'^submenu/edit/(?P<pk>[0-9]+)/$', views.submenu_edit, name='submenu_edit'),
    #-- Delete SubMenu Item
    url(r'^submenu/delete/(?P<pk>[0-9]+)/$', views.submenu_delete, name='submenu_delete'),

	#-- List Sessions
    url(r'^account/sessions/$', views.sessions_list, name='sessions_list'),
    url(r'^account/sessions/(?P<pk>\w+)/delete/$', views.sessions_delete, name='sessions_delete'),
]
