# -*- coding: utf-8 -*-
from django.shortcuts import render,redirect,get_object_or_404
from django.template.loader import render_to_string
from django.contrib import messages
from django.http import JsonResponse
from django.db.models import F

from panel.core.decorators import access_for_superuser
from panel.accounts.models import UserRole, User
from panel.core.utils import user_logs,delete_record
from panel.config.forms import MenuItemForm, MenuSubItemForm
from panel.config.models import MenuItems, MenuSubItems

# Create your views here.
@access_for_superuser
def menu_list(request):
    context = {}

    context['menu_list'] = MenuItems.objects.filter(status=True).order_by('title')

    return render(request,'config/menu_list.html',context)

@access_for_superuser
def menu_add(request):
    context = {}

    if request.method == 'POST':
        context['menu_form'] = MenuItemForm(request.POST)
        
        if context['menu_form'].is_valid():
            context['menu_form'].save()
			
            #-- Message to user
            messages.success(request, 'Menú creado satisfactorimente')

            #-- User Logs (Info, Access, Error)
            user_logs(request,None,'I','Menú creado satisfactoriamente')

            return redirect('config:menu_list')
    else:
        context['menu_form'] = MenuItemForm()

    return render(request, 'config/menu_form.html', context)

@access_for_superuser
def menu_edit(request,pk):
    context = {}

    context['menu_obj'] = get_object_or_404(MenuItems, pk=pk)

    if request.method == 'POST':
        context['menu_form'] = MenuItemForm(request.POST, instance=context['menu_obj'])
        
        if context['menu_form'].is_valid():
            context['menu_form'].save()
			
            #-- Message to user
            messages.success(request, 'Menú modificado satisfactorimente')

            #-- User Logs (Info, Access, Error)
            user_logs(request,None,'I','Menú modificado satisfactoriamente')

            return redirect('config:menu_list')
    else:

        context['menu_form'] = MenuItemForm(instance=context['menu_obj'])

    return render(request, 'config/menu_form.html', context)

@access_for_superuser
def menu_delete(request,pk):

	menu = get_object_or_404(MenuItems, pk=pk)

	data = delete_record(request,menu,'/panel/config/menu/','config:menu_delete')

	#-- Delete Submenu Items
	if data:
		MenuSubItems.objects.filter(item=menu).update(status=False)

		#-- Delete Module´s User
		User.delete_modules_user(menu)

	#-- User Logs (Info, Access, Error)
	user_logs(request,None,'I','Registro eliminado satisfactorimente')

	return JsonResponse(data)	

# Create your views here.
@access_for_superuser
def submenu_list(request):
    context = {}

    context['submenu_list'] = MenuSubItems.objects.filter(status=True).order_by('item__title','title')

    return render(request,'config/submenu_list.html',context)

@access_for_superuser
def submenu_add(request):
    context = {}

    if request.method == 'POST':
        context['submenu_form'] = MenuSubItemForm(request.POST)
        
        if context['submenu_form'].is_valid():
            context['submenu_form'].save()
			
            #-- Message to user
            messages.success(request, 'submenu creado satisfactorimente')

            #-- User Logs (Info, Access, Error)
            user_logs(request,None,'I','submenu creado satisfactoriamente')

            return redirect('config:submenu_list')
    else:
        context['submenu_form'] = MenuSubItemForm()

    return render(request, 'config/submenu_form.html', context)

@access_for_superuser
def submenu_edit(request,pk):
    context = {}

    context['submenu_obj'] = get_object_or_404(MenuSubItems, pk=pk)

    if request.method == 'POST':
        context['submenu_form'] = MenuSubItemForm(request.POST, instance=context['submenu_obj'])
        
        if context['submenu_form'].is_valid():
            context['submenu_form'].save()
			
            #-- Message to user
            messages.success(request, 'submenu modificado satisfactorimente')

            #-- User Logs (Info, Access, Error)
            user_logs(request,None,'I','submenu modificado satisfactoriamente')

            return redirect('config:submenu_list')
    else:

        context['submenu_form'] = MenuSubItemForm(instance=context['submenu_obj'])

    return render(request, 'config/submenu_form.html', context)

@access_for_superuser
def submenu_delete(request,pk):

	submenu = get_object_or_404(MenuSubItems, pk=pk)

	data = delete_record(request,submenu,'/panel/config/submenu/','config:submenu_delete')

	#-- Delete Submenu Items
	if data:
		#-- Delete SubModule´s User
		User.delete_submodules_user(submenu)

	#-- User Logs (Info, Access, Error)
	user_logs(request,None,'I','submenu eliminado satisfactorimente')

	return JsonResponse(data)	