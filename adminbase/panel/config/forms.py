# -*- coding: utf-8 -*-
from django import forms

from panel.config.models import PanelAdmin, MenuItems, MenuSubItems

class PanelAdminForm(forms.ModelForm):
     title = forms.CharField(
        label = 'Título',
        error_messages = {'required':'Debe capturar el título'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     url = forms.URLField(
        label = 'URL',
        required = False,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     logo = forms.ImageField(
        label = 'Logo Login',
        required = False,
        widget = forms.FileInput()
     )
     logo_menu = forms.ImageField(
        label = 'Logo Menú',
        required = False,
        widget = forms.FileInput()
     )
     author = forms.CharField(
        label = 'Autor',
        required = False,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )

     class Meta:
         model = PanelAdmin
         fields = ('title','url','logo','logo_menu','author')

class MenuItemForm(forms.ModelForm):
     app_label = forms.CharField(
        label = 'App Label',
        error_messages = {'required':'Debe capturar el label'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     title = forms.CharField(
        label = 'Título',
        error_messages = {'required':'Debe capturar el título'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     module = forms.CharField(
        label = 'Módulo',
        required = False,
        error_messages = {'required':'Debe capturar el nombre del módulo'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     url = forms.CharField(
        label = 'URL',
        required = False,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     icon = forms.CharField(
        label = 'Icono',
        error_messages = {'required':'Debe capturar el ícono'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     order = forms.CharField(
        label = 'Orden',
        error_messages = {'required':'Debe capturar el orden'},
        widget = forms.TextInput(
            attrs = {
                'class': 'touchspin',
            }
        )
     )
     has_submenu = forms.BooleanField(
        label = 'Con Submenu',
        required = False,
        widget = forms.CheckboxInput(
            attrs = {
				'class':'js-switch'
            }
        )
    )


     class Meta:
         model = MenuItems
         fields = ('app_label','title','module','url','icon','order','has_submenu')

class MenuSubItemForm(forms.ModelForm):
     item = forms.ModelChoiceField(
        label = 'Menú',
        queryset = MenuItems.objects.filter(status=True,has_submenu=True),
        empty_label = 'Seleccione...',
        error_messages = {'required':'Debe seleccionar el Menú'},
        widget = forms.Select(
            attrs = {
                'class': 'form-control select2'
            }
        )
     )
     title = forms.CharField(
        label = 'Título',
        error_messages = {'required':'Debe capturar el título'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     module = forms.CharField(
        label = 'Módulo',
        error_messages = {'required':'Debe capturar el nombre del módulo'},
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )
     url = forms.CharField(
        label = 'URL',
        required = False,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-control'
            }
        )
     )

     order = forms.CharField(
        label = 'Orden',
        error_messages = {'required':'Debe capturar el orden'},
        widget = forms.TextInput(
            attrs = {
                'class': 'touchspin',
            }
        )
     )

     class Meta:
         model = MenuSubItems
         fields = ('item','title','module','url','order')