from ..models import Freight
from ..forms import FreightForm

from django.db.models import Sum, Count
from django.db.models.functions import TruncMonth, TruncYear

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

import datetime

from panel.core.decorators import has_permissions

# Create your views here.

app_name = __package__.rsplit('.')[1]
@has_permissions('can_view_' + app_name)
def freight_list(request):
	context = {}

	today = datetime.date.today()
	data = {}
	if (Freight.objects.filter(user=request.user)):
		if Freight.objects.filter(user=request.user, created__month=today.month):
			data = {
				'earnings_month': Freight.objects.filter(user=request.user, created__month=today.month).values('user').annotate(month=TruncMonth('created')).values('month').annotate(earnings=Sum('cost'))[0],
				'count_month': Freight.objects.filter(user=request.user, created__month=today.month).values('user').annotate(month=TruncMonth('created')).values('month').annotate(earnings=Count('*'))[0],
				'earnings_year': Freight.objects.filter(user=request.user).values('user').annotate(year=TruncYear('created')).values('year').annotate(earnings=Sum('cost'))[0],
				'count_year': Freight.objects.filter(user=request.user).values('user').annotate(year=TruncYear('created')).values('year').annotate(earnings=Count('*'))[0],
			}
		else:
			data = {
				'earnings_year': Freight.objects.filter(user=request.user).values('user').annotate(year=TruncYear('created')).values('year').annotate(earnings=Sum('cost'))[0],
				'count_year': Freight.objects.filter(user=request.user).values('user').annotate(year=TruncYear('created')).values('year').annotate(earnings=Count('*'))[0],
			}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['freight_list'] = Freight.objects.filter(user=request.user, status=True).order_by('pk')
	context['freight_info'] = data

	return render(request, 'freights/freight_list.html', context)

@has_permissions('can_add_' + app_name)
def freight_add(request):
	context = {}

	if request.method == 'POST':
		context['freight_form'] = FreightForm(request.POST)

		if context['freight_form'].is_valid():
			freight_obj = context['freight_form'].save(commit=False)
			freight_obj.user = request.user
			freight_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Flete creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Flete creado satisfactorimente')

			return redirect('freight:freight_list')
	else:
		context['freight_form'] = FreightForm()
	
	return render(request, 'freights/freight_form.html', context)
	
@has_permissions('can_edit_' + app_name)
def freight_edit(request, pk):
	context = {}

	context['freight_obj'] = get_object_or_404(Freight, pk=pk)

	if request.method == 'POST':
		context['freight_form'] = FreightForm(request.POST,request.FILES, instance=context['freight_obj'])

		if context['freight_form'].is_valid():
			freight_obj = context['freight_form'].save(commit=False)	
			freight_obj.save()

			msg = messages.success(request, 'Flete modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Flete modificado satisfactorimente')

			return redirect('freight:freight_list')
	else:
		context['freight_form'] = FreightForm(instance=context['freight_obj'], \
			initial={
				'source_region': context['freight_obj'].source_path.region_id, 
				'target_region': context['freight_obj'].target_path.region_id})

	return render(request, 'freights/freight_form.html', context)

def freight_detail(request, pk):
	context = {}
	context['freight_obj'] = get_object_or_404(Freight, pk=pk)
	
	data = dict()
	data['html_form'] = render_to_string('freights/freight_detail.html', context, request=request)
	
	return JsonResponse(data)

@has_permissions('can_delete_' + app_name)
def freight_delete(request, pk):
	freight = get_object_or_404(Freight, pk=pk)

	data = delete_record(request, freight, '/panel/freight/', 'freight:freight_delete')
	return JsonResponse(data)