from django import forms

from .models import Freight
from panel.commodities.models import Commodity
from panel.equipments.models import Equipment
from panel.carriers.models import Carriers
from panel.customers.models import Customer

from cities.models import City, Region

class FreightForm(forms.ModelForm):
	title = forms.CharField(
		label = 'Título',
		error_messages = {'required': 'Debe capturar un título'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	note = forms.CharField(
		label='Observaciones',
        max_length=1000,
		required = False,
		widget = forms.Textarea(
			attrs = {
				'class' : 'form-control',
                'rows' : 4,
			}
        )
	)
	commodity = forms.ModelChoiceField(
		label='Mercancia',
		error_messages = {'required' : 'Debe capturar la mercancia'},
		queryset = Commodity.objects.filter(status=True),
		empty_label = 'Seleccione la mercancia...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	
	commodity_amount = forms.DecimalField(
		label = 'Cantidad de mercancia',
 		error_messages = {'required' : 'Debe capturar la cantidad de mercancia'},
 		widget = forms.TextInput(
			attrs = {
				'class':'form-control touchspin',
			}
		)
	)

	equipment = forms.ModelChoiceField(
		label='Equipamiento',
		error_messages = {'required' : 'Debe capturar el equipamiento'},
		queryset = Equipment.objects.filter(status=True),
		empty_label = 'Seleccione el equipo...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	carrier = forms.ModelChoiceField(
		label='Transportista',
		error_messages = {'required' : 'Debe capturar el transportista'},
		queryset = Carriers.objects.filter(status=True),
		empty_label = 'Seleccione al transportista...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	customer = forms.ModelChoiceField(
		label='Cliente',
		error_messages = {'required' : 'Debe capturar el cliente'},
		queryset = Customer.objects.filter(status=True),
		empty_label = 'Seleccione al cliente...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	source_path = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_path = forms.ModelChoiceField(
		label='Ciudad destino',
		error_messages = {'required' : 'Debe capturar la ruta destino'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	cost = forms.DecimalField(
		label = 'Costo',
 		required = False,
 		widget = forms.TextInput(
			attrs = {
				'class':'form-control touchspin',
			}
		)
	)

	source_region = forms.ModelChoiceField(
		label='Estado origen',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_region = forms.ModelChoiceField(
		label='Estado destino',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	class Meta:
		model = Freight
		fields = ('title', 'note', 'commodity', 'commodity_amount', 'equipment', 'carrier', \
			'customer', 'source_path', 'target_path', 'cost')
