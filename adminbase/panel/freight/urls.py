from django.conf.urls import url
from .views import freights

urlpatterns = [
    url(r'^$', freights.freight_list, name='freight_list'),
	url(r'^add/$', freights.freight_add, name='freight_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', freights.freight_edit, name='freight_edit'),
	url(r'^detail/(?P<pk>[0-9]+)/$', freights.freight_detail, name='freight_detail'),
	url(r'^delete/(?P<pk>[0-9]+)/$', freights.freight_delete, name='freight_delete'),
]