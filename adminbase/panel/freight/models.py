from __future__ import unicode_literals

from panel.equipments.models import Equipment
from panel.commodities.models import Commodity
from panel.employees.models import Employee
from panel.accounts.models import User
from panel.carriers.models import Carriers
from panel.customers.models import Customer

from cities.models import City

from django.db import models
from django_extensions.db.models import TimeStampedModel
# Create your models here.

class Freight(TimeStampedModel):
    title = models.CharField(verbose_name='Título', max_length=500)
    note = models.CharField(verbose_name='Título', max_length=1000)
    commodity = models.ForeignKey(Commodity, related_name="commodity_freight", verbose_name="Mercancia", on_delete=models.DO_NOTHING)
    commodity_amount = models.CharField(verbose_name='Razón Social', max_length=200)
    equipment = models.ForeignKey(Equipment, related_name="equipment_freight", verbose_name="Equipo", on_delete=models.DO_NOTHING)
    carrier = models.ForeignKey(Carriers, related_name="carrier_freight", verbose_name="Transportista", on_delete=models.DO_NOTHING)
    customer = models.ForeignKey(Customer, related_name="customer_freight", verbose_name="Cliente", on_delete=models.DO_NOTHING)
    source_path = models.ForeignKey(City, related_name="source_path_freight", verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name="target_path_freight", verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING)
    cost = models.DecimalField(max_digits=8, decimal_places=2)
    user = models.ForeignKey(User, related_name = 'user_freight', verbose_name = 'Usuario transportista', null=True, on_delete=models.DO_NOTHING)

    status = models.BooleanField(verbose_name="status", default=True)

    class Meta:
        db_table = 'freights'
        verbose_name = 'Flete'
        verbose_name_plural = 'Fletes'

    def __str__(self):
        return self.title
