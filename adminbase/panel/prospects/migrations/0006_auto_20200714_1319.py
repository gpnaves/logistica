# Generated by Django 3.0.7 on 2020-07-14 19:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('prospects', '0005_auto_20200713_1258'),
    ]

    operations = [
        migrations.RenameField(
            model_name='interestequipment',
            old_name='prospect',
            new_name='interest',
        ),
    ]
