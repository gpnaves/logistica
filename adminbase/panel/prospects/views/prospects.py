from django.shortcuts import render
from ..models import Prospect, Interest
from ..forms import ProspectForm
from ..views import interest

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def prospect_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['filter'] = request.GET.get('filter')
	context['prospect_list'] = Prospect.objects.filter(status=True).order_by('name')

	return render(request, 'prospects/prospect_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def prospect_add(request):
	context = {}

	if request.method == 'POST':
		context['prospect_form'] = ProspectForm(request.POST,request.FILES)

		if context['prospect_form'].is_valid():
			prospect_obj = context['prospect_form'].save(commit=False)
			prospect_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Prospecto creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Prospecto creado satisfactorimente')

			return redirect('prospects:prospect_list')
	else:
		context['prospect_form'] = ProspectForm()
	
	return render(request, 'prospects/prospect_form.html', context)
	
@login_required
@has_permissions('can_edit_' + app_name)
def prospect_edit(request, pk):
	context = {}

	context['prospect_obj'] = get_object_or_404(Prospect, pk=pk)
	
	try:
		obj = Interest.objects.get(prospect_id = context['prospect_obj'].pk)
		return redirect('prospects:interest_edit', str(obj.pk))
	except MultipleObjectsReturned:
		pass
	except ObjectDoesNotExist:
		pass

	if request.method == 'POST':
		context['prospect_form'] = ProspectForm(request.POST,request.FILES, instance=context['prospect_obj'])

		if context['prospect_form'].is_valid():
			prospect_obj = context['prospect_form'].save(commit=False)	
			prospect_obj.save()

			msg = messages.success(request, 'Prospecto modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Prospecto modificado satisfactorimente')

			return redirect('prospects:prospect_list')
	else:
		context['prospect_form'] = ProspectForm(instance=context['prospect_obj'])

	return render(request, 'prospects/prospect_form.html', context)

# def prospect_interest(request, pk):
# 	prospect = get_object_or_404(Prospect, pk=pk)
# 	data = interest.interest_confirm(request, prospect.pk, 'prospects:prospect_interest')
# 	return JsonResponse(data, safe=False)

def prospect_call(request, pk):
	pass

@login_required
@has_permissions('can_delete_' + app_name)
def prospect_delete(request, pk):
	prospect = get_object_or_404(Prospect, pk=pk)

	data = delete_record(request, prospect, '/panel/prospects/', 'prospects:prospect_delete')
	return JsonResponse(data)