from django.shortcuts import render
from ..models import Prospect, Interest, InterestEquipment, InterestRoutes
from ..forms import InterestForm, ProspectForm, InterestConfirmForm, InterestEquipmentForm, InterestRoutesForm
from panel.core.forms import MailForm
from panel.carriers.forms import CarrierForm, EquipmentForm

from panel.contacts.models import Contact
from panel.contacts.forms import ContactForm

from panel.equipments.models import Equipment
from panel.accounts.models import User

from django.core.cache import cache
from django.conf import settings
from django.apps import apps
from django.forms import formset_factory
from django.db.models import Sum
from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item, sendmail
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse, Http404

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def interest_list(request):
	pass 

# @login_required
# @has_permissions('can_add_' + app_name)
# def interest_add(request, pk):
# 	context = {}

# 	context['interest_obj'] = get_object_or_404(Interest, pk=pk)
# 	context['prospect_obj'] = context['interest_obj'].prospect

# 	equipment_formset = formset_factory(InterestEquipmentForm)

# 	if request.method == 'POST':
# 		context['interest_form'] = InterestForm(request.POST, request.FILES, instance=context['interest_obj'])
# 		context['prospect_form'] = ProspectForm(request.POST, request.FILES, instance=context['prospect_obj'])
# 		context['equipment_form'] = equipment_formset(request.POST)

# 		if context['interest_form'].is_valid():
# 			interest_obj = context['interest_form'].save()
# 			prospect_obj = context['prospect_form'].save(commit=False)

# 			if interest_validate(interest_obj):
# 				approve_mail(request, interest_obj)
# 				prospect_obj.prospect_status = 'wa'

# 			prospect_obj.save()

# 			for equipment in context['equipment_form']:
# 				if equipment.has_changed():
# 					equipment_obj = equipment.save(commit=False)
# 					equipment_obj.interest = interest_obj
# 					equipment_obj.save()

# 			#-- Message to beneficiary
# 			msg = messages.success(request, 'Interesado creado satisfactorimente')

# 			#-- User Logs (Info, Access, Error)
# 			user_logs(request,None,'I','Interesado creado satisfactorimente')

# 			return redirect('prospects:prospect_list')

# 	else:
# 		context['prospect_form'] = ProspectForm(instance=context['prospect_obj'])
# 		context['interest_form'] = InterestForm(instance=context['interest_obj'])
# 		context['equipment_form'] = equipment_formset()

# 	return render(request, 'prospects/interest_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def interest_edit(request, pk):
	
	context = {}
	context['interest_obj'] = get_object_or_404(Interest, pk=pk)
	context['prospect_obj'] = context['interest_obj'].prospect

	context['prefix_routes'] = 'routes'
	context['prefix_equipment'] = 'equipment'

	equipment_formset = formset_factory(InterestEquipmentForm)
	routes_formset = formset_factory(InterestRoutesForm)

	context['equipments'] = []
	equipments = InterestEquipment.objects.filter(interest_id=pk).values('equipment').annotate(quantity=Sum('quantity'))
	for item in equipments:
		dic = {}
		dic['equipment'] = get_object_or_404(InterestEquipment, interest_id=pk, equipment_id=item['equipment'])
		dic['quantity'] = item['quantity']
		context['equipments'].append(dic)
	context['interest_routes'] = InterestRoutes.objects.filter(interest_id=context['interest_obj'].pk)

	if request.method == 'POST':
		context['interest_form'] = InterestForm(request.POST, request.FILES, instance=context['interest_obj'])
		context['prospect_form'] = ProspectForm(request.POST, request.FILES, instance=context['prospect_obj'])
		context['equipment_form'] = equipment_formset(request.POST, prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(request.POST, prefix=context['prefix_routes'])

		if context['interest_form'].is_valid() and context['prospect_form'].is_valid():
			interest_obj = context['interest_form'].save(commit=False)
			interest_obj.save()

			prospect_obj = context['prospect_form'].save(commit=False)

			if interest_validate(interest_obj):
				approve_mail(request, interest_obj)
				prospect_obj.prospect_status = 'wa'
				

			prospect_obj.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.interest = interest_obj
					equipment_obj.save()

			for route in context['routes_form']:
				if route.has_changed():
					if route.is_valid():
						route_obj = route.save(commit=False)
						route_obj.interest = interest_obj
						route_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Interesado modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Interesado modificado satisfactorimente')

			return redirect('prospects:prospect_list')

	else:
		context['prospect_form'] = ProspectForm(instance=context['prospect_obj'])
		context['interest_form'] = InterestForm(instance=context['interest_obj'])
		context['equipment_form'] = equipment_formset(prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(prefix=context['prefix_routes'])

	return render(request, 'prospects/interest_form.html', context)

@login_required
def interest_documents(request, prospect_id):
	context = {}
	data = dict()

	interest_obj = get_object_or_404(Interest, prospect_id=prospect_id)

	documents = []
	documents_missing = []

	if interest_obj.person_type == 'legal':
		if interest_obj.constitutive_act:
			documents.append({ 'title': 'Acta constitutiva', 'file': interest_obj.constitutive_act, 'is_pdf': interest_obj.constitutive_act.url.endswith('.pdf') })
		else:
			documents_missing.append('Acta constitutiva')
		if interest_obj.power_attorney:
			documents.append({ 'title': 'Poder de los representantes', 'file': interest_obj.power_attorney, 'is_pdf': interest_obj.power_attorney.url.endswith('.pdf') })
		else:
			documents_missing.append('Poder de los representantes')
		if interest_obj.payment_authorization:
			documents.append({ 'title': 'Autorizacion de pago', 'file': interest_obj.payment_authorization, 'is_pdf': interest_obj.payment_authorization.url.endswith('.pdf') }) 
		else:
			documents_missing.append('Autorizacion de pago')

	if interest_obj.tax_number:
		documents.append({ 'title':'RFC', 'file': interest_obj.tax_number, 'is_pdf': interest_obj.tax_number.url.endswith('.pdf')	 })
	else:
		documents_missing.append('RFC')
	if interest_obj.oficial_id:
		documents.append({ 'title':'Identificacion Oficial', 'file': interest_obj.oficial_id, 'is_pdf': interest_obj.oficial_id.url.endswith('.pdf') })
	else:
		documents_missing.append('Identificacion Oficial')
	if interest_obj.transport_allowance:
		documents.append({ 'title': 'Permiso de transporte',	'file': interest_obj.transport_allowance, 'is_pdf': interest_obj.transport_allowance.url.endswith('.pdf') })
	else:
		documents_missing.append('Permiso de transporte')
	if interest_obj.proof_address:
		documents.append({ 'title': 'Comprobante de domicilio', 'file': interest_obj.proof_address, 'is_pdf': interest_obj.proof_address.url.endswith('.pdf') })
	else:
		documents_missing.append('Comprobante de domicilio')
	if interest_obj.e_billin_demo:
		documents.append({ 'title': 'Demo de factura electronica',	'file': interest_obj.e_billin_demo, 'is_pdf': interest_obj.e_billin_demo.url.endswith('.pdf') })
	else:
		documents_missing.append('Demo de factura electronica')
	if interest_obj.bank_statement:
		documents.append({ 'title': 'Estado de cuenta', 'file': interest_obj.bank_statement, 'is_pdf': interest_obj.bank_statement.url.endswith('.pdf') })
	else:
		documents_missing.append('Estado de cuenta')
	if interest_obj.unity_list:
		documents.append({ 'title': 'Relacion de unidades',	'file': interest_obj.unity_list, 'is_pdf': interest_obj.unity_list.url.endswith('.pdf') })
	else:
		documents_missing.append('Relacion de unidades')
	if interest_obj.contract:
		documents.append({ 'title': 'Contrato', 'file': interest_obj.contract, 'is_pdf': interest_obj.contract.url.endswith('.pdf') })
	else:
		documents_missing.append('Contrato')
	
	#-- Documentos para asignar viajes
	if interest_obj.driving_license:
		documents.append({ 'title': 'Licencia de conducir', 'file': interest_obj.driving_license, 'is_pdf': interest_obj.driving_license.url.endswith('.pdf') })
	else:
		documents_missing.append('Licencia de conducir')
	if interest_obj.insurance:
		documents.append({ 'title': 'Seguro de la unidad', 'file': interest_obj.insurance, 'is_pdf': interest_obj.insurance.url.endswith('.pdf') })
	else:
		documents_missing.append('Seguro de la unidad')
	if interest_obj.registration:
		documents.append({ 'title': 'Tarjeta de circulacion', 'file': interest_obj.registration, 'is_pdf': interest_obj.registration.url.endswith('.pdf') })
	else:
		documents_missing.append('Tarjeta de circulacion')
	if interest_obj.gps:
		documents.append({ 'title': 'GPS', 'file': interest_obj.gps, 'is_pdf': interest_obj.gps.url.endswith('.pdf') })
	else:
		documents_missing.append('GPS')
	if interest_obj.mirror_account:
		documents.append({ 'title': 'Cuenta espejo', 'file': interest_obj.mirror_account, 'is_pdf': interest_obj.mirror_account.url.endswith('.pdf') })
	else:
		documents_missing.append('Cuenta espejo')

	context['documents_list'] = documents
	context['documents_missing'] = documents_missing

	data['html_form'] = render_to_string('prospects/documents_carousel.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
def interest_confirm(request, prospect_id):
	context = {}
	data = dict()

	context['prospect_obj'] = get_object_or_404(Prospect, pk=prospect_id)
	
	if request.is_ajax and request.method == 'POST':
		context['interest_confirm_form'] = InterestConfirmForm(request.POST)
		if context['interest_confirm_form'].is_valid():
			context['prospect_obj'].prospect_status = 'pr'
			context['prospect_obj'].save()

			interest_obj = context['interest_confirm_form'].save(commit=False)
			interest_obj.prospect = context['prospect_obj']
			interest_obj.save()

			data['form_is_valid'] = True
			messages.success(request, 'Interesado creado satisfactoriamente')
			user_logs(request, None, 'I', 'Interesado creado satisfactoriamente')

			data['url_redirect'] = '/panel/prospects/interest/edit/' + str(interest_obj.pk)
		else:
			data['form_is_valid'] = False
	else:
		context['interest_confirm_form'] = InterestConfirmForm()
	
	
	data['html_form'] = render_to_string('prospects/interest_confirm_form.html', context, request=request)
	
	return JsonResponse(data) 

def interest_validate(interest_obj):
	if interest_obj.person_type == 'legal':
		if not interest_obj.constitutive_act: return False
		if not interest_obj.power_attorney: return False
		if not interest_obj.payment_authorization: return False
	if not interest_obj.tax_number: return False
	if not interest_obj.oficial_id: return False
	if not interest_obj.transport_allowance: return False
	if not interest_obj.proof_address : return False
	if not interest_obj.e_billin_demo: return False
	if not interest_obj.bank_statement: return False
	if not interest_obj.unity_list: return False
	if not interest_obj.contract: return False

	return True
	
@login_required
def approve_mail(request, interest_obj):
	mails_to = []
	administrative_mails = User.objects.filter(status=True).prefetch_related('user_groups').filter(user_groups__name='Gerente').values('email')
	attorney_mail = User.objects.filter(status=True).prefetch_related('user_groups').filter(user_groups__name='Abogado').values('email')
	accountant_mail = User.objects.filter(status=True).prefetch_related('user_groups').filter(user_groups__name='Contador').values('email')
	if administrative_mails:
		for admin in administrative_mails:
			mails_to.append(admin['email'])
	if attorney_mail:
		for attorney in attorney_mail:
			mails_to.append(attorney['email'])
	if accountant_mail:
		for accountant in accountant_mail:
			mails_to.append(accountant['email'])


	panel = cache.get('datapanel')
	if not panel:
		PanelAdmin = apps.get_model('config','PanelAdmin')
		panel = PanelAdmin.objects.first()

		cache.set('datapanel',panel)

	subject = 'Transportista inetersado en espera'
	message = render_to_string('prospects/interest_mail_aprove.html', {
									'interest_obj': interest_obj,
									'name_fixed': interest_obj.prospect.name.replace(' ', '%20'),
									'domain': request.build_absolute_uri('/')[:-1],
									'website': panel.url,
									'websitename': panel.title,
									})

	from_email = settings.DEFAULT_TO_EMAIL

	sendmail(subject, message, from_email, mails_to)
	
@login_required
def interest_sendmail(request, prospect_id):
	context = {}
	data = dict()

	interest = get_object_or_404(Interest, prospect_id=prospect_id)

	context["from_mail"] = request.user.email
	context["to_mail"] = interest.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)

			if interest.user:
				mail_obj.user = interest.user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/prospects/'
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 
	
@login_required
def interest_approve_form(request, prospect_id):
	context = {}
	data = dict()

	if request.is_ajax and request.method == 'POST':
		context['interest_confirm_form'] = InterestConfirmForm(request.POST)
		if context['interest_confirm_form'].is_valid():	

			interest = get_object_or_404(Interest, prospect_id=prospect_id)

			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/prospects/interest/approve' + str(interest.pk)
		else:
			data['form_is_valid'] = False
	else:
		context['interest_confirm_form'] = InterestConfirmForm()
	
	
	data['html_form'] = render_to_string('prospects/interest_approve_form.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
def interest_approve(request, pk):
	context = {}
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'
	context["prefix_operative"] = "operative_contact"
	context["prefix_administrative"] = "administrative"

	equipment_formset = formset_factory(InterestEquipmentForm)

	fields = {}

	if request.method == 'POST':
		context['carrier_form'] = CarrierForm(request.POST,request.FILES)
		context['equipment_form'] = equipment_formset(request.POST)
		context['admin_contact_form'] = ContactForm(request.POST, prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(request.POST, prefix=context["prefix_operative"])

		if context['carrier_form'].is_valid() and context['admin_contact_form'].is_valid() and context['operative_contact_form']:
			carrier_obj = context['carrier_form'].save(commit=False)

			admin_contact = context['admin_contact_form'].save(commit=False)
			admin_contact.contact_type = 'administrative'
			admin_contact.save()
			
			operative_contact = context['operative_contact_form'].save(commit=False)
			operative_contact.id = None
			operative_contact.contact_type = 'operative'
			operative_contact.save()

			carrier_obj.administrative_contact = admin_contact
			carrier_obj.operative_contact = operative_contact
			carrier_obj.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.carrier = carrier_obj
					equipment_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Transportista creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Transportista creado satisfactorimente')

			return redirect('carriers:carrier_list')
	else:
		context['carrier_form'] = CarrierForm()
		context['equipment_form'] = equipment_formset()
		context['admin_contact_form'] = ContactForm(prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(prefix=context["prefix_operative"])
	
	return render(request, 'carriers/carrier_form.html', context)

@login_required
def interest_route_delete(request, pk):
	obj = InterestRoutes.objects.get(pk=pk)
	url_redirect = '/panel/prospects/interest/edit/' + str(obj.interest.id)
	data = delete_item(request, obj, 'prospects:interest_route_delete','La ruta del prospecto fue eliminado', url_redirect)
	return JsonResponse(data)

@login_required
def interest_equipment_delete(request, pk):
	equipment_obj = InterestEquipment.objects.get(pk=pk)
	url_redirect = '/panel/prospects/interest/edit/' + str(equipment_obj.interest.id)
	data = delete_item(request, equipment_obj, 'prospects:interest_equipment_delete','El equio del prospecto fue eliminado', url_redirect)
	return JsonResponse(data)