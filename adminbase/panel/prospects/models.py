import os

from django.db import models
from django_extensions.db.models import TimeStampedModel

from panel.core.utils import get_filename
from panel.core.validators import validate_file_extension, validate_pdf_extension

from panel.equipments.models import Equipment
from cities.models import City, Region

from django.urls import reverse

MEDIA_CHOICES = [
    ('', 'Seleccione un medio'),
    ('recommended', 'Recomendado'),
    ('pension', 'Pension'),
    ('web page', 'Pagina web'),
    ('facebook', 'Facebook')
]
PROSPECT_STATUS_CHOICES = [
    ('pd', 'Prospeccion'),
    ('pr', 'Interesado'),
    ('wa', 'En espera'),
    ('ap', 'Aprovado'),
    ('na', 'No apto')
]

def get_file(intance, filename):
	name, ext = os.path.splitext(filename)

	return 'prospect/documents/%s' % get_filename(ext)

def get_image(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'prospect/images/%s' % get_filename(ext)

# Create your models here.
class Prospect(TimeStampedModel):
    medio = models.CharField(max_length=20,choices=MEDIA_CHOICES,verbose_name='Media')
    name = models.CharField(verbose_name='Nombre', max_length=300)
    source = models.ForeignKey(City, related_name='source_path_prospect', verbose_name='Ruta origen de interes', on_delete=models.DO_NOTHING, null=True)
    target = models.ForeignKey(City, related_name='target_path_prospect', verbose_name='Ruta destino de interes', on_delete=models.DO_NOTHING, null=True)
    phone = models.CharField(verbose_name='Telefono', max_length=20)
    picture = models.ImageField(upload_to=get_image,validators=[validate_file_extension],verbose_name='Imagen', null=True, blank=True)
    prospect_status = models.CharField(max_length=20, choices=PROSPECT_STATUS_CHOICES, default='pd', verbose_name='Status del prospecto')
    
    status = models.BooleanField(verbose_name='status', default=True)

    class Meta:
        db_table = 'prospect'
        verbose_name = 'Prospecto'
        verbose_name_plural = 'Prospectos'
        
    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class Interest(TimeStampedModel):
    prospect = models.ForeignKey(Prospect, related_name='prospect_interest', verbose_name='Prospecto interesado', on_delete=models.CASCADE)
    phone = models.CharField(verbose_name='Telefono de oficina', max_length=20, null=True)
    email = models.EmailField(max_length=120, verbose_name='Email')
    person_type = models.CharField(verbose_name='Tipo de persona', max_length=10)

    # Document
    tax_number = models.FileField(upload_to=get_file, verbose_name='RFC', null=True)
    oficial_id = models.FileField(upload_to=get_file, verbose_name='Identificacion', null=True)
    transport_allowance = models.FileField(upload_to=get_file, verbose_name='Permiso de transporte', null=True)
    proof_address = models.FileField(upload_to=get_file, verbose_name='Comprobante de domicilio', null=True)
    e_billin_demo = models.FileField(upload_to=get_file, verbose_name='Factura electronica demo', null=True)
    bank_statement = models.FileField(upload_to=get_file, verbose_name='Estado de cuenta', null=True)
    unity_list = models.FileField(upload_to=get_file, verbose_name='Relacion de unidades', null=True)
    contract = models.FileField(upload_to=get_file, verbose_name='Contrato', null=True)
    constitutive_act = models.FileField(upload_to=get_file, verbose_name='Acta constitutiva', null=True)
    power_attorney = models.FileField(upload_to=get_file, verbose_name='Poder de los representante', null=True)
    payment_authorization = models.FileField(upload_to=get_file, verbose_name='Autorizacion para pagos', null=True)
    
    # 
    driving_license = models.FileField(upload_to=get_file, verbose_name='Licencia', null=True)
    insurance = models.FileField(upload_to=get_file, verbose_name='Seguro de la unidad', null=True)
    registration = models.FileField(upload_to=get_file, verbose_name='Tarjeta de circulacion', null=True)
    gps = models.FileField(upload_to=get_file, verbose_name='GPS', null=True)
    mirror_account = models.FileField(upload_to=get_file, verbose_name='Cuenta espejo', null=True)
    # --

    equipments = models.ManyToManyField(Equipment, through='InterestEquipment')
    
    status = models.BooleanField(verbose_name='status', default=True)

    class Meta:
        db_table = 'prospect_interested'
        verbose_name = 'Prospecto'
        verbose_name_plural = 'Prospectos'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class InterestEquipment(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    interest = models.ForeignKey(Interest, on_delete=models.CASCADE)
    quantity = models.SmallIntegerField(verbose_name="cantidad", default=1)
    
    def get_absolute_url(self):
        return reverse('prospects:interest_equipment_delete', kwargs={'pk': self.pk})


class InterestRoutes(models.Model):
    interest = models.ForeignKey(Interest, related_name='prospect_routes', on_delete=models.CASCADE)
    source_path = models.ForeignKey(City, related_name="prospect_source", verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING)
    source_path_region = models.ForeignKey(Region, related_name="prospect_source_region", verbose_name="Estado origen", max_length=100, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name="prospect_target", verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING, null=True)
    target_path_region = models.ForeignKey(Region, related_name="prospect_target_region", verbose_name="Estado destino", max_length=100, on_delete=models.DO_NOTHING, null=True)

    def get_absolute_url(self):
        return reverse('prospects:interest_route_delete', kwargs={'pk': self.pk})
