from django.conf.urls import url
from .views import prospects
from .views import interest

urlpatterns = [
    url(r'^$', prospects.prospect_list, name='prospect_list'),
	url(r'^add/$', prospects.prospect_add, name='prospect_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', prospects.prospect_edit, name='prospect_edit'),
	url(r'^delete/(?P<pk>[0-9]+)/$', prospects.prospect_delete, name='prospect_delete'),
	url(r'^confirm/(?P<prospect_id>[0-9]+)/$', interest.interest_confirm, name='interest_confirm'),

	url(r'^interest/(?P<pk>[0-9]+)/$', interest.interest_list, name='interest_list'),
	# url(r'^interest/add/(?P<pk>[0-9]+)/$', interest.interest_add, name='interest_add'),
	url(r'^interest/edit/(?P<pk>[0-9]+)/$', interest.interest_edit, name='interest_edit'),
	url(r'^interest/documents/(?P<prospect_id>[0-9]+)/$', interest.interest_documents, name='interest_documents'),
	url(r'^interest/sendmail/(?P<prospect_id>[0-9]+)/$', interest.interest_sendmail, name='interest_sendmail'),
	url(r'^interest/approve/(?P<pk>[0-9]+)/$', interest.interest_approve, name='interest_approve'),
	url(r'^interest/approve/ask/(?P<prospect_id>[0-9]+)/$', interest.interest_approve_form, name='interest_approve_form'),

	url(r'^routes/delete/(?P<pk>[0-9]+)/$', interest.interest_route_delete, name='interest_route_delete'),
	url(r'^equipments/delete/(?P<pk>[0-9]+)/$', interest.interest_equipment_delete, name='interest_equipment_delete'),

]