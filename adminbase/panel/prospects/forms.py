from django import forms
from .models import Prospect, Interest, InterestEquipment, InterestRoutes, MEDIA_CHOICES
from panel.equipments.models import Equipment
from cities.models import City, Region

PERSON_CHOICES= [
	('natural', 'Persona Fisica'),
	('legal', 'Persona Moral')
]

class ProspectForm(forms.ModelForm):
	name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required' : 'Debe capturar el nombre del prospecto'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	medio = forms.ChoiceField(
		label = 'Medio de informacion',
		error_messages = {'required' : 'Debe capturar el medio de informacion'},
		choices = MEDIA_CHOICES,
		widget = forms.Select(
			attrs = {
				'class' : 'form-control select2',
			}
		)
	)
	source = forms.ModelChoiceField(
		label = 'Ciudad origen',
		required = False,
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	source_region = forms.ModelChoiceField(
		label='Estado origen',
		required = False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target = forms.ModelChoiceField(
		label='Ciudad destino',
		required = False,
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	target_region = forms.ModelChoiceField(
		label='Estado destino',
		required = False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	phone = forms.CharField(
		label="Celular",
		error_messages = {'required': 'Debe capturar el telefono'},
		widget=forms.TextInput(
			attrs={
				"class": "form-control", 
			}
		)
	)
	picture = forms.ImageField(
		label = 'Imagen',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	class Meta:
		model = Prospect
		fields = ('medio', 'name', 'source', 'target', 'phone', 'picture')

class InterestForm(forms.ModelForm):
	phone = forms.CharField(
		label="Telefono de casa/oficina",
		error_messages = {'required': 'Debe capturar el telefono'},
		widget=forms.TextInput(
			attrs={
				"class": "form-control", 
			}
		)
	)
	tax_number = forms.FileField(
		label = 'RFC',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	oficial_id = forms.FileField(
		label = 'Identificacion oficial de los representantes',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	transport_allowance = forms.FileField(
		label = 'Permiso federal de transporte, emitido por SCT',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	proof_address = forms.FileField(
		label = 'Comprobante de domicilio (No mayor a 3 meses)',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	e_billin_demo = forms.FileField(
		label = 'Demo de factura electronica',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	bank_statement = forms.FileField(
		label = 'Caratula de estado de cuenta bancario',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	unity_list = forms.FileField(
		label = 'Relacion de unidades: Placas, serie, Nombre de operador',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	contract = forms.FileField(
		label = 'Contrato de Logistica Naves',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)

	constitutive_act = forms.FileField(
		label = 'Acta constitutiva',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	power_attorney = forms.FileField(
		label = 'Poder de los representantes legales',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	payment_authorization = forms.FileField(
		label = 'Carta autorizacion para pagos',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	driving_license = forms.FileField(
		label = 'Licencia de manejo al 200%',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	insurance = forms.FileField(
		label = 'Seguro de la unidad',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	registration = forms.FileField(
		label = 'Tarjeta de circulacion (tractor y remolque)',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	gps = forms.FileField(
		label = 'GPS activo (pagina web y clave de acceso)',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)
	mirror_account = forms.FileField(
		label = 'Cuenta espejo obligatoria',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
				'accept' : 'application/pdf',
				'accept' : 'images/*',
			}
		)
	)

	class Meta:
		model = Interest
		fields = ('phone', 'tax_number', 'oficial_id', 'transport_allowance', 'proof_address',\
			'e_billin_demo', 'bank_statement', 'unity_list', 'contract', 'constitutive_act',\
			'power_attorney', 'payment_authorization')

class InterestConfirmForm(forms.ModelForm):
	person_type = forms.ChoiceField(
		choices = PERSON_CHOICES, 
		widget=forms.RadioSelect
	)

	email = forms.EmailField(
		label = 'Email',
		error_messages = {'required': 'Debe capturar el Email'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control required',
				'aria-required' : 'True'
			}
		)
	)

	class Meta:
		model = Interest
		fields = ('person_type', 'email',)

class InterestEquipmentForm(forms.ModelForm):
	
	equipment = forms.ModelChoiceField(
		label='Equipos',
		required = False,
		queryset = Equipment.objects.filter(status=True),
		empty_label = 'Seleccione un equipo...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	quantity = forms.CharField(
		label = 'Cantidad',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class': 'touchspin',
			}
		)
	 )
	class Meta:
		model = InterestEquipment
		fields = ('equipment', 'quantity')

class InterestRoutesForm(forms.ModelForm):
	source_path = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	source_path_region = forms.ModelChoiceField(
		label='Estado origen',
		required= False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_path = forms.ModelChoiceField(
		label='Ciudad destino',
		error_messages = {'required' : 'Debe capturar la ruta destino'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	target_path_region = forms.ModelChoiceField(
		label='Estado destino',
		required= False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	class Meta:
		model = InterestRoutes
		fields = ('source_path', 'source_path_region', 'target_path', 'target_path_region',)
