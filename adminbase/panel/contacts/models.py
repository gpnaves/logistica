from django.db import models

# Create your models here.
class Contact(models.Model):
    contact_type = models.CharField(verbose_name='Tipo', max_length=100, default='')
    name = models.CharField(verbose_name='Nombre', max_length=200)
    email = models.EmailField(verbose_name='Email', max_length=200)
    phone = models.CharField(verbose_name='Telefono', max_length=60)
    cellphone = models.CharField(verbose_name='Celular', max_length=60)

    class Meta:
        db_table = 'contacts'
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'

    def __str__(self):
        return self.name + ', ' + self.email