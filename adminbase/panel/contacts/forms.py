from django import forms

from .models import Contact

class ContactForm(forms.ModelForm):
	name = forms.CharField(
		label = 'Nombre',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	email = forms.EmailField(
		label = 'Email',
		error_messages = {'required': 'Debe capturar el correo'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	phone = forms.CharField(
		label="Telefono",
		error_messages = {'required': 'Debe capturar el telefono'},
		widget=forms.TextInput(
			attrs={
				"class": "form-control", 
			}
		)
	)
	cellphone = forms.CharField(
		label="Celular",
		error_messages = {'required': 'Debe capturar el celular'},
		widget=forms.TextInput(
			attrs={
				"class": "form-control", 
			}
		)
	)

	class Meta:
		model = Contact
		fields = ('name', 'email', 'phone', 'cellphone',)
	