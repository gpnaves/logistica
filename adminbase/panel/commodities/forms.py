from django import forms

from .models import Commodity

class CommodityForm(forms.ModelForm):

	name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required': 'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	picture = forms.ImageField(
		label = 'Imagen',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	class Meta:
		model = Commodity
		fields = ('picture', 'name')