
from django.conf.urls import url
from panel.commodities.views import commodities

urlpatterns = [
    url(r'^$', commodities.commodity_list, name='commodity_list'),
	url(r'^add/$', commodities.commodity_add, name='commodity_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', commodities.commodity_edit, name='commodity_edit'),
	url(r'^delete/(?P<pk>[0-9]+)/$', commodities.commodity_delete, name='commodity_delete'),
]