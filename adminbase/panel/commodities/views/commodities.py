
from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from ..models import Commodity
from ..forms import CommodityForm

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def commodity_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['commodity_list'] = Commodity.objects.filter(status=True).order_by('name')

	return render(request, 'commodities/commodity_list.html', context)
	
@login_required
@has_permissions('can_add_' + app_name)
def commodity_add(request):
	context = {}

	if request.method == 'POST':
		context['commodity_form'] = CommodityForm(request.POST,request.FILES)

		if context['commodity_form'].is_valid():
			commodity_obj = context['commodity_form'].save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Mercancia creada satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Mercancia creada satisfactorimente')

			#user = create_user(request, context['commodity_form'].type_user)
			return redirect('commodities:commodity_list')
	else:
		context['commodity_form'] = CommodityForm()
	
	return render(request, 'commodities/commodity_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def commodity_edit(request, pk):
	context = {}

	context['commodity_obj'] = get_object_or_404(Commodity, pk=pk)

	if request.method == 'POST':
		context['commodity_form'] = CommodityForm(request.POST, instance=context['commodity_obj'])
		
		if context['commodity_form'].is_valid():
			context['commodity_form'].save()
			
			#-- Message to user
			messages.success(request, 'Mercancia modificada satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Mercancia modificada satisfactoriamente')

			return redirect('commodities:commodity_list')
	else:

		context['commodity_form'] = CommodityForm(instance=context['commodity_obj'])

	return render(request, 'commodities/commodity_form.html', context)

@login_required
@has_permissions('can_delete_' + app_name)
def commodity_delete(request, pk):
	commodity = get_object_or_404(Commodity, pk=pk)

	data = delete_record(request, commodity, '/panel/commodities/', 'commodities:commodity_delete')
	return JsonResponse(data)