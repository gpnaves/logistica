# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

from django.db import models

from panel.core.utils import get_filename
from panel.core.validators import validate_file_extension

# Create your models here.

def get_image(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'commodity/%s' % get_filename(ext)

class Commodity(models.Model):
    name = models.CharField(verbose_name="mercancia", max_length=150)
    picture = models.ImageField(upload_to=get_image,validators=[validate_file_extension],verbose_name='Imagen', null=True, blank=True)
    status = models.BooleanField(verbose_name="status", default=True)

    class Meta:
        db_table = 'commodities'
        verbose_name = 'Mercancia'
        verbose_name_plural = 'Mercancias'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name