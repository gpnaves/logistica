from django.conf.urls import url
from panel.customers.views import customers

urlpatterns = [
    url(r'^$', customers.customer_list, name='customer_list'),
	url(r'^add/$', customers.customer_add, name='customer_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', customers.customer_edit, name='customer_edit'),
	url(r'^detail/(?P<pk>[0-9]+)/$', customers.customer_detail, name='customer_detail'),
	url(r'^delete/(?P<pk>[0-9]+)/$', customers.customer_delete, name='customer_delete'),
	url(r'^sendmail/(?P<pk>[0-9]+)/(?P<type>[a-z]+)$', customers.customer_sendmail, name='customer_sendmail'),

	url(r'^equipments/delete/(?P<pk>[0-9]+)/$', customers.customer_equipment_delete, name='customer_equipment_delete'),
	url(r'^commodities/delete/(?P<pk>[0-9]+)/$', customers.customer_commodity_delete, name='customer_commodity_delete'),
	url(r'^routes/delete/(?P<pk>[0-9]+)/$', customers.customer_route_delete, name='customer_route_delete'),

	url(r'^routes/(?P<pk>[0-9]+)/(?P<source>[0-9]+)/(?P<target>[0-9]+)/$', customers.customer_routes, name='customer_routes'),
	url(r'^routes/(?P<pk>[0-9]+)/$', customers.customer_routes, name='customer_routes'),
]