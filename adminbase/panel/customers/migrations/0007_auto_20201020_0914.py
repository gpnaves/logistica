# Generated by Django 3.0.7 on 2020-10-20 15:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0006_auto_20201020_0839'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='tax_number',
            field=models.CharField(max_length=15, verbose_name='RFC'),
        ),
    ]
