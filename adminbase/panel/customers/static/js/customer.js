$(document).ready(function() {
    $('#id_city').prop('readonly', true);
    $('#id_subregion').prop('readonly', true);
    $('#id_region').prop('readonly', true);
    $('#id_country').prop('readonly', true); 

    $("#id_source_path_region").change(function() {
        if($(this).val() != ""){
            _value = $(this).val();

            $.get('/panel/locations/cities_per_region/', {region_id: _value}, function(data){
              $("#id_source_path").empty();
              $("#id_source_path").append('<option selected disabled="disabled" value="">Seleccione el origen...</option>');
              $.each(JSON.parse(data.cities), function(idx, obj) {
                  $("#id_source_path").append('<option value='+obj.pk+'>'+obj.fields.name+'</option>');
              });
              $("#id_source_path").prop('disabled', false);
            });
        
        }
    });

    $("#id_target_path_region").change(function() {
        if($(this).val() != ""){
            _value = $(this).val();

            $.get('/panel/locations/cities_per_region/', {region_id: _value}, function(data){
              $("#id_target_path").empty();
              $("#id_target_path").append('<option selected disabled="disabled" value="">Seleccione el destino...</option>');
              $.each(JSON.parse(data.cities), function(idx, obj) {
                  $("#id_target_path").append('<option value='+obj.pk+'>'+obj.fields.name+'</option>');
              });
              $("#id_target_path").prop('disabled', false);
            });
        
        }
    });

    if($("#id_alias").val() == "") {
        $(".cities").hide();
    }
    $("#id_address").focusout(function(){
        var postal_code = $("#id_postal_code").val();
        var address = $(this).val();
        if(postal_code != "" && address != ""){
            var _address = address.replace(/ /g, '%20');
            address = _address.replace('#', '%23');

            get_map_loc(postal_code, address)            
        }
    });
    $("#id_postal_code").focusout(function(){
        var postal_code = $(this).val();
        var address = $("#id_address").val();
        if(postal_code != "" && address != ""){
            var _address = address.replace(/ /g, '%20');
            address = _address.replace('#', '%23');

            get_map_loc(postal_code, address)            
        }
    });
    var get_map_loc = function(postal_code, address){
        $.ajax({
            url: '/panel/locations/get_cities_info/',
            type: 'GET',
            data: {postal_code: postal_code},
            success: function (data) {
                var coordinates = []
                $.ajax({
                    url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '%2C%20' +postal_code + '%2C%20' + data.city + '.json?access_token=pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA',
                    type: 'GET',
                    success: function(data) {
                        coordinates = data.features[0].geometry.coordinates;
                        mapboxgl.accessToken = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA';
                        var map = new mapboxgl.Map({
                            container: 'map',
                            center: coordinates,
                            zoom: 7,
                            style: 'mapbox://styles/mapbox/streets-v11'
                        });

                        var marker = new mapboxgl.Marker({
                            draggable: false
                        }).setLngLat(coordinates).addTo(map);
                        map.addControl(new mapboxgl.NavigationControl());
                    ;}
                });


                if(data.success){  
                    $(".cities").show();
                    $('#id_city').val(data.city);
                    $('#id_city').prop('readonly', true);
                    $('#id_subregion').val(data.subregion);
                    $('#id_subregion').prop('readonly', true);
                    $('#id_region').val(data.region);
                    $('#id_region').prop('readonly', true);
                    $('#id_country').val(data.country);
                    $('#id_country').prop('readonly', true);  

                } else {
                    $('#id_city').prop('readonly', false);
                    $('#id_city').val('');
                    $('#id_subregion').prop('readonly', false);
                    $('#id_subregion').val('');
                    $('#id_region').prop('readonly', false);
                    $('#id_region').val('');
                    $('#id_country').prop('readonly', false);  
                    $('#id_country').val('');
                };
            ;}
        });
    };
});