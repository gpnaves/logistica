$(document).ready(function() {
    $("#id_mail_form").hide();

    var is_mail_visible=false;

    $("#operative_mail").click(function(){
        if(!is_mail_visible){
            $("#id_mail_form").show();
            is_mail_visible=true;
        }
        $("#id_to_mail").val($("#id_operative_mail").val());
    });

    $("#admin_mail").click(function(){
        if(!is_mail_visible){
            $("#id_mail_form").show();
            is_mail_visible=true;
        }
        $("#id_to_mail").val($("#id_admin_mail").val());
    });

    $("#id_close_mail").click(function(){
        $("#id_mail_form").hide();
        is_mail_visible=false;
    });
});