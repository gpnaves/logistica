# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from cities.models import City, Region
from django.db import models
from django.urls import reverse
from panel.accounts.models import User
from panel.commodities.models import Commodity
from panel.contacts.models import Contact
from panel.core.utils import get_filename
from panel.core.validators import validate_file_extension
from panel.employees.models import Employee
from panel.equipments.models import Equipment

# Create your models here.


def get_image(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'customers/%s' % get_filename(ext)


class Customer(models.Model):
    picture = models.ImageField(upload_to=get_image, validators=[
                                validate_file_extension], verbose_name='Imagen', null=True, blank=True)
    business_name = models.CharField(
        verbose_name='Razón Social', max_length=200)
    alias = models.CharField(verbose_name="Alias", max_length=200)
    address = models.CharField(verbose_name="Direccion", max_length=500)
    postal_code = models.CharField(
        verbose_name="Codigo postal", max_length=5, null=True)
    city = models.CharField(verbose_name="Ciudad",
                            max_length=100, default="", null=True)
    subregion = models.CharField(
        verbose_name="Municipio", max_length=100, null=True)
    region = models.CharField(verbose_name="Estado", max_length=100, null=True)
    country = models.CharField(verbose_name="Pais", max_length=100, null=True)
    tax_number = models.CharField(verbose_name="RFC", max_length=20)

    user = models.ForeignKey(User, related_name='customer_user',
                             verbose_name='Usuario cliente', null=True, on_delete=models.DO_NOTHING)
    operative_contact = models.ForeignKey(Contact, verbose_name="Contacto operativo",
                                          null=True, related_name="operative_contact_customer", on_delete=models.SET_NULL)
    administrative_contact = models.ForeignKey(Contact, verbose_name="Contacto administrativo",
                                               null=True, related_name="administrative_contact_customet", on_delete=models.SET_NULL)

    coordinator = models.ForeignKey(Employee, related_name='customer_coordinator',
                                    verbose_name='Coordinador', on_delete=models.DO_NOTHING, null=True)

    equipments = models.ManyToManyField(Equipment, through='Equipments')
    commodities = models.ManyToManyField(Commodity, through='Commodities')

    status = models.BooleanField(verbose_name="status", default=True)

    class Meta:
        db_table = 'customers'
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return self.alias


class Equipments(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, related_name="equipment_customer")

    def get_absolute_url(self):
        return reverse('customers:customer_equipment_delete', kwargs={'pk': self.pk})


class Commodities(models.Model):
    commodity = models.ForeignKey(Commodity, on_delete=models.CASCADE)
    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, related_name="commodity_customer")

    def get_absolute_url(self):
        return reverse('customers:customer_commodity_delete', kwargs={'pk': self.pk})


class CustomerInterestRoutes(models.Model):
    customer = models.ForeignKey(
        Customer, related_name='customer_interest_routes', on_delete=models.CASCADE)
    source_path = models.ForeignKey(City, related_name="customer_source_path",
                                    verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING)
    source_path_region = models.ForeignKey(Region, related_name="customer_source_path_region",
                                           verbose_name="Estado origen", max_length=100, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name="customer_target_path",
                                    verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING, null=True)
    target_path_region = models.ForeignKey(Region, related_name="customer_target_path_region",
                                           verbose_name="Estado destino", max_length=100, on_delete=models.DO_NOTHING, null=True)

    def get_absolute_url(self):
        return reverse('customers:customer_route_delete', kwargs={'pk': self.pk})
