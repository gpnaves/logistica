
from ..models import Customer, Equipments, Commodities, CustomerInterestRoutes
from ..forms import CustomerForm, EquipmentForm, CommodityForm, InterestRoutesForm
from panel.core.forms import MailForm
from panel.equipments.models import Equipment

from panel.contacts.models import Contact
from panel.contacts.forms import ContactForm
from panel.routes.models import Route

from django.forms import formset_factory

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item, sendmail
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from django.contrib.auth.decorators import login_required

from panel.core.decorators import has_permissions

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def customer_list(request):
	context = {}
	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['customers_list'] = Customer.objects.filter(status=True).order_by('alias')

	return render(request, 'customers/customer_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def customer_add(request):
	context = {}
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'
	context["prefix_operative"] = "operative_contact"
	context["prefix_administrative"] = "administrative"

	context['prefix_equipment'] = 'equipment'
	context['prefix_commodity'] = 'commodity'
	context['prefix_routes'] = 'routes'

	equipment_formset = formset_factory(EquipmentForm)
	commodity_formset = formset_factory(CommodityForm)
	routes_formset = formset_factory(InterestRoutesForm)

	if request.method == 'POST':
		context['customer_form'] = CustomerForm(request.POST,request.FILES)
		context['equipment_form'] = equipment_formset(request.POST, prefix=context['prefix_equipment'])
		context['commodity_form'] = commodity_formset(request.POST, prefix=context['prefix_commodity'])
		context['admin_contact_form'] = ContactForm(request.POST, prefix=context['prefix_administrative'])
		context['operative_contact_form'] = ContactForm(request.POST, prefix=context["prefix_operative"])
		context['routes_form'] = routes_formset(request.POST, prefix=context['prefix_routes'])

		if context['customer_form'].is_valid() and context['admin_contact_form'].is_valid() and context['operative_contact_form']:
			customer_obj = context['customer_form'].save(commit=False)

			admin_contact = context['admin_contact_form'].save(commit=False)
			admin_contact.contact_type = 'administrative'
			admin_contact.save()
			
			operative_contact = context['operative_contact_form'].save(commit=False)
			operative_contact.id = None
			operative_contact.contact_type = 'operative'
			operative_contact.save()

			customer_obj.administrative_contact = admin_contact
			customer_obj.operative_contact = operative_contact
			customer_obj.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.customer = customer_obj
					equipment_obj.save()

			for commodity in context['commodity_form']:
				if commodity.has_changed():
					commodity_obj = commodity.save(commit=False)
					commodity_obj.customer = customer_obj
					commodity_obj.save()

			for route in context['routes_form']:
				if route.has_changed():
					route_obj = route.save(commit=False)
					route_obj.customer = customer_obj
					route_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Cliente creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Cliente creado satisfactorimente')

			return redirect('customers:customer_list')
	else:
		context['customer_form'] = CustomerForm()
		context['equipment_form'] = equipment_formset(prefix=context['prefix_equipment'])
		context['commodity_form'] = commodity_formset(prefix=context['prefix_commodity'])
		context['admin_contact_form'] = ContactForm(prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(prefix=context["prefix_operative"])
		context['routes_form'] = routes_formset(prefix=context['prefix_routes'])

	return render(request, 'customers/customer_form.html', context)
	
@login_required
@has_permissions('can_edit_' + app_name)
def customer_edit(request, pk):
	context = {}
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'

	context["prefix_operative"] = "operative_contact"
	context["prefix_administrative"] = "administrative"

	context['prefix_equipment'] = 'equipment'
	context['prefix_commodity'] = 'commodity'
	context['prefix_routes'] = 'routes'
	
	context['customer_obj'] = get_object_or_404(Customer, pk=pk)
	context['admin_contact_obj'] = get_object_or_404(Contact, pk=context['customer_obj'].administrative_contact.pk)
	context['operative_contact_obj'] = get_object_or_404(Contact, pk=context['customer_obj'].operative_contact.pk)

	context['equipments'] = Equipments.objects.filter(customer_id=pk)
	context['commodities'] = Commodities.objects.filter(customer_id=pk)
	
	equipment_formset = formset_factory(EquipmentForm)
	commodity_formset = formset_factory(CommodityForm)
	routes_formset = formset_factory(InterestRoutesForm)

	if request.method == 'POST':
		context['customer_form'] = CustomerForm(request.POST,request.FILES, instance=context['customer_obj'])
		context['admin_contact_form'] = ContactForm(request.POST, instance=context['admin_contact_obj'], prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(request.POST, instance=context['operative_contact_obj'], prefix=context["prefix_operative"])
		context['equipment_form'] = equipment_formset(request.POST, prefix=context['prefix_equipment'])
		context['commodity_form'] = commodity_formset(request.POST, prefix=context['prefix_commodity'])
		context['routes_form'] = routes_formset(request.POST, prefix=context['prefix_routes'])

		if context['customer_form'].is_valid() and context['admin_contact_form'].is_valid() and context['operative_contact_form']:
			customer_obj = context['customer_form'].save(commit=False)
			
			admin_contact = context['admin_contact_form'].save(commit=False)
			admin_contact.contact_type = 'administrative'
			admin_contact.save()
			
			operative_contact = context['operative_contact_form'].save(commit=False)
			operative_contact.id = None
			operative_contact.contact_type = 'operative'
			operative_contact.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.customer = customer_obj
					equipment_obj.save()

			for commodity in context['commodity_form']:
				if commodity.has_changed():
					commodity_obj = commodity.save(commit=False)
					commodity_obj.customer = customer_obj
					commodity_obj.save()

			for route in context['routes_form']:
				if route.has_changed():
					if route.is_valid():
						route_obj = route.save(commit=False)
						route_obj.customer = customer_obj
						route_obj.save()

			customer_obj.administrative_contact = admin_contact
			customer_obj.operative_contact = operative_contact
			customer_obj.save()

			

			msg = messages.success(request, 'Cliente modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Cliente modificado satisfactorimente')

			return redirect('customers:customer_list')
	else:
		context['equipment_form'] = equipment_formset(prefix=context['prefix_equipment'])
		context['commodity_form'] = commodity_formset(prefix=context['prefix_commodity'])
		context['customer_form'] = CustomerForm(instance=context['customer_obj'])
		context['admin_contact_form'] = ContactForm(instance=context['admin_contact_obj'], prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(instance=context['operative_contact_obj'], prefix=context["prefix_operative"])
		context['routes_form'] = routes_formset(prefix=context['prefix_routes'])
		context['interest_routes'] = CustomerInterestRoutes.objects.filter(customer_id=pk)

	return render(request, 'customers/customer_form.html', context)

@login_required
def customer_detail(request, pk):
	context = {}
	context['customer_obj'] = get_object_or_404(Customer, pk=pk)
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'
	context['equipments'] = Equipments.objects.filter(customer_id=pk)
	context['commodities'] = Commodities.objects.filter(customer_id=pk)
	
	data = dict()
	context['mail_form'] = MailForm()
	data['html_form'] = render_to_string('customers/customer_detail.html', context, request=request)
	
	return JsonResponse(data)

@login_required
@has_permissions('can_delete_' + app_name)
def customer_delete(request, pk):
	customer = get_object_or_404(Customer, pk=pk)

	data = delete_record(request, customer, '/panel/customers/', 'customers:customer_delete')
	return JsonResponse(data)
	
def customer_sendmail(request, pk, type):
	context = {}
	data = dict()

	customer = get_object_or_404(Customer, pk=pk)

	context["from_mail"] = request.user.email
	if type=="operative":
		context["to_mail"] = customer.operative_contact.email
	elif type=="administrative":
		context["to_mail"] = customer.administrative_contact.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)

			if customer.user:
				mail_obj.user = customer.user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/customer/'
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 
	
@login_required
def customer_route_delete(request, pk):
	obj = CustomerInterestRoutes.objects.get(pk=pk)
	url_redirect = '/panel/customers/edit/' + str(obj.customer.id)
	data = delete_item(request, obj, 'customers:customer_route_delete','La ruta del cliente fue eliminada', url_redirect)
	return JsonResponse(data)

@login_required
def customer_equipment_delete(request, pk):
	equipment_obj = Equipments.objects.get(pk=pk)
	url_redirect = '/panel/customers/edit/' + str(equipment_obj.customer.id)
	data = delete_item(request, equipment_obj, 'customers:customer_equipment_delete','El equio del cliente fue eliminado', url_redirect)
	return JsonResponse(data)


@login_required
def customer_commodity_delete(request, pk):
	commodity_obj = Commodities.objects.get(pk=pk)
	url_redirect = '/panel/customers/edit/' + str(commodity_obj.customer.id)
	data = delete_item(request, commodity_obj, 'customers:customer_commodity_delete','La mercancia del cliente fue eliminada', url_redirect)
	return JsonResponse(data)

@login_required
def customer_routes(request, pk, source=None, target=None):
	context = {}
	data = dict()

	if source and target:
		routes = Route.objects.filter(customer_id=pk, source_id=source, target_id=target)
	else:
		routes = Route.objects.filter(customer_id=pk)
	context['routes_list'] = routes

	data['html_form'] = render_to_string('customers/customer_routes.html', context, request=request)
	
	return JsonResponse(data) 