from django.conf.urls import url
from .views import employees

urlpatterns = [
    url(r'^$', employees.employee_list, name='employee_list'),
	url(r'^add/$', employees.employee_add, name='employee_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', employees.employee_edit, name='employee_edit'),
	url(r'^detail/(?P<pk>[0-9]+)/$', employees.employee_detail, name='employee_detail'),
	url(r'^delete/(?P<pk>[0-9]+)/$', employees.employee_delete, name='employee_delete'),
	url(r'^sendmail/(?P<pk>[0-9]+)/$', employees.employee_sendmail, name='employee_sendmail'),
]