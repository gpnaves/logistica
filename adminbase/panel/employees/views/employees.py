from ..models import Employee
from ..forms import EmployeeForm
from panel.core.forms import MailForm

from panel.contacts.models import Contact
from panel.contacts.forms import ContactForm
from panel.locations.models import ZONES

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item, sendmail
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def employee_list(request):
	context = {}
	
	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['filter'] = request.GET.get('filter')
	context['zones'] = ZONES	
	context['employee_list'] = Employee.objects.filter(status=True).order_by('first_name')

	return render(request, 'employees/employee_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def employee_add(request):
	context = {}

	if request.method == 'POST':
		context['employee_form'] = EmployeeForm(request.POST,request.FILES)
		context['contact_form'] = ContactForm(request.POST)

		if context['employee_form'].is_valid() and context['contact_form'].is_valid():
			employee_obj = context['employee_form'].save(commit=False)

			contact = context['contact_form'].save(commit=False)
			contact.contact_type = 'worker'
			contact.name = employee_obj.first_name
			contact.save()

			employee_obj.contact = contact
			employee_obj.save()


			#-- Message to beneficiary
			msg = messages.success(request, 'Empleado creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Empleado creado satisfactorimente')

			return redirect('employees:employee_list')
	else:
		context['employee_form'] = EmployeeForm()
		context['contact_form'] = ContactForm()
	
	return render(request, 'employees/employee_form.html', context)
	
@login_required
@has_permissions('can_edit_' + app_name)
def employee_edit(request, pk):
	context = {}

	context['employee_obj'] = get_object_or_404(Employee, pk=pk)
	context['contact_obj'] = get_object_or_404(Contact, pk=context['employee_obj'].contact.pk)

	if request.method == 'POST':
		context['employee_form'] = EmployeeForm(request.POST,request.FILES, instance=context['employee_obj'])
		context['contact_form'] = ContactForm(request.POST, instance=context['contact_obj'])

		if context['employee_form'].is_valid() and context['contact_form'].is_valid():
			employee_obj = context['employee_form'].save(commit=False)
			
			contact = context['contact_form'].save(commit=False)
			contact.name = employee_obj.first_name
			contact.contact_type = 'worker'
			contact.save()

			employee_obj.contact = contact
			employee_obj.save()

			msg = messages.success(request, 'Empleado modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Empleado modificado satisfactorimente')

			return redirect('employees:employee_list')
	else:
		context['employee_form'] = EmployeeForm(instance=context['employee_obj'])
		context['contact_form'] = ContactForm(instance=context['contact_obj'])
	
	return render(request, 'employees/employee_form.html', context)

@login_required
def employee_detail(request, pk):
	context = {}
	context['employee_obj'] = get_object_or_404(Employee, pk=pk)
	
	data = dict()
	data['html_form'] = render_to_string('employees/employee_detail.html', context, request=request)
	
	return JsonResponse(data)

@login_required
@has_permissions('can_delete_' + app_name)
def employee_delete(request, pk):
	employee = get_object_or_404(Employee, pk=pk)

	data = delete_record(request, employee, '/panel/employees/', 'employees:employee_delete')
	return JsonResponse(data)

@login_required
def employee_sendmail(request, pk):
	context = {}
	data = dict()

	employee = get_object_or_404(Employee, pk=pk)

	context["from_mail"] = request.user.email
	context["to_mail"] = employee.contact.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)

			if employee.user:
				mail_obj.user = employee.user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/employees/'
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 
