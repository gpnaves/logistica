from django import forms

from .models import Employee, ZONES_COICES
from panel.position.models import Position
from panel.contacts.models import Contact
from panel.accounts.models import User

BIRTH_YEAR_CHOICES = [
	'1960', '1961', '1962', '1963', '1964', '1965', '1966', '1967', '1968', '1969', \
	'1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978', '1979',\
	'1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987', '1988', '1989',\
	'1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999',\
	'2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009',\
	'2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019',\
	'2020'
]
HIRED_YEAR_CHOICES = [
	'2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', 
	'2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019',
	'2020'  
]

class EmployeeForm(forms.ModelForm):
	first_name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required': 'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	last_name = forms.CharField(
		label = 'Apellidos',
		error_messages = {'required': 'Debe capturar los apellidos'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	birthdate = forms.DateField(
		label = 'Fecha de nacimiento',
		error_messages = {'required': 'Debe capturar la fecha de nacimiento'},
		widget= forms.SelectDateWidget(
			years = BIRTH_YEAR_CHOICES,
			attrs = {
				'class' : 'form-contorl select2 birthdate'
			}
		)
	)

	position = forms.ModelChoiceField(
		label='Posicion',
		error_messages = {'required' : 'Debe capturar la posicion'},
		queryset = Position.objects.all(),
		empty_label = 'Seleccione la posicion...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	picture = forms.ImageField(
		label = 'Foto',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	hiredate = forms.DateField(
		label = 'Fecha de Contrato',
		required = False,
		widget= forms.SelectDateWidget(
			years = HIRED_YEAR_CHOICES,
			attrs = {
				'class' : 'form-contorl select2'
			}
		)
	)

	salary = forms.DecimalField(
		label = 'Salario',
 		required = False,
 		widget = forms.NumberInput(
			attrs = {
				'class':'form-control',
				'min': '0',
				'step': '100',
			}
		)
	)

	zone = forms.ChoiceField(
		label = 'Zona',
		required = False,
        choices = ZONES_COICES,
		widget = forms.Select(
			attrs = {
				'class' : 'form-control select2',
			}
		)
	)

	manager = forms.ModelChoiceField(
		label='Encargado',
		required = False,
		queryset = Employee.objects.filter(position__title__icontains = 'Supervisor de zona'),
		empty_label = 'Opcional...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	user = forms.ModelChoiceField(
		label='Usuario',
		required=False,
		queryset = User.objects.filter(status=True),
		empty_label = "Seleccione...",
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2'
			}
		)
	)

	class Meta:
		model = Employee
		fields = ('picture', 'first_name','last_name','birthdate','position', 'salary', \
			'hiredate', 'zone', 'manager', 'user')

