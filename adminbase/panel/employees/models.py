# -*- coding: utf-8 -*-
import os
from django.db import models

from panel.core.utils import get_filename
from panel.core.validators import validate_file_extension

from panel.position.models import Position
from panel.contacts.models import Contact
from panel.accounts.models import User

from datetime import datetime

ZONES_COICES = [
    ('', 'Selecciona una zona'),
    ('pacific', 'Pacifico'),
    ('center', 'Bajio'),
    ('southeast', 'Sureste'),
    ('occident', 'Occidente'),
    ('north', 'Norte'),
    ('all', 'Todas')
]

# Create your models here.
def get_image(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'employees/%s' % get_filename(ext)


class Employee(models.Model):
    picture = models.ImageField(upload_to=get_image,validators=[validate_file_extension],verbose_name='Imagen', null=True, blank=True)
    first_name = models.CharField(verbose_name='Nombre', max_length=200)
    last_name = models.CharField(verbose_name='Apellidos', max_length=300)
    birthdate = models.DateField(verbose_name='Fecha de nacimiento')
    contact = models.ForeignKey(Contact, related_name="employee_contact", verbose_name="Contacto", on_delete=models.DO_NOTHING, null=True)
    position = models.ForeignKey(Position, related_name="employee_position", verbose_name="Posicion", on_delete=models.DO_NOTHING, null=True)
    salary = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    hiredate = models.DateField(verbose_name="Fecha de contrato", null=True)
    manager = models.ForeignKey('self', verbose_name='Encargado', null=True, blank=True, on_delete=models.DO_NOTHING)
    zone = models.CharField(verbose_name='Zona', choices=ZONES_COICES, max_length=20, null=True)
    user = models.ForeignKey(User, related_name="employee_user", verbose_name="Usuario", on_delete=models.DO_NOTHING, null=True)

    status = models.BooleanField(verbose_name="status", default=True)

    class Meta:
        db_table = 'employee'
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'

    @property
    def age(self):
        return int((datetime.now().date() - self.birthdate).days / 365.25)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

