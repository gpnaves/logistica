from ..models import Equipment
from ..forms import EquipmentForm

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def equipment_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['equipment_list'] = Equipment.objects.filter(status=True).order_by('name')
	return render(request, 'equipments/equipment_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def equipment_add(request):
	context = {}

	if request.method == 'POST':
		context['equipment_form'] = EquipmentForm(request.POST,request.FILES)

		if context['equipment_form'].is_valid():
			equipment_obj = context['equipment_form'].save(commit=False)
			equipment_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Cliente creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Cliente creado satisfactorimente')

			return redirect('equipments:equipment_list')
	else:
		context['equipment_form'] = EquipmentForm()
	
	return render(request, 'equipments/equipment_form.html', context)
	
@login_required
@has_permissions('can_edit_' + app_name)
def equipment_edit(request, pk):
	context = {}

	context['equipment_obj'] = get_object_or_404(Equipment, pk=pk)

	if request.method == 'POST':
		context['equipment_form'] = EquipmentForm(request.POST,request.FILES, instance=context['equipment_obj'])

		if context['equipment_form'].is_valid():
			equipment_obj = context['equipment_form'].save(commit=False)	
			equipment_obj.save()

			msg = messages.success(request, 'Cliente modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Cliente modificado satisfactorimente')

			return redirect('equipments:equipment_list')
	else:
		context['equipment_form'] = EquipmentForm(instance=context['equipment_obj'])

	return render(request, 'equipments/equipment_form.html', context)

@login_required
@has_permissions('can_delete_' + app_name)
def equipment_delete(request, pk):
	equipment = get_object_or_404(Equipment, pk=pk)

	data = delete_record(request, equipment, '/panel/equipments/', 'equipments:equipment_delete')
	return JsonResponse(data)