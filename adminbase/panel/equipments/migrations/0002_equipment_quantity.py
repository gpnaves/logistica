# Generated by Django 3.0.5 on 2020-04-23 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipments', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='quantity',
            field=models.SmallIntegerField(default=1, verbose_name='cantidad'),
        ),
    ]
