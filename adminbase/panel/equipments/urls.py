from django.conf.urls import url
from .views import equipments

urlpatterns = [
    url(r'^$', equipments.equipment_list, name='equipment_list'),
	url(r'^add/$', equipments.equipment_add, name='equipment_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', equipments.equipment_edit, name='equipment_edit'),
	url(r'^delete/(?P<pk>[0-9]+)/$', equipments.equipment_delete, name='equipment_delete'),
]