from django.conf.urls import url
from panel.login.views import authorization

urlpatterns = [
	
	#-- Dashboard
	url(r'^$', authorization.dashboard, name='dashboard'),
	#-- Login
    url(r'^login/$', authorization.login_account, name='login'),
    #-- Logout
    url(r'^logout/$', authorization.logout_account, name='logout'),

    url(r'^reset/$', authorization.reset_password, name='password_reset'),
#     url(r'^reset/done/$',
#         auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'),
#         name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        authorization.reset_password_confirm, name='password_reset_confirm')
#     url(r'^reset/complete/$',
#         auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'),
#         name='password_reset_complete'),
]