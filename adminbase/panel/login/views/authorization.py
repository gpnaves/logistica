from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from django.core.cache import cache 
from panel.core.authentication_email import EmailAuthBackend
from panel.core.utils import user_logs
from panel.core.context_processors import menu_user
from panel.accounts.models import User
from panel.login.forms import LoginForm, ResetPasswordForm, MenuSearchForm
from panel.routes.models import Route
from django.contrib import messages

from django.db.models import Count


# Create your views here.
def login_account(request):
	context = {}

	if request.user.is_authenticated:
		return redirect('authorization:dashboard')
	else:
		if request.method == 'POST':
			context['form_login'] = LoginForm(request.POST)

			if context['form_login'].is_valid():
				user = EmailAuthBackend.authenticate(None,email=context['form_login'].cleaned_data['email'], \
									password=context['form_login'].cleaned_data['password'])
				if user is not None:
					if user.is_active and user.status:
						login(request, user)

						#-- Delete Redis cache
						cache.delete('menu_%s' % (request.user.pk))
						cache.delete('menu_%s' % (request.user.pk))
						cache.delete('datapanel')

						msg = messages.success(request, 'Bienvenido a Logistica Naves')

						#-- User Logs (Info, Access, Error)
						user_logs(request,None,'A','Acceso al sistema')

						if request.GET.get('next'):
							return redirect(request.GET.get('next'))
						else:
							return redirect('authorization:dashboard')
					else:
						context['message'] = 'Usuario o contraseña incorrectos'

						#-- User Logs (Info, Access, Error)
						user_logs(request,user,'E','Usuario o contraseña incorrectos')
				else:
					context['message'] = 'Usuario o contraseña incorrectos'
			else:
				context['message'] = 'Datos incorrectos'
				
		else:
			context['form_login'] = LoginForm()
	return render(request, 'login/login.html', context)

def dashboard(request):
	context = menu_user(request)
	context['menu_form'] = MenuSearchForm()

	source = Route.objects.all().values('source__region__code', 'source__region__name').annotate(quantity=Count('*')).order_by('quantity')
	target = Route.objects.all().values('target__region__code', 'target__region__name').annotate(quantity=Count('*')).order_by('quantity')
	
	data_source = []
	total_quantity = 0
	for s in source:
		total_quantity += s['quantity']

	for s in source:
		percentage = s['quantity'] / total_quantity * 100
		dicc = {
			'name': s['source__region__name'],
			'code': s['source__region__code'], 
			'quantity': s['quantity'],
			'percentage': round(percentage, 2)
			}
		if percentage <= 1: dicc['category'] = 1
		elif percentage <= 5: dicc['category'] = 2
		elif percentage <= 10: dicc['category'] = 3
		elif percentage <= 15: dicc['category'] = 4
		elif percentage <= 20: dicc['category'] = 5
		elif percentage <= 30: dicc['category'] = 6
		elif percentage <= 50: dicc['category'] = 7
		data_source.append(dicc)
		
	data_target = []
	total_quantity = 0
	for t in target:
		total_quantity += t['quantity']

	for t in target:
		percentage = t['quantity'] / total_quantity * 100
		dicc = {
			'name': t['target__region__name'],
			'code': t['target__region__code'], 
			'quantity': t['quantity'],
			'percentage': round(percentage, 2)
			}
		if percentage <= 1: dicc['category'] = 1
		elif percentage <= 5: dicc['category'] = 2
		elif percentage <= 10: dicc['category'] = 3
		elif percentage <= 15: dicc['category'] = 4
		elif percentage <= 20: dicc['category'] = 5
		elif percentage <= 30: dicc['category'] = 6
		elif percentage <= 50: dicc['category'] = 7
		data_target.append(dicc)
	
	context['data_source'] = data_source
	context['data_target'] = data_target

	# if request.user_agent.is_mobile:
	# 	context['is_mobile'] = True
	# else:
	# 	context['is_mobile'] = False
	return render(request, 'login/dashboard.html', context)

def logout_account(request):

	#-- User Logs (Info, Access, Error)
	user_logs(request,None,'A','Cierre de Sesión')

	logout(request)

	return redirect('authorization:login')

def reset_password(request):
	context = {}
	context['reset_password_form'] = ResetPasswordForm(request.POST or None)


	return render(request, 'login/password_reset.html', context)

def reset_password_confirm(request):
	pass
