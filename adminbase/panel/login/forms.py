from django import forms
from panel.config.models import MenuItems

class LoginForm(forms.Form):
	email = forms.EmailField(
		label = 'Email',
		error_messages = {'required' : 'Email incorrecto'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control form-control-user',
				'aria-describedby' : "emailHelp",
				'placeholder' : 'Ingrese su email...',
			}
		)
	)
	password = forms.CharField(
		label = 'Contraseña',
		error_messages = {'required' : 'Contraseña incorrecta'},
		widget = forms.PasswordInput(
			attrs = {
				'class' : 'form-control form-control-user',
				'placeholder' : 'Contraseña'
			}
		)
	)

class ResetPasswordForm(forms.Form):
	email = forms.EmailField(
		error_messages = {'required' : 'Email incorrecto'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control form-control-user',
				'aria-describedby' : "emailHelp",
				'placeholder' : 'Ingrese su email...',
			}
		)
	)


class MenuSearchForm(forms.Form):
	menu = forms.ModelChoiceField(
		label='Buscar',
		required = False,
		queryset = MenuItems.objects.all(),
		empty_label = 'Buscar...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	