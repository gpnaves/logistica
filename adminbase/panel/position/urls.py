from django.conf.urls import url
from .views import positions

urlpatterns = [
    url(r'^$', positions.position_list, name='position_list'),
	url(r'^add/$', positions.position_add, name='position_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', positions.position_edit, name='position_edit'),
	url(r'^delete/(?P<pk>[0-9]+)/$', positions.position_delete, name='position_delete'),
]