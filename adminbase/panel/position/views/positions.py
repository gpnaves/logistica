from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from ..models import Position
from ..forms import PositionForm

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def position_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['position_list'] = Position.objects.all().order_by('title')

	return render(request, 'positions/position_list.html', context)
	
@login_required
@has_permissions('can_add_' + app_name)
def position_add(request):
	context = {}
	data = dict()

	if request.is_ajax and request.method == 'POST':
		context['position_form'] = PositionForm(request.POST or None)
		if context['position_form'].is_valid():
			context['position_form'].save()

			data['form_is_valid'] = True
			messages.success(request, 'Posicion creada satisfactoriamente')
			user_logs(request, None, 'I', 'Posicion creada satisfactoriamente')

			data['url_redirect'] = '/panel/position/'
		else:
			data['form_is_valid'] = False
	else:
		context['position_form'] = PositionForm()
	
	
	data['html_form'] = render_to_string('positions/position_form.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
@has_permissions('can_edit_' + app_name)
def position_edit(request, pk):
	context = {}
	data = dict()

	context['position_obj'] = get_object_or_404(Position, pk=pk)

	if request.is_ajax and request.method == 'POST':
		context['position_form'] = PositionForm(request.POST or None, instance=context['position_obj'])
		if context['position_form'].is_valid():
			context['position_form'].save()

			data['form_is_valid'] = True
			messages.success(request, 'Posicion creada satisfactoriamente')
			user_logs(request, None, 'I', 'Posicion creada satisfactoriamente')

			data['url_redirect'] = '/panel/position/'
		else:
			data['form_is_valid'] = False
	else:
		context['position_form'] = PositionForm(instance=context['position_obj'])
	
	
	data['html_form'] = render_to_string('positions/position_form.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
@has_permissions('can_delete_' + app_name)
def position_delete(request, pk):
	position = get_object_or_404(Position, pk=pk)

	data = delete_record(request, position, '/panel/position/', 'positions:position_delete')
	return JsonResponse(data)