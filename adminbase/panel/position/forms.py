from django import forms

from .models import Position

class PositionForm(forms.ModelForm):

	title = forms.CharField(
		label = 'Nombre',
		error_messages = {'required': 'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	description = forms.CharField(
		label='Descripcion',
        max_length=1000,
		error_messages = {'required' : 'Debe capturar una breve descripcion'},
		widget = forms.Textarea(
			attrs = {
				'class' : 'form-control',
                'rows' : 4,
			}
        )
	)

	class Meta:
		model = Position
		fields = ('title', 'description')