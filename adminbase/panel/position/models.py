from django.db import models

# Create your models here.

class Position(models.Model):
    title = models.CharField(verbose_name='Título', max_length=200)
    description = models.CharField(verbose_name='Descripcion', max_length=1000)

    class Meta:
        db_table = 'position'
        verbose_name = 'Puesto'
        verbose_name_plural = 'Puestos'

    def __str__(self):
        return self.title