# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.hashers import check_password
from panel.accounts.models import User
from django.contrib.auth.backends import BaseBackend

class EmailAuthBackend(BaseBackend):
	"""
	Authenticate using e-mail account.
	"""
	def authenticate(self, email=None, password=None):
		try:
			user = User.objects.get(email=email)
			if user.check_password(password):
				return user
			return None
		except User.DoesNotExist:
			return None

	def get_user(self, user_id):
		try:
			return User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None
