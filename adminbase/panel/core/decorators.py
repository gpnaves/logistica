# -*- coding: utf-8 -*-
import functools
import time

from django.conf import settings
from django.http import HttpResponseBadRequest
from django.shortcuts import redirect
from django.urls import resolve
from django.shortcuts import get_object_or_404

from panel.accounts.models import User, UserRole, UserMenuGroup


#--- Request only if is Ajax
def ajax_required(f):
	def wrap(request, *args, **kwargs):
		if not request.is_ajax():
			return HttpResponseBadRequest()
		
		return f(request, *args, **kwargs)

	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	
	return wrap

#-- Method only for Superuser
def access_for_superuser(function):
    def wrap(request, *args, **kwargs):
        user = get_object_or_404(User,pk=request.user.pk)
        if user.is_superuser:
            return function(request, *args, **kwargs)
        else:
            return redirect('authorization:login')

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    wrap.__module__ = function.__module__

    return wrap

#-- Method only for User
def access_for_user(function):
    def wrap(request, *args, **kwargs):
        user = get_object_or_404(User,pk=kwargs['pk'])
        if not user.is_superuser or request.user.is_superuser:
            return function(request, *args, **kwargs)
        else:
            return redirect('accounts:accounts_list')

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    wrap.__module__ = function.__module__
    return wrap

#-- Method only for User
def has_permissions(perm):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            if request.user.is_superuser:
                return view_func(request, *args, **kwargs)
            if request.user.user_groups.exists():
                permissions = UserMenuGroup.objects.filter(user_type=request.user.user_type)

                for permission in permissions:
                    if perm == permission.permission_code:
                        return view_func(request, *args, **kwargs)
                return HttpResponseBadRequest()
            else:
                return HttpResponseBadRequest()
        return wrapper_func
    return decorator


#-- Method only for User
def login_required():
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            if request.user.is_superuser or request.user.is_authenticated():
                return view_func(request, *args, **kwargs)     
            else:
                return redirect('authorizarion:login')           
        return wrapper_func
    return decorator

def debugger_queries(func):
    """Basic function to debug queries."""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print("func: ", func.__name__)
        reset_queries()

        start = time.time()
        start_queries = len(connection.queries)

        result = func(*args, **kwargs)

        end = time.time()
        end_queries = len(connection.queries)

        print("queries:", end_queries - start_queries)
        print("took: %.2fs" % (end - start))
        return result

    return wrapper