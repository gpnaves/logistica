from django import template
from django.conf import settings
from django.utils.html import format_html
from django.db.models import FileField, ImageField

register = template.Library()

#-- Split Text with character given
def split_video(value, arg):
	text = value.split(arg)
	
	return text[4]

register.filter('splitvideo', split_video)

def range_count(min=1):
    value = int(min) + 1

    return range(1,value)

register.filter('range_count', range_count)



def instance_form_field(instance):
    field_list = []
    class_names = ['ManyToOneRel','AutoField','ForeignKey','ManyToManyField','CreationDateTimeField','ModificationDateTimeField']
    for field in instance._meta.get_fields():
        field_class = field.__class__.__name__
        if field_class not in class_names:
            field_name = str(field).split(".").pop()
            if isinstance(field,ImageField) or isinstance(field,FileField):
                field_list.append(format_html("<div class='mr-lg-3 mb-3 mb-lg-0'>\
									             <a href='{}{}' data-popup='lightbox'>\
									               <img src='{}{}' height='60'>\
									             </a></div>",settings.MEDIA_URL,getattr(instance, field_name)\
                                               ,settings.MEDIA_URL,getattr(instance, field_name)
                                    )
                                 )
            else:
                field_list.append(getattr(instance, field_name))
    return field_list    

register.filter('instance_form_field',instance_form_field)

def instance_file_field(instance):
    files_list = []
    class_names = ['ManyToOneRel','ForeignKey','AutoField','ManyToManyField']
    for field in instance._meta.get_fields():
        field_class = field.__class__.__name__
        if field_class not in class_names:
            field_name = str(field).split(".").pop()
    
            if isinstance(field,FileField) or isinstance(field,ImageField):
                files_list.append(getattr(instance, field_name))

    return files_list

register.filter('instance_file_field', instance_file_field)
def split(string, sep):
    """Return the string split by sep.

    Example usage: {{ value|split:"/" }}
    """
    return string.split(sep)

register.filter('split', split)

@register.filter
def get_at_index(list, index):
    return list[index]

@register.filter()
def startswith(text, starts):
    if isinstance(text, str):
        return text.startswith(starts)
    return False
