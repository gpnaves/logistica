#-- Author: Hugo Camargo <ratedchr@gmail.com>

from django.http import HttpResponseRedirect,HttpResponseBadRequest
from django.conf import settings
from django.shortcuts import redirect, get_object_or_404
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.urls import resolve
from re import compile

from panel.config.models import MenuItems, MenuSubItems
from panel.core.context_processors import menu_user, menu_top

#from django.contrib.auth import get_user_model
#django.contrib.auth.models.User.get_full_name()

EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]

class AccessRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_request(self, request):

        if request.user.is_authenticated and not request.user.is_anonymous:
            path = request.path_info.lstrip('/')

            myfunc, myargs, mykwargs = resolve(request.path)
            mymodule = myfunc.__module__

            module_name = mymodule.split(".").pop()

            if not request.user.is_superuser:
                if not any(m.match(path) for m in EXEMPT_URLS):
                    menu = cache.get('menu_%s' % (request.user.pk))
      
                    menut = cache.get('menu_top_%s' % (request.user.pk))
                    
                    if not menu:
                        menu = menu_user(request,True)
                    if not menut:
                        menut = menu_top(request,True)

                    if path != 'panel/' and path != 'panel/logout/' and path != 'panel/profile/' and path != 'panel/profile/changepassword/':
                        has_perms = False 

                        #-- check if module is True
                        module_access = True
                        try:
                            module_obj = MenuItems.objects.get(module=module_name)
                            if not module_obj.is_module:
                                module_access = False
                        except MenuItems.DoesNotExist:
                             pass
                        if module_access:
                            for item in menu:
                                if module_name == item.menu_item.module:
                                    has_perms = True
                                elif item.menu_subitems is not None:
                                    if module_name == item.menu_subitems.module:
                                        has_perms = True
      
                            for item in menut:
                                if module_name == item.menu_item.module:
                                    has_perms = True
                                elif item.menu_subitems is not None:
                                    if module_name == item.menu_subitems.module:
                                        has_perms = True

                            #-- Has not permission to the module
                            if not has_perms:
                                raise PermissionDenied
                                

            
