# -*- coding: utf-8 -*-
from django.core.cache import cache
from panel.config.models import PanelAdmin

from panel.config.models import MenuItems
from panel.accounts.models import UserMenuPermission, User, UserRole, UserMenuGroup


#-- Data Panel on the CMS
def data_panel(request):
    context = {}

    
    #-- Get menu in cache
    panel = cache.get('datapanel')
    
    if not panel:
        panel = PanelAdmin.objects.first()
        cache.set('datapanel',panel)
    context['datapanel'] = panel

    return context

#-- List of User´s Modules
def menu_user(request,middle=None):
    context = {}

    if request.user.is_authenticated:
        user = User.objects.get(pk=request.user.pk)

        #-- Get menu in cache
        menu = cache.get('menu_%s' % (request.user.pk))

        if not menu:
            if request.user.is_superuser:
                menu = MenuItems.objects.filter(status=True,is_module=True,is_global=False).order_by('order')
            else:   
                if user.user_groups.exists():
                    menu = UserMenuGroup.objects.select_related('user_type','menu_item','menu_subitems')\
                                        .filter(user_type__user=request.user,menu_item__is_global=False).order_by('menu_item__order','menu_item__title','menu_subitems__order')
                else:
                    menu = UserMenuPermission.objects.filter(user=request.user,menu_item__is_global=False).order_by('menu_item__order','menu_item__title','menu_subitems__order')

            cache.set('menu_%s' % (request.user.pk),menu)
        context['menu_user'] = menu
    if middle:
        return menu
    else:
        return context

#-- List of User´s Modules
def menu_top(request,middle=None):
    context = {}

    if request.user.is_authenticated:
        user = User.objects.get(pk=request.user.pk)

        #-- Get menu in cache
        menu = cache.get('menu_top_%s' % (request.user.pk))
        if not menu:
            if request.user.is_superuser:
                menu = MenuItems.objects.filter(status=True,is_module=True,is_global=True).order_by('order')
            else:   
                if user.user_groups.exists():
                    menu = UserMenuGroup.objects.select_related('user_type','menu_item','menu_subitems')\
                                        .filter(user_type__user=request.user,menu_item__is_global=True).order_by('menu_item__order')
                else:
                    menu = UserMenuPermission.objects.filter(user=request.user,menu_item__is_global=True).order_by('menu_item__order')
            cache.set('menu_top_%s' % (request.user.pk),menu)
            
        context['menu_top'] = menu
    if middle:
        return menu
    else:
        return context

