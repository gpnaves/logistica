# -*- coding: utf-8 -*-
import os
from datetime import datetime


import xlwt
#import weasyprint
import magic

from django.conf import settings
from django.utils import timezone
from django.core.files.storage import FileSystemStorage, Storage
from django.core.files.base import ContentFile
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from django.core.mail import EmailMessage, send_mail, BadHeaderError, EmailMultiAlternatives
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpRequest

from django.core.cache import cache
from django.utils.cache import get_cache_key
from django.urls import reverse
from panel.config.models import Logger

# -- Rename filename with datetime format


def get_filename(extension):
    ts = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    return '%s%s' % (ts, extension)

# -- Sendmail with html template


def sendmail(subject, html_content, from_email, to_email):
    # -- Sendmail: (Subjetc, body, email user, email host, False)

    try:
        #msg = EmailMessage(subject, html_content, from_email, [to_email])
        if type(to_email) == list:
            msg = EmailMultiAlternatives(
                subject, html_content, from_email, to_email)
        else:
            msg = EmailMultiAlternatives(
                subject, html_content, from_email, [to_email])
        # msg.content_subtype = "html"  # Main content is now text/html
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
    except BadHeaderError:
        return HttpResponse('Error al enviar el correo')
    return True

# -- Sendmail with html template and attachments


def sendmail_attachs(subject, html_content, from_email, to_email, files):
    # -- Sendmail: (Subjetc, body, email user, email host, False)
    try:
        files_list = []

        for k, f in files.items():

            files_list.append(
                (f.name, f.read(), magic.from_buffer(f.read(), mime=True)))

        msg = EmailMessage(subject, html_content, from_email, [
                           to_email], attachments=files_list)
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send(fail_silently=True)
    except BadHeaderError:
        return HttpResponse('Error al enviar el correo')
    return True


#-- Pagination
"""
	from adminpanel.core.utils import pagination

	list = User.objects.all()
	page = request.GET.get('page', 1)

	objects_list = pagination(list, page, 10)
	/////////////////////////////////////////
  
	* Build a lazy html ajax data */
	
	list = User.objects.all()
	page = request.GET.get('page', 1)

	objects_list = pagination(list, page, 10)

	# custom html with style
	lazy_data = render_to_string('posts/post_html.html', {'post_list': objects_list})
  
	# package output data and return it as a JSON object
	output_data = {'lazy_html': lazy_data, 'has_next': objects_list.has_next()}
  
	return JsonResponse(output_data)
	
	# HTML tags
	<div id="lazy_div"></div>
	<div><a id="lazyLoadLink" href="javascript:void(0);" data-page="2" data-url="{% url namespace:name %}">Ver Mas</a></div>
	
"""


def expire_page_cache(view, args=None):
    """
    Removes cache created by cache_page functionality. 
    Parameters are used as they are in reverse()
    """

    if args is None:
        path = reverse(view)
    else:
        path = reverse(view, args=args)

    request = HttpRequest()
    request.path = path
    key = get_cache_key(request)
    if cache.has_key(key):
        cache.delete(key)


def pagination(instance, page, num):
    paginator = Paginator(instance, num)

    try:
        obj_list = paginator.page(page)
    except PageNotAnInteger:
        obj_list = paginator.page(1)
    except EmptyPage:
        obj_list = paginator.page(paginator.num_pages)

    return obj_list


def storage_file(path, filename, name):
    context = {}

    FILE_DIR = '/home/vagrant/files_ccvta/%s' % (path)

    # create FileSystemStorage with location as base directory
    fileStorage = FileSystemStorage(location=FILE_DIR)

    # create content in memory file
    file = ContentFile(filename)

    # save file
    # It returns the name of file which gets stored.
    # Returned fileName could be different from input fileName, when file does exist already.
    try:
        newFileName = fileStorage.save(name, file)
    except Exception as e:
        print(e.__str__())
        return

    # return name of the saved file
    return newFileName


def user_logs(request, user, logtype, message):

    if user is None:
        user = request.user

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    log = Logger.objects.create(user=user, ip=ip,
                                sessionkey=request.COOKIES.get(
                                    settings.SESSION_COOKIE_NAME, ''),
                                logtype=logtype, message=message, url=request.path)

    return log


def confirm_record(request, instance, url, namespace):
    context = {}
    data = {}

    if request.is_ajax and request.method == 'POST':
        messages.success(request, 'Registro confirmado satisfactoriamente')

        # -- User Logs (Info, Access, Error)
        user_logs(request, None, 'I', 'Registro confirmado satisfactoriamente')

        data['form_is_valid'] = True
        data['url_redirect'] = url

    else:
        # -- Parameters modal form
        context['obj'] = instance
        context['url_post'] = namespace

        data['html_confirm'] = render_to_string(
            'core/snippets/modal_confirm.html', context, request=request)

    return data


def delete_record(request, instance, url, namespace):

    context = {}
    data = {}

    if request.is_ajax and request.method == 'POST':
        instance.status = False
        instance.save()

        messages.success(request, 'Registro eliminado satisfactoriamente')

        # -- User Logs (Info, Access, Error)
        user_logs(request, None, 'I', 'Registro eliminado satisfactoriamente')

        data['form_is_valid'] = True
        data['url_redirect'] = url

    else:
        # -- Parameters modal form
        context['obj_delete'] = instance
        context['url_post'] = namespace

        data['html_delete'] = render_to_string(
            'core/snippets/modal_delete.html', context, request=request)

    return data


def sendmail_activation_record(request, instance, password, url, namespace):

    context = {}
    data = {}

    if request.is_ajax and request.method == 'POST':

        # -- Call static method
        instance.activation_email(request, instance, password)

        messages.success(request, 'Correo de activación satisfactoriamente')

        # -- User Logs (Info, Access, Error)
        user_logs(request, None, 'I',
                  'Correo de activación satisfactoriamente')

        data['form_is_valid'] = True
        data['url_redirect'] = url

    else:
        # -- Parameters modal form
        context['obj_activation'] = instance
        context['url_post'] = namespace

        data['html_sendmail'] = render_to_string(
            'core/snippets/modal_sendmail.html', context, request=request)

    return data


def delete_item(request, instance, namespace, message, url):
    context = {}
    data = {}

    if request.is_ajax and request.method == 'POST':
        data['obj_pk'] = instance.pk

        if hasattr(instance, 'image'):
            instance.image.delete()

        instance.delete()

        messages.success(request, message)

        # -- User Logs (Info, Access, Error)
        user_logs(request, None, 'A', message)

        data['form_is_valid'] = True
        data['url_redirect'] = url

    else:

        # -- Parameters modal form
        context['obj_delete'] = instance
        context['url_post'] = namespace

        data['html_delete'] = render_to_string(
            'core/snippets/modal_delete.html', context, request=request)

    return data


def cancel_status(request, instance, url, namespace):
    context = {}
    data = {}

    if request.is_ajax and request.method == 'POST':
        instance.status = 'C'
        instance.save()

        messages.success(request, 'Registro cancelado satisfactoriamente')

        data['form_is_valid'] = True
        data['url_redirect'] = url

    else:

        # -- Parameters modal form
        context['obj_cancel'] = instance
        context['url_post'] = namespace

        data['html_delete'] = render_to_string(
            'core/snippets/modal_cancel.html', context, request=request)

    return data


def export_xls_file(filename, sheet, headers, qs):
    response = HttpResponse(content_type='application/ms-excel')
    # -- File name
    response['Content-Disposition'] = 'attachment; filename="%s.xls"' % (
        filename)
    # -- Add Workbook and sheet
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(sheet)

    # Sheet header, first row
    row_num = 0
    # -- Headers Style
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    # -- Header in the first row
    columns = headers

    # -- Create Headers
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    # -- Set Queryset to rows
    rows = qs
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)

    return response

# def export_pdf_file(request,html_template,context,filename):

# 	html = render_to_string(html_template, context)
# 	response = HttpResponse(content_type='application/pdf')
# 	response['Content-Disposition'] = 'filename="{0}.pdf"'.format(filename)

# 	weasyprint.HTML(string=html, base_url=request.build_absolute_uri('/')[:-1]).write_pdf(response)

# 	return response
