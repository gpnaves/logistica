
$('#lazyLoadLink').on('click', function() {
    var link = $(this);
    var page = link.data('page');
    var url = link.data('url');

    $.ajax({
        type: 'post',
        url: url,
        data: {
        'page': page
        },
        success: function(data) {
            // if there are still more pages to load,
            // add 1 to link's page data attribute
            // else hide the link
            if (data.has_next) {
                link.data('page', page+1);
            } else {
                link.hide();
            }
            // append html to the lazy div
            $('#lazy_div').append(data.lazy_html).animate({
                opacity: "show"
            }, {
                duration: "slow",
                easing: "easein"
            })
        },
        error: function(xhr, status, error) {
            // shit happens friends!
        }
    });
});