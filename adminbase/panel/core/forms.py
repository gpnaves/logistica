from django import forms

from panel.notifications.models import Notification

from django_summernote.widgets import SummernoteWidget


class MailForm(forms.ModelForm):
	to_mail = forms.CharField(
		label = 'Para:',
		error_messages = {'required':'Debe ingresar el destinatario'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control'
			}
		)
	 )
	from_mail = forms.CharField(
		label = 'De:',
		error_messages = {'required':'Debe ingresar el remitente'},
		widget = forms.HiddenInput(
			attrs = {
				'class': 'form-control'
			}
		)
	 )

	subject = forms.CharField(
		label = 'Asunto:',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control'
			}
		)
	 )

	message = forms.CharField(
		label = 'Mensaje:',
		required = False,
		widget = forms.Textarea(
			attrs = {
				'class' : 'form-control',
                'rows' : 5,
			}
		)
	)

	class Meta:
		 model = Notification
		 fields = ('subject', 'message')