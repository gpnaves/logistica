# -*- coding: utf-8 -*-
from django.shortcuts import render,redirect,get_object_or_404
from django.template.loader import render_to_string
from django.contrib import messages
from django.http import JsonResponse

from panel.core.utils import user_logs,delete_record
from panel.accounts.models import UserRole
from panel.accounts.forms import UserRoleForm

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.
app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def roles_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['roles_list'] = UserRole.objects.filter(status=True).order_by('name')

	return render(request,'accounts/roles_list.html',context)

@login_required
@has_permissions('can_add_' + app_name)
def roles_add(request):
	context = {}

	if request.method == 'POST':
		context['roles_form'] = UserRoleForm(request.POST)
		
		if context['roles_form'].is_valid():
			context['roles_form'].save()
			
			#-- Message to user
			messages.success(request, 'Rol de usuario creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Rol de usuario creado satisfactoriamente')

			return redirect('accounts:roles_list')
	else:
		context['roles_form'] = UserRoleForm()

	return render(request, 'accounts/roles_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def roles_edit(request,pk):
	context = {}

	context['roles_obj'] = get_object_or_404(UserRole, pk=pk)

	if request.method == 'POST':
		context['roles_form'] = UserRoleForm(request.POST, instance=context['roles_obj'])
		
		if context['roles_form'].is_valid():
			context['roles_form'].save()
			
			#-- Message to user
			messages.success(request, 'Rol de usuario modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Rol de usuario modificado satisfactoriamente')

			return redirect('accounts:roles_list')
	else:
		context['roles_form'] = UserRoleForm(instance=context['roles_obj'])

	return render(request, 'accounts/roles_form.html', context)

@login_required
@has_permissions('can_delete_' + app_name)
def roles_delete(request,pk):
	role = get_object_or_404(UserRole, pk=pk)

	data = delete_record(request,role,'/panel/accounts/roles/','accounts:roles_delete')

	if data:

		#-- Delete group permissions
		role.group.all().delete()

		#-- Delete relations users with group
		role.user.clear()

		#-- Set null all user types in user
		role.users.update(user_type=None)

	return JsonResponse(data)	