# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.crypto import get_random_string
from django.contrib import messages
from django.template.loader import render_to_string
from django.http import JsonResponse

from panel.core.utils import user_logs,delete_record,sendmail_activation_record
from panel.core.decorators import access_for_user
from panel.core.forms import MailForm

from panel.config.models import Logger
from panel.accounts.models import User,UserRole,UserMenuPermission
from panel.accounts.forms import UserForm

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_add_' + app_name)
def accounts_add(request):
	context = {}

	#-- List of menu modules
	context['menu_list'] = User.modules_list()

	if request.method == 'POST':
		context['profile_form'] = UserForm(request.POST,user=request.user)
		if context['profile_form'].is_valid():
			
			user = context['profile_form'].save(commit=False)
			password = get_random_string(10)
			user.set_password(password)
			user.save()

			user_role = context['profile_form'].cleaned_data['user_type']

			#-- If user role is a group
			if user_role.is_group:
				#-- Save relation User/ Group
				user_role.user.add(user)
			else:
				User.save_modules_list(request.POST.getlist('modules_user'),user,False)

			#-- Call static method
			User.activation_email(request,user,password)

			#-- Message to user
			messages.success(request, 'Usuario creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Usuario creado satisfactorimente')

			return redirect('accounts:accounts_list')
	else:
		 		
		context['profile_form'] = UserForm(user=request.user)
	
	return render(request, 'accounts/accounts_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def accounts_edit(request, pk):

	context = {}
	
	context['user_profile'] = get_object_or_404(User, pk=pk)

	current_usertype = context['user_profile'].user_type
	
	#-- List of menu modules
	context['menu_list'] = User.modules_list()

	if request.method == 'POST':
		context['profile_form'] = UserForm(request.POST, request.FILES, instance=context['user_profile'],\
											   user=context['user_profile'])

		if context['profile_form'].is_valid():

			#-- If user is in group
			user_role = context['profile_form'].cleaned_data['user_type']

			if user_role.is_group:
				UserMenuPermission.objects.filter(user=context['user_profile']).delete()
				context['user_profile'].user_groups.remove(current_usertype)
				user_role.user.add(context['user_profile'])
			else:
				context['user_profile'].user_groups.remove(current_usertype)
				User.save_modules_list(request.POST.getlist('modules_user'),context['user_profile'],True)

			context['profile_form'].save()

			messages.success(request, 'Usuario modificado satisfactoriamente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Usuario modificado satisfactoriamente')

			return redirect('accounts:accounts_list')
	else:
		
		#-- Get User Role
		if context['user_profile'].user_type.is_group:
			context['usertype_select'] = 'true'
		else:
			context['usertype_select'] = 'false'

		context['profile_form'] = UserForm(instance=context['user_profile'], user=context['user_profile'])
	
	return render(request, 'accounts/accounts_form.html', context)

@login_required
@has_permissions('can_view_' + app_name)
def accounts_list(request):
	context = {}
	
	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['user_list'] = User.objects.filter(status=True).exclude(is_superuser=True)

	return render(request, 'accounts/accounts_list.html', context)

@login_required
@has_permissions('can_delete_' + app_name)
def accounts_delete(request, pk):
	
	user = get_object_or_404(User, pk=pk)

	data = delete_record(request,user,'/panel/accounts/','accounts:accounts_delete')

	return JsonResponse(data)	

@login_required
def accounts_sendmail(request, pk):
	context = {}
	data = dict()

	user = get_object_or_404(User, pk=pk)

	context["from_mail"] = request.user.email
	context["to_mail"] = user.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)
			mail_obj.user = user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/accounts/'
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 

def accounts_sendmail_activation(request, pk):
	
	user = get_object_or_404(User, pk=pk)

	password = get_random_string(10)
	user.set_password(password)
	user.save()

	data = sendmail_activation_record(request,user,password,'/panel/accounts/','accounts:accounts_sendmail_activation')

	return JsonResponse(data)	

def accounts_activation(request, uidb64, token):

	user_activation = User.activation_url(uidb64,token)

	if user_activation: 
		
		#-- User Logs (Info, Access, Error)
		user_logs(request,user_activation,'A','Usuario activado satisfactoriamente')

		return render(request, 'accounts/accounts_activation.html')
	else:
		return render(request, 'accounts/accounts_expire.html')

@login_required
def accounts_logs(request,pk):
	context = {}

	user = get_object_or_404(User, pk=pk)
	
	context['logs_info'] = Logger.objects.filter(user=user,logtype='I').order_by('-created')
	context['logs_access'] = Logger.objects.filter(user=user,logtype='A').order_by('-created')
	context['logs_error'] = Logger.objects.filter(user=user,logtype='E').order_by('-created')

	return render(request, 'accounts/logs_list.html', context)

def accounts_userrole(request):
	data = {}
	
	pk = request.GET.get('user_type')
	group = get_object_or_404(UserRole, pk=pk)
	
	data['is_group'] = group.is_group
	
	return JsonResponse(data)	
