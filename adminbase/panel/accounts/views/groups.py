# -*- coding: utf-8 -*-
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib import messages
from django.http import JsonResponse
from django.template.loader import render_to_string

from panel.core.utils import user_logs,delete_record
from panel.accounts.models import User,UserRole,UserMenuGroup
from panel.accounts.forms import UserMenuGroupForm
from panel.config.models import MenuItems, MenuSubItems

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

# Create your views here.
app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def userpermissions_list(request):
	context = {}

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['permissions_list'] = UserRole.objects.order_by('name')

	#context['permissions_list'] = UserMenuGroup.objects.values('user_type__pk','user_type__name').annotate(total=Count('menu_item')).\
	#                         filter(status=True).order_by('user_type__name')

	return render(request,'accounts/groups_list.html',context)

@login_required
@has_permissions('can_add_' + app_name)
def userpermissions_add(request):
	context = {}

	#-- List of menu modules
	context['menu_list'] = User.modules_list()

	if request.method == 'POST':
		context['group_form'] = UserMenuGroupForm(request.POST)
		
		if context['group_form'].is_valid():

			User.save_modules_group(request.POST.getlist('modules_group'),context['group_form'],True)
			
			#-- Message to user
			messages.success(request, 'Permisos de usuario creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Permisos de usuario creado satisfactoriamente')

			return redirect('accounts:roles_list')
	else:
		context['group_form'] = UserMenuGroupForm()

	return render(request, 'accounts/groups_options_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def userpermissions_edit(request,pk):
	context = {}

	context['user_group_list'] = UserMenuGroup.objects.filter(user_type__pk=pk)

	#-- List of menu modules
	context['menu_list'] = User.modules_list()

	if request.method == 'POST':
		context['group_form'] = UserMenuGroupForm(request.POST, instance=context['user_group_list'].first())
		
		if context['group_form'].is_valid():

			User.save_modules_group(request.POST.getlist('modules_group'),context['group_form'],True)
			
			#-- Message to user
			messages.success(request, 'Permisos de usuario modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Permisos de usuario modificado satisfactoriamente')

			return redirect('accounts:userpermissions_list')
	else:
		context['group_form'] = UserMenuGroupForm(instance=context['user_group_list'].first())

	return render(request, 'accounts/groups_options_form.html', context)

@login_required
def userpermissions_options(request, pk):
	context = {}

	#-- List of menu modules
	context['user_group_list'] = UserMenuGroup.objects.filter(user_type__pk=pk)

	if request.method == 'POST':
		context['group_form'] = UserMenuGroupForm(request.POST)
		
		if context['group_form'].is_valid():

			User.save_modules_group(request.POST.getlist('modules_group'),context['group_form'],False)
			
			#-- Message to user
			messages.success(request, 'Permisos de usuario creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Permisos de usuario creado satisfactoriamente')

			return redirect('accounts:userpermissions_list')
	else:
		context['group_form'] = UserMenuGroupForm(instance=context['user_group_list'].first())

	return render(request, 'accounts/groups_options_form.html', context)

@login_required
@has_permissions('can_delete_' + app_name)
def userpermissions_delete(request,pk):

	context = {}
	data = {}

	context['user_role'] = get_object_or_404(UserRole,pk=pk)

	if request.is_ajax and request.method == 'POST':

	  #-- Delete group permissions
	  UserMenuGroup.objects.filter(user_type=context['user_role']).delete()

	  #-- Delete relations users with group
	  context['user_role'].user.clear()

	  messages.success(request, 'Registro eliminado satisfactoriamente')
		
	  #-- User Logs (Info, Access, Error)
	  user_logs(request,None,'A','Registro eliminado satisfactoriamente')

	  data['form_is_valid'] = True
	  data['url_redirect'] = '/panel/accounts/userpermissions/'

	else:

	  context['obj_delete'] = context['user_role']
	  context['url_post'] = 'accounts:userpermissions_delete'

	  data['html_delete'] = render_to_string('core/snippets/modal_delete.html', context, request=request)
			
	return JsonResponse(data)
