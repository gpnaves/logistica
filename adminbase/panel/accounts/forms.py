# -*- coding: utf-8 -*-
from django import forms
from django.db.models import Case

from panel.config.models import MenuItems
from .models import User, UserRole, UserMenuGroup

class UserForm(forms.ModelForm):
	email = forms.EmailField(
		label = 'Email',
		error_messages = {'required': 'Debe capturar el Email'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control required',
				'aria-required' : 'True'
			}
		)
	)
	first_name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required': 'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	last_name = forms.CharField(
		label = 'Apellidos',
		error_messages = {'required': 'Debe capturar los apellidos'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control'
			}
		)
	)
	user_type = forms.ModelChoiceField(
		label = 'Rol de Usuario',
		error_messages = {'required': 'Debe seleccionar el rol de usuario'},
		empty_label="Seleccione...",
		queryset = UserRole.objects.filter(status=True).order_by('name'),
		widget = forms.Select(
			attrs = {
				'class' : 'form-control select2'
			}
		)
	)
	is_active = forms.BooleanField(
		label = 'Activo',
		required = False,
		widget = forms.CheckboxInput(
			attrs = {
				'class':'js-switch'
			}
		)
	)
	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user')
		super(UserForm, self).__init__(*args, **kwargs)	

	def clean(self):
		pass
		#cd = super(UserForm,self).clean()
		#if not self.user.user_groups.exists():
		#	if cd.get('user_type') is None:
		#		self.add_error('user_type','Debe seleccionar el rol de usuario')

		#return cd

	def clean_email(self):
		cd = self.cleaned_data

		if self.instance.pk:
			email = User.objects.filter(email__iexact=cd['email']).exclude(pk=self.user.id).exists()
		else:
			email = User.objects.filter(email__iexact=cd['email']).exists()

		if email:
			raise forms.ValidationError('Este correo ya está registrado')

		return cd['email']

	class Meta:
		model = User
		fields = ('email', 'first_name','last_name', 'user_type','is_active')

class UserRoleForm(forms.ModelForm):
	name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required':'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control'
			}
		)
	)
	description = forms.CharField(
		label = 'Descripción',
		required = False,
		widget = forms.TextInput(
			attrs = {
                'class': 'form-control'
            }
        )
    )
	
	is_group = forms.BooleanField(
		label = 'Es grupo',
		required = False,
		widget = forms.CheckboxInput(
			attrs = {
				'class':'js-switch',
			}
		)
	)

	class Meta:
		model = UserRole
		fields = ('name','description','is_group')

class UserMenuGroupForm(forms.ModelForm):
     user_type = forms.ModelChoiceField(
        label = 'Rol de Usuario',
		queryset =  UserRole.objects.filter(status=True,is_group=True),
		empty_label = 'Seleccione...',
        error_messages = {'required':'Debe capturar el rol de usuario'},
        widget = forms.Select(
            attrs = {
                'class': 'form-control select2'
            }
        )
     )

     class Meta:
         model = UserMenuGroup
         fields = ('user_type',)