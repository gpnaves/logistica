# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
	#-- List Users
    url(r'^$', views.accounts_list, name='accounts_list'),
	#-- Add User
    url(r'^add/$', views.accounts_add, name='accounts_add'),
	#-- Edit User
    url(r'^edit/(?P<pk>[0-9]+)/$', views.accounts_edit, name='accounts_edit'),
    #-- Delete User
    url(r'^delete/(?P<pk>[0-9]+)/$', views.accounts_delete, name='accounts_delete'),
    #-- Send Mail
    url(r'^sendmail/(?P<pk>[0-9]+)/$', views.accounts_sendmail, name='accounts_sendmail'),
    #-- Mail Activation User
    url(r'^sendmail_activation/(?P<pk>[0-9]+)/$', views.accounts_sendmail_activation, name='accounts_sendmail_activation'),
    #-- Activation Link User
    url(r'^activation/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.accounts_activation, name='accounts_activation'),
	#-- Logs List
    url(r'^logs/(?P<pk>[0-9]+)/$', views.accounts_logs, name='accounts_logs'),
	#-- User Role Ajax
    url(r'^userrole/$', views.accounts_userrole, name='accounts_userrole'),

    #-- Reset password
    

	#-- List User Roles
    url(r'^roles/$', views.roles_list, name='roles_list'),
	#-- Add User Role
    url(r'^roles/add/$', views.roles_add, name='roles_add'),
	#-- Edit User Role
    url(r'^roles/edit/(?P<pk>[0-9]+)/$', views.roles_edit, name='roles_edit'),
    #-- Delete User Role
    url(r'^roles/delete/(?P<pk>[0-9]+)/$', views.roles_delete, name='roles_delete'),

	#-- List User Group
    url(r'^userpermissions/$', views.userpermissions_list, name='userpermissions_list'),
	#-- Add User Group
    url(r'^userpermissions/add/$', views.userpermissions_add, name='userpermissions_add'),
	#-- Edit User Group
    url(r'^userpermissions/edit/(?P<pk>[0-9]+)/$', views.userpermissions_edit, name='userpermissions_edit'),
    #-- Options User Group
    url(r'^userpermissions/options/(?P<pk>[0-9]+)/$', views.userpermissions_options, name='userpermissions_options'),
    
    #-- Delete User Groups
    url(r'^userpermissions/delete/(?P<pk>[0-9]+)/$', views.userpermissions_delete, name='userpermissions_delete'),
]
