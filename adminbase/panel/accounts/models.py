# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import datetime

from django.db import models
from django.conf import settings
from django.apps import apps
from django.urls import reverse
from django.db.models.signals import post_delete
from django.template.loader import render_to_string
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django_extensions.db.models import TimeStampedModel
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.crypto import get_random_string
from django.core.cache import cache

from panel.core.tokens import account_activation_token
from panel.core.managers import UserManager
from panel.core.signals import file_cleanup
from panel.core.utils import get_filename,sendmail
from panel.core.validators import validate_file_extension
from panel.config.models import MenuItems, MenuSubItems

#-- Activation Status Values
ACTIVATIONSTATUS_CHOICES = (
    (0, 'Enviado'),
    (1, 'Activado o Expirado'),
)
PERMISSION_CHOICES = [
    ('can_add', 'Agregar'),
    ('can_edit', 'Editar'),
    ('can_delete', 'Eliminar'),
    ('can_view', 'Ver'),
]

def get_avatar(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'accounts/%s' % get_filename(ext)

class UserRole(models.Model):
    user = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='user_groups',verbose_name='Usuario')
    name = models.CharField(max_length=80, verbose_name='Nombre')
    description = models.CharField(max_length=120, verbose_name='Descripción', blank=True)
    is_group = models.BooleanField(verbose_name='Es grupo', default=False)
    status = models.BooleanField(verbose_name='Status', default=True)
    
    class Meta:
        db_table = 'users_roles'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class User(AbstractBaseUser, PermissionsMixin, TimeStampedModel):
    email = models.EmailField(max_length=120, verbose_name='Email', unique=True)
    first_name = models.CharField(max_length=120, verbose_name='Nombre')
    last_name = models.CharField(max_length=120, verbose_name='Apellidos')
    phone = models.CharField(max_length=30, verbose_name='Teléfono', blank=True)
    avatar = models.ImageField(upload_to=get_avatar,validators=[validate_file_extension],verbose_name='Avatar', null=True, blank=True)
    facebook = models.CharField(max_length=120, verbose_name='Facebook', blank=True, help_text='Nombre de usuario')
    twitter = models.CharField(max_length=120, verbose_name='Twitter', blank=True, help_text='@su_nick')
    user_type = models.ForeignKey(UserRole, related_name='users', verbose_name='Rol de Usuario', null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(verbose_name='Activo', default=False)
    is_staff = models.BooleanField(verbose_name='Staff',default=False)
    status = models.BooleanField(verbose_name='Status', default=True)
    
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'users'
        ordering = ['email']
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_absolute_url(self):
        return reverse('personal:personal_profile')

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)

        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def last_seen(self):
        return cache.get('seen_%s' % self.email)

    def online(self):
        if self.last_seen():
            now = datetime.datetime.now()
            if now > self.last_seen() + datetime.timedelta(
                        seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False 

    @staticmethod
    def activation_email(request,user,password):
       
        #-- Send email
        #-- Get menu in cache
        panel = cache.get('datapanel')
        if not panel:
            PanelAdmin = apps.get_model('config','PanelAdmin')
            panel = PanelAdmin.objects.first()

            cache.set('datapanel',panel)

        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = account_activation_token.make_token(user)
            
        subject = '%s :: Registro de cuenta' % (panel.title)
        message = render_to_string('accounts/accounts_email.html', {
                                    'email': user.email,
                                    'password': password,
                                    'domain': request.build_absolute_uri('/')[:-1],
                                    'uid': uid,
                                    'token': token,
                                    'website': panel.url,
                                    'websitename': panel.title,
                                    })

        expires_key = datetime.datetime.today() + datetime.timedelta(2)
        #-- Save activation token
        user_activation = UserRequest.objects.create(user=user, uid=uid, token=token, expires_key=expires_key)

        sendmail(subject, message, settings.DEFAULT_FROM_EMAIL, user.email)

        return user_activation

    @staticmethod
    def activation_url(uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
            user_activation = UserRequest.objects.filter(token=token).first()
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token) and user_activation.activation_status == '0':
            user.is_active = True
            user.save()
            user_activation.activation_status = '1'
            user_activation.save()

            return user
        else:
            return False

    @staticmethod
    def create_account_group(request,user_type,info):
        group = UserRole.objects.get(name=user_type)

        #-- Create User
        user = User(email=info['email'],first_name=info['first_name'],last_name=info['last_name'],user_type=group)
        user.save()

        #-- Generate random password
        password = get_random_string(10)
        user.set_password(password)
        user.save()

        #-- Send activation email
        User.activation_email(request,user,password)

        #-- Save relation User/ Group
        group.user.add(user)

        #-- Return User
        return user
    
    @staticmethod
    def modules_list():
        return MenuItems.objects.filter(status=True,is_module=True).order_by('order')

    @staticmethod
    def save_modules_list(obj_form, user, change):
        #-- Edit User
        if change:
            UserMenuPermission.objects.filter(user=user).delete()

        for item in obj_form:
            module = item.split('|')
            if module[1] == '1':
                submenu = MenuSubItems.objects.get(pk=module[0])
                UserMenuPermission.objects.create(user=user,menu_item=submenu.item,menu_subitems=submenu)
            else:
                menu = MenuItems.objects.get(pk=module[0])
                UserMenuPermission.objects.create(user=user,menu_item=menu)

    @staticmethod
    def save_modules_group(obj_form, form, change):
        #-- Edit Group
        if change:
            UserMenuGroup.objects.filter(user_type=form.cleaned_data['user_type']).delete()
        for item in obj_form:
            module = item.split('|')
            code = module[2].lower()
            if module[1] == '1':
                submenu = MenuSubItems.objects.get(pk=module[0])
                UserMenuGroup.objects.create(user_type=form.cleaned_data['user_type'],menu_item=submenu.item,menu_subitems=submenu, permission_code=code)
            else:
                menu = MenuItems.objects.get(pk=module[0])
                UserMenuGroup.objects.create(user_type=form.cleaned_data['user_type'],menu_item=menu, permission_code=code)
    
    @staticmethod
    def get_user_permissions(user):
        permissions = []
        if user.is_superuser:
            qs = UserMenuGroup.objects.all().values('permission_code')
        else:
            qs = UserMenuGroup.objects.filter(user_type=user.user_type).values('permission_code')
        
        for q in qs:
            permissions.append(q['permission_code'])

        return permissions

    @staticmethod
    def delete_modules_user(item):
        UserMenuPermission.objects.filter(menu_item=item).delete()
   
    @staticmethod
    def delete_submodules_user(item):
        UserMenuPermission.objects.filter(menu_subitems=item).delete()


post_delete.connect(file_cleanup,sender=User)

class UserRequest(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='activation', on_delete=models.CASCADE)
    uid = models.CharField(max_length=20, default='')
    token = models.CharField(max_length=60, default='')
    expires_key = models.DateTimeField(default=datetime.datetime.now)
    activation_status = models.CharField(max_length=2, choices=ACTIVATIONSTATUS_CHOICES, default='0')
    activation_date = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = 'users_requests'

class UserMenuPermission(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='user_permission', verbose_name='Usuario', on_delete=models.CASCADE)
    menu_item = models.ForeignKey(MenuItems, related_name='user_items',verbose_name='Items', on_delete=models.DO_NOTHING)
    menu_subitems = models.ForeignKey(MenuSubItems, related_name='user_subitems',verbose_name='SubItems',null=True, on_delete=models.SET_NULL)
    status = models.BooleanField(verbose_name='Status',default=True)

    class Meta:
        db_table = 'users_menu_permissions'
        verbose_name = 'Permiso de Usuario'
        verbose_name_plural = 'Permisos de Usuario'

    def __unicode__(self):
        return self.user.get_full_name()   

class UserMenuGroup(models.Model):
    user_type = models.ForeignKey(UserRole, related_name='group', verbose_name='Rol de Usuario', on_delete=models.CASCADE)
    menu_item = models.ForeignKey(MenuItems, related_name='group_items',verbose_name='Items', on_delete=models.DO_NOTHING)
    menu_subitems = models.ForeignKey(MenuSubItems, related_name='group_subitems',verbose_name='SubItems',null=True, on_delete=models.SET_NULL)
    permission_code = models.CharField(choices=PERMISSION_CHOICES, max_length=40, default='')
    status = models.BooleanField(verbose_name='Status',default=True)
    class Meta:
        db_table = 'users_group_permissions'
        verbose_name = 'Grupos de Usuarios'
        verbose_name_plural = 'Grupos de Usuarios'

    def __unicode__(self):
        return self.user_type.name

    def __str__(self):
        return self.user_type.name


