# Generated by Django 3.0.7 on 2020-08-04 17:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_user_is_staff'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermenugroup',
            name='permission_code',
            field=models.CharField(default='', max_length=20),
        ),
        migrations.AddField(
            model_name='usermenugroup',
            name='permission_name',
            field=models.CharField(default='', max_length=20),
        ),
    ]
