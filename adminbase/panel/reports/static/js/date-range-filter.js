$(document).ready(function() {

    /* Custom filtering function which will search data in column four between two values */
    $.fn.dataTable.ext.search.push(

        function( settings, data, dataIndex ) {
            var start = $("#fini").val();
            var end = $("#ffin").val();
            var date = data[0];
            start = start.substring(6,10) + start.substring(3,5)+ start.substring(0,2);
            end = end.substring(6,10) + end.substring(3,5)+ end.substring(0,2);
            date = date.substring(6,10) + date.substring(3,5)+ date.substring(0,2);
            if ( ( start == "" && end =="" ) ||
                ( start == "" && date <= end ) ||
                ( start <= date   && end == "" ) ||
                ( start <= date   && date <= end ) )
            {
                return true;
            }
            return false;
        }
    );
});