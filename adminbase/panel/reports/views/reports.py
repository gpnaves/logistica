from django.utils import timezone

from django.shortcuts import render

from django.contrib import messages
from panel.core.utils import user_logs
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

from django.db.models import Count, Sum, Q

from panel.routes.models import Note
from panel.employees.models import Employee

from django.urls import resolve

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required

@has_permissions('can_view_' + app_name)
def report_list(request):
	context = {}

	employees = Employee.objects.filter(status=True)
	calls_info = Note.objects.filter(user__status = True).values('user__employee_user').annotate(total=Count('*'))
	context['calls_info'] = []

	for employee in employees:
		dic = {}
		has_calls = False
		dic['employee'] = employee
		for call in calls_info:
			if call['user__employee_user'] == employee.pk:
				dic['calls'] = call['total']
				has_calls = True
				break
		if not has_calls:
			dic['calls'] = 0
		
		context['calls_info'].append(dic)

	today = timezone.now()
	yesterday = today - timezone.timedelta(days=1)
	this_monday = today - timezone.timedelta(days=today.weekday())
	last_monday = this_monday - timezone.timedelta(days=7)
	# this_month = today -

	context['today_calls'] = Note.objects.filter(created__date=today).order_by('-created')
	context['yesterday_calls'] = Note.objects.filter(created__date=yesterday).order_by('-created')
	context['week_calls'] = Note.objects.filter(created__range=(this_monday, today)).order_by('-created')
	context['last_week_calls'] = Note.objects.filter(created__range=(last_monday, this_monday-timezone.timedelta(days=1))).order_by('-created')
	context['month_calls'] = Note.objects.filter(created__month=today.month).order_by('-created')
	context['last_month_calls'] = Note.objects.filter(created__month=today.month - 1).order_by('-created')

	context['calls_list'] = Note.objects.all().order_by('-created')
	
	return render(request, 'reports/report_list.html', context)

# @has_permissions('can_view_' + app_name)
# def employee_report(request):
# 	context = {}
# 	return render(request, 'reports/emloyees_report.html', context)

def call_detail(request, pk):
	context = {}
	data = dict()

	context['employee_obj'] = get_object_or_404(Employee, pk=pk)
	context['calls_info'] = Note.objects.filter(user=context['employee_obj'].user).order_by('-created')

	data['html_form'] = render_to_string('reports/report_employee_calls.html', context, request=request)
	
	return JsonResponse(data) 
