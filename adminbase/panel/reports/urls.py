from django.conf.urls import url
from .views import reports

urlpatterns = [
    url(r'^$', reports.report_list, name='report_list'),
	url(r'^info/calls/employee/(?P<pk>[0-9]+)/detail/$', reports.call_detail, name='call_detail'),
    # url(r'^info/employee/(?P<pk>[0-9]+)/calls/$', reports.call_detail, name='call_detail'),

]
