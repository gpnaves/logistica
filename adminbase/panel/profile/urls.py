
from django.conf.urls import url
from panel.profile.views import profile

urlpatterns = [
    #-- Personal Profile
    url(r'^$', profile.personal_profile, name='personal_profile'),
    #-- Change password 
    url(r'^change/password/$', profile.change_password, name='change_password'),

]