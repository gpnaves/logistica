from django import forms
from panel.accounts.models import User

class UserProfileForm(forms.ModelForm):
	email = forms.EmailField(
		label = 'Email',
		error_messages = {'required': 'Debe capturar el Email'},
		widget = forms.EmailInput(
			attrs = {
				'class' : 'form-control bg-light border-0 small'
			}
		)
	)
	first_name = forms.CharField(
		label = 'Nombre',
		error_messages = {'required': 'Debe capturar el nombre'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control bg-light border-0 small',
			}
		)
	)
	last_name = forms.CharField(
		label = 'Apellidos',
		error_messages = {'required': 'Debe capturar los apellidos'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control bg-light border-0 small'
			}
		)
	)
	phone = forms.CharField(
		label = 'Teléfono',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control bg-light border-0 small'
			}
		)
	)
	facebook = forms.CharField(
		label = 'Facebook',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control bg-light border-0 small'
			}
		)
	)
	twitter = forms.CharField(
		label = 'Twitter',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control'
			}
		)
	)
	avatar = forms.ImageField(
		label = 'Foto',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user')
		super(UserProfileForm, self).__init__(*args, **kwargs)		

	def clean_email(self):
		cd = self.cleaned_data
		email = User.objects.filter(email__iexact=cd['email']).exclude(pk=self.user.id).exists()

		if email:
			raise forms.ValidationError('Este correo ya está registrado')

		return cd['email']

	class Meta:
		model = User
		fields = ('email', 'first_name','last_name','phone','facebook','twitter','avatar')
