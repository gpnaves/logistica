from django.shortcuts import render, redirect, get_object_or_404

from panel.core.utils import user_logs
from panel.accounts.models import User
from panel.profile.forms import UserProfileForm

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm

# Create your views here.
@login_required
def personal_profile(request):
	context = {}
	if request.method == 'POST':
		context['profile_form'] = UserProfileForm(request.POST, request.FILES, instance=request.user,\
											   user=request.user)
		if context['profile_form'].is_valid():
				
				profile = context['profile_form'].save()
				
				msg = messages.success(request, 'Usuario modificado satisfactoriamente')
				
				#-- User Logs (Info, Access, Error)
				user_logs(request,None,'I','Usuario modificado satisfactoriamente')

				return redirect('authorization:dashboard')
	else:
		context['profile_form'] = UserProfileForm(instance=request.user, user=request.user)
	
	return render(request, 'personal/personal_profile.html', context)

def change_password(request):
	context = {}
	
	if request.method == 'POST':
		context['change_password_form'] = PasswordChangeForm(request.user, request.POST)
		if context['change_password_form'].is_valid():
			user = context['change_password_form'].save()
			update_session_auth_hash(request, user)  # Important!
			messages.success(request, 'Tu contraseña se ha cambiado correctamente')
			return redirect('authorization:dashboard')
		else:
			messages.error(request, 'Please correct the error below.')
	else:
		context['change_password_form'] = PasswordChangeForm(request.user)
	return render(request, 'personal/change_password.html', context)