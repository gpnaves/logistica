
from django.conf.urls import url
from panel.routes.views import routes

urlpatterns = [
    url(r'^$', routes.routes_list, name='routes_list'),
    url(r'^add/$', routes.routes_add, name='routes_add'),
    url(r'^note/(?P<pk>[0-9]+)/(?P<source_id>[0-9]+)/(?P<target_id>[0-9]+)/$', routes.routes_note, name='routes_note'),
    url(r'^note/(?P<pk>[0-9]+)/$', routes.routes_note, name='routes_note'),
    url(r'^note/add/(?P<pk>[0-9]+)/(?P<source_id>[0-9]+)/(?P<target_id>[0-9]+)/$', routes.routes_note, name='routes_note'),

]