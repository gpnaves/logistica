from django.shortcuts import render
from django.db.models import Q, Max, Count
from panel.routes.forms import RouteForm, RouteFilterForm, NoteForm
from panel.routes.models import Route, Note, LastFilterRoute
from panel.carriers.models import Carriers, InterestRoutes
from panel.customers.models import Customer, CustomerInterestRoutes
from panel.locations.models import ZONES
from cities.models import City 

from django.contrib import messages

from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.core.exceptions import MultipleObjectsReturned
from panel.core.utils import user_logs
from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
def routes_list(request):
	context = {}

	# Esto se va a reescribir una vez se hace el POST
	url_redirect = 'routes/routes_filter.html'
	if request.method == 'POST':
		context = {}
		context['route_form'] = RouteFilterForm(request.POST)

		if context['route_form'].is_valid():
			# Guarda el filtro buscado
			filter_obj = context['route_form'].save(commit=False)
			
			only_source = True if context['route_form'].cleaned_data['search_type'] == '1' else  False

			filter_obj.user = request.user
			filter_obj.save()
			# --

			routes_context = {}

			person_type = context['route_form'].cleaned_data['person_type']

			if person_type == 'carrier':
				# Guarda los resultados de los transportistas con los filtros en el contexto temporal
				routes_context, url_redirect = get_carriers_routes_context(filter_obj, only_source)
			elif person_type == 'customer':
				# Guarda los resultados de los clientes con los filtros en el contexto temporal
				routes_context, url_redirect = get_customer_routes_context(filter_obj, only_source)

			# Agrega el contexto temporal al contexto
			context.update(routes_context)

			context['route_form'] = RouteFilterForm(instance=filter_obj)

			if context['count'] > 0:
				messages.success(request, 'Se encontraron '+ str(context['count']) + ' resultados')
			else:
				messages.error(request, 'No se encontraron resultados')			
	else:
		context['route_form'] = RouteFilterForm()

	context['permissions'] = request.user.get_user_permissions(request.user)
	context['app_name'] = app_name
	context['last_filters'] = get_last_filters(request.user)

	return render(request, url_redirect, context)


@login_required
@has_permissions('can_add_' + app_name)
def routes_add(request):
	context = {}

	if request.method == 'POST':
		context['route_form'] = RouteForm(request.POST)

		if context['route_form'].is_valid():
			route_obj = context['route_form'].save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Ruta creada satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Ruta creada satisfactorimente')
			
			return redirect('routes:route_list')
	else:
		context['route_form'] = RouteForm()
	
	return render(request, 'routes/routes_form.html', context)

def routes_note(request, pk, source_id=None, target_id=None):
	context = {}
	data = dict()

	carrier = get_object_or_404(Carriers, pk=pk)

	context['note_form'] = NoteForm(request.POST or None)

	if request.is_ajax and request.method == 'POST':

		if context['note_form'].is_valid():
			note_obj = context['note_form'].save(commit=False)
			note_obj.user = request.user
			note_obj.carrier = carrier
			if source_id:
				note_obj.source = City.objects.get(pk=source_id)
			if target_id:
				note_obj.target = City.objects.get(pk=target_id)
			note_obj.save()
					
			messages.success(request, 'Nota registrada')

			data['form_is_valid'] = True

			# data['url_redirect'] = '/panel/matchs/detail/' + str(pk)
		else:
			data['form_is_valid'] = False

	data['html_form'] = render_to_string('routes/routes_notes.html', context, request=request)
	
	return JsonResponse(data) 


def get_carriers_routes_context(filter_obj, only_source=False):
	context = {}

	carriers_found = []

	# Obtiene los transportistas con esa ruta exacta
	interest_routes = []
	if only_source:
		interest_routes = \
			InterestRoutes.objects.filter(
				source_path=filter_obj.source_path, 
				carrier__is_national=False)
	else:
		interest_routes = \
			InterestRoutes.objects.filter(
				source_path=filter_obj.source_path, 
				target_path = filter_obj.target_path, 
				carrier__is_national=False)	
	# --
	for route in interest_routes:
		carriers_found.append(route.carrier.pk)
	

	national_list = Carriers.objects.filter(is_national=True, base_city=filter_obj.source_path)
	for carrier in national_list:
		carriers_found.append(carrier.pk)
	# Obtiene los transportistas que hallan hecho esa ruta
	routes = []

	routes_filter = []
	if only_source:
		routes_filter = Route.objects.filter(source = filter_obj.source_path).values('carrier', 'source', 'target').annotate(travels=Count('*'))
	else:
		routes_filter = Route.objects.filter(source = filter_obj.source_path, target=filter_obj.target_path).values('carrier', 'source', 'target').annotate(travels=Count('*'))
	
	for item in routes_filter:
		try:
			dicc = {}
			carrier = Carriers.objects.get(pk=item['carrier'])
			dicc['carrier'] = carrier
			dicc['source_path'] = City.objects.get(pk = item['source'])
			dicc['target_path'] = City.objects.get(pk = item['target'])
			dicc['travels'] = item['travels']
			if carrier.pk not in carriers_found:
				routes.append(dicc)
				carriers_found.append(carrier.pk)

		except MultipleObjectsReturned:
			pass
	# --



	# Obtiene los transportistas con una ruta cercana 
	carriers_filter = []
	routes_filter = []
	national_filter = []

	near_cities_target = []
	if not only_source:
		near_cities_target = City.objects.filter(
					location__distance_lte=(filter_obj.target_path.location, D(km=filter_obj.target_radio))).exclude(id=filter_obj.target_path.id)
	
	near_cities_source = City.objects.filter(
					location__distance_lte=(filter_obj.source_path.location, D(km=filter_obj.source_radio))).exclude(id=filter_obj.source_path.id)
					
	for near_city in near_cities_target:
		result = InterestRoutes.objects.filter(
						source_path=filter_obj.source_path.id, 
						target_path=near_city.id,
						carrier__is_national=False)

		result2 = Route.objects.filter(
						source=filter_obj.source_path.id,
						target=near_city.id,
						carrier__is_national=False) \
					.values(
						'carrier', 
						'source', 
						'target') \
					.annotate(travels=(Count('*')))

		if result:
			carriers_filter.append(result)
		if result2:
			routes_filter.append(result2)

	
	for near_city in near_cities_source:
		result = []
		result2 = []

		if only_source:
			result = InterestRoutes.objects.filter(
						source_path=near_city.id, 
						carrier__is_national=False)
	
		else:
			result = InterestRoutes.objects.filter(
						source_path=near_city.id, 
						target_path=filter_obj.target_path.id,
						carrier__is_national=False)
		
		if only_source: 
			result2 = Route.objects.filter(
						source=near_city.id) \
					.values(
						'carrier', 
						'source', 
						'target') \
					.annotate(
						travels=(Count('*')))
		else:
			result2 = Route.objects.filter(
						source=near_city.id,
						target=filter_obj.target_path.id) \
					.values(
						'carrier', 
						'source', 
						'target') \
					.annotate(
						travels=(Count('*')))
		
		result3 = Carriers.objects.filter(is_national=True, base_city=near_city.id)

		if result:
			carriers_filter.append(result)
		if result2:
			routes_filter.append(result2)
		if result3:
			national_filter.append(result3)
	# --

	# -- Estas variables son las que iran en el contexto. Se busca obtener el id del transportista
	near_carriers = []
	near_routes = []
	near_national = []

	if carriers_filter:
		for items in carriers_filter:
			for item in items:
				try:
					carrier = Carriers.objects.get(pk=item.carrier.pk)
					if carrier.pk not in carriers_found:
						dic = {}
						dic['pk'] = carrier.pk
						dic['carrier'] = carrier
						dic['source_path'] = City.objects.get(pk = item.source_path.pk)
						dic['target_path'] = City.objects.get(pk = item.target_path.pk)
						dic['contact'] = carrier.operative_contact
						near_carriers.append(dic)
						carriers_found.append(carrier.pk)
				except MultipleObjectsReturned:
					pass
	if routes_filter:
		for items in routes_filter:
			for item in items:
				try:
					carrier = Carriers.objects.get(pk=item['carrier'])
					if carrier.pk not in carriers_found:
						dic = {}
						dic['carrier'] = carrier
						dic['source_path'] = City.objects.get(pk = item['source'])
						dic['target_path'] = City.objects.get(pk = item['target'])
						dic['travels'] = item['travels']
						near_routes.append(dic)
						carriers_found.append(carrier.pk)
				except MultipleObjectsReturned:
					pass

	if national_filter:
		for items in national_filter:
			for item in items:
				near_national.append(item)
	# --

	context['count'] = len(interest_routes) + len(routes) + len(near_carriers) + len(near_routes) + len(national_list) + len(national_filter)

	context['filter_obj'] = filter_obj
	context['interest_routes_list'] = interest_routes
	context['national_list'] = national_list
	context['near_national'] = near_national
	context['routes_list'] = routes
	context['near_person'] = near_carriers
	context['near_routes'] = near_routes

	url_redirect = 'routes/routes_carrier.html'

	return context, url_redirect

def get_customer_routes_context(filter_obj, only_source=False):
	context = {}

	customer_found = []

	# Obtiene los clientes con esa ruta exacta

	if only_source:
		interest_routes = \
			CustomerInterestRoutes.objects.filter(
				source_path=filter_obj.source_path)	
	else:
		interest_routes = \
			CustomerInterestRoutes.objects.filter(
				source_path=filter_obj.source_path, 
				target_path = filter_obj.target_path)	
	# --
	for route in interest_routes:
		customer_found.append(route.customer.pk)
	
	# Obtiene los clientes que hallan hecho esa ruta
	routes = []
	routes_filter = []
	if only_source:
		routes_filter = Route.objects.filter(source = filter_obj.source_path).values('customer', 'source', 'target').annotate(travels=Count('*'))
	else:
		routes_filter = Route.objects.filter(source = filter_obj.source_path, target = filter_obj.target_path).values('customer', 'source', 'target').annotate(travels=Count('*'))
	for item in routes_filter:
		try:
			dicc = {}
			customer = Customer.objects.get(pk=item['customer'])
			dicc['customer'] = customer
			dicc['source_path'] = City.objects.get(pk = item['source'])
			dicc['target_path'] = City.objects.get(pk = item['target'])
			dicc['travels'] = item['travels']
			if customer.pk not in customer_found:
				routes.append(dicc)
				customer_found.append(customer.pk)

		except MultipleObjectsReturned:
			pass
	# --

	# Obtiene los transportistas con una ruta cercana 
	customer_filter = []
	routes_filter = []


	near_cities_target = []
	if not only_source:
		near_cities_target = City.objects.filter(
					location__distance_lte=(filter_obj.target_path.location, D(km=filter_obj.target_radio))).exclude(id=filter_obj.target_path.id)
	near_cities_source = City.objects.filter(
					location__distance_lte=(filter_obj.source_path.location, D(km=filter_obj.source_radio))).exclude(id=filter_obj.source_path.id)

	
	for near_city in near_cities_target:
		result = CustomerInterestRoutes.objects.filter(
						source_path=filter_obj.source_path.id, 
						target_path=near_city.id)

		result2 = Route.objects.filter(
						source=filter_obj.source_path.id,
						target=near_city.id) \
					.values(
						'customer', 
						'source', 
						'target') \
					.annotate(travels=(Count('*')))

		if result:
			customer_filter.append(result)
		if result2:
			routes_filter.append(result2)

	
	for near_city in near_cities_source:
		result = []
		result2 = []
		if only_source:
			result = CustomerInterestRoutes.objects.filter(
						source_path=near_city.id)
		else:
			result = CustomerInterestRoutes.objects.filter(
						source_path=near_city.id, 
						target_path=filter_obj.target_path.id)
		
		if only_source:
			result2 = Route.objects.filter(
						source=near_city.id) \
					.values(
						'customer', 
						'source', 
						'target') \
					.annotate(
						travels=(Count('*')))
		else:
			result2 = Route.objects.filter(
						source=near_city.id,
						target=filter_obj.target_path.id) \
					.values(
						'customer', 
						'source', 
						'target') \
					.annotate(
						travels=(Count('*')))
		
		if result:
			customer_filter.append(result)
		if result2:
			routes_filter.append(result2)
	# --

	# -- Estas variables son las que iran en el contexto. Se busca obtener el id del transportista
	near_customers = []
	near_routes = []

	if customer_filter:
		for items in customer_filter:
			for item in items:
				try:
					customer = Customer.objects.get(pk=item.customer.pk)
					if customer.pk not in customer:
						dic = {}
						dic['pk'] = customer.pk
						dic['customer'] = customer
						dic['source_path'] = City.objects.get(pk = item.source_path.pk)
						dic['target_path'] = City.objects.get(pk = item.target_path.pk)
						dic['contact'] = customer.operative_contact
						near_customers.append(dic)
						customer_found.append(customer.pk)
				except MultipleObjectsReturned:
					pass
	if routes_filter:
		for items in routes_filter:
			for item in items:
				try:
					customer = Customer.objects.get(pk=item['customer'])
					if customer.pk not in customer_found:
						dic = {}
						dic['customer'] = customer
						dic['source_path'] = City.objects.get(pk = item['source'])
						dic['target_path'] = City.objects.get(pk = item['target'])
						dic['travels'] = item['travels']
						near_routes.append(dic)
						customer_found.append(customer.pk)
				except MultipleObjectsReturned:
					pass
	# --

	context['count'] = len(interest_routes) + len(routes) + len(near_customers) + len(near_routes)

	context['filter_obj'] = filter_obj
	context['interest_routes_list'] = interest_routes
	context['routes_list'] = routes
	context['near_person'] = near_customers
	context['near_routes'] = near_routes

	url_redirect = 'routes/routes_customer.html'

	return context, url_redirect

def get_last_filters(user):
	filters = LastFilterRoute.objects.filter(user=user).values('source_path', 'target_path').annotate(last=Max('created')).order_by('-last')[:5]
	
	last_filters = []
	for item in filters:
		dic = {}
		dic['source_path'] = City.objects.get(pk=item['source_path'])
		if item['target_path']:
			dic['target_path'] = City.objects.get(pk=item['target_path'])
		dic['last'] = item['last']
		last_filters.append(dic)

	return last_filters
