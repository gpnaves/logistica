$(document).ready(function() {
    var phone_detail_button;


    $(window).focus(function(){
        if (phone_detail_button !== undefined) {
            phone_detail_button.click();
            phone_detail_button = undefined;
        }
    });

    var filter = function(select, value){
        if($(value != "")){
            $.get('/panel/locations/cities_per_region/', {region_id: value}, function(data){
              $(select).empty();
              $(select).append('<option selected disabled="disabled" value="">Seleccione el destino...</option>');
              $.each(JSON.parse(data.cities), function(idx, obj) {
                  $(select).append('<option value='+obj.pk+'>'+obj.fields.name+'</option>');
              });
              $(select).prop('disabled', false);
            });
        
        }
    };
    
    $("#id_source_region").change(function() {
        _value = $(this).val();
        filter("#id_source_path", _value);
    });

    $("#id_target_region").change(function() {
        _value = $(this).val();
        filter("#id_target_path", _value);
    });


    $(".phone_ref").click(function(){
        phone_detail_button = $(this).next();
    });

    $('input[type=radio][name=search_type]').change(function() {
        _value = $(this).val();

        switch (_value) {
            case '0':
                $("#div_source").show();
                $("#div_target").show();
                break;
            case '1':
                $("#div_target").hide();
                $("#div_source").show();
                break;
            case '2':
                $("#div_source").hide();
                $("#div_target").show();
                break;
        }
    });

});