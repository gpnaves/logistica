# -*- coding: utf-8 -*-
from django import forms

from panel.routes.models import Route, LastFilterRoute, Note, NOTE_OPTIONS
from panel.carriers.models import Carriers
from panel.customers.models import Customer
from cities.models import City, Region

CHOICES=[('carrier','Transportistas'),
		 ('customer','Clientes')]


SEARCH_TYPE = (
	('0', 'Ambos'),
	('1', 'Buscar solo por origen')
	# ('2', 'Buscar solo por destino')
)

class RouteForm(forms.ModelForm):
	date = forms.DateField(
		label = 'Fecha',
		error_messages = {'required' : 'Debe capturar la fecha de la ruta'},
		widget = forms.DateInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	customer = forms.ModelChoiceField(
		label='Cliente',
		required = False, # Temporal
		# error_messages = {'required' : 'Debe capturar el cliente'}, Todavia no se desarrolla el modulo de clientes
		queryset = Customer.objects.filter(status=True),
		empty_label = 'Seleccione al cliente...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	carrier = forms.ModelChoiceField(
		label='Transportista',
		error_messages = {'required' : 'Debe capturar el transportista'},
		queryset = Carriers.objects.filter(status=True),
		empty_label = 'Seleccione al transportista...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	source = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target = forms.ModelChoiceField(
		label='Ciudad destino',
		error_messages = {'required' : 'Debe capturar la ruta destino'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	source_region = forms.ModelChoiceField(
		label='Estado origen',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_region = forms.ModelChoiceField(
		label='Estado destino',
		error_messages = {'required' : 'Debe capturar el estado'},
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	class Meta:
		model = Route
		fields = ('date', 'customer', 'carrier', 'source', 'target')


class RouteFilterForm(forms.ModelForm):
	source_path = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	source_region = forms.ModelChoiceField(
		label='Estado origen',
		required = False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_path = forms.ModelChoiceField(
		label='Ciudad destino',
		required=False,
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	target_region = forms.ModelChoiceField(
		label='Estado destino',
		required = False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	source_radio = forms.CharField(
		label = 'Radio origen',
		required = False,
		initial = 0,
		widget = forms.TextInput(
			attrs = {
				'class': 'touchspin',
			}
		)
	 )
	target_radio = forms.CharField(
		label = 'Radio destino',
		required = False,
		initial = 0,
		widget = forms.TextInput(
			attrs = {
				'class': 'touchspin',
			}
		)
	 )

	person_type = forms.ChoiceField(
		choices=CHOICES, 
		widget=forms.RadioSelect(
			attrs = {
				'class': 'color simple_tag'
			}
		)
	)

	search_type = forms.ChoiceField(
		choices=SEARCH_TYPE, 
		widget=forms.RadioSelect(
			attrs = {
				'class': 'color simple_tag'
			}
		)
	)

	class Meta:
		 model = LastFilterRoute
		 fields = ('source_region', 'source_path', 'target_region', 'target_path', 'source_radio', 'target_radio',)

class NoteForm(forms.ModelForm):
	options = forms.ChoiceField(
		label = "Observaciones",
		required = False,
		choices = NOTE_OPTIONS,
		widget = forms.Select(attrs={"class": "form-control",}),
	)
	note = forms.CharField(
		label="Comentarios",
		required = False,
		widget=forms.Textarea(attrs={"rows": 4, "class": "form-control",}),
	)

	class Meta:
		model = Note
		fields = ('note', 'options')