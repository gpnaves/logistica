# Generated by Django 3.0.7 on 2020-11-03 23:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('routes', '0009_auto_20201006_1238'),
    ]

    operations = [
        migrations.AddField(
            model_name='route',
            name='consignment_note',
            field=models.CharField(max_length=10, null=True, unique=True, verbose_name='Carta porte'),
        ),
    ]
