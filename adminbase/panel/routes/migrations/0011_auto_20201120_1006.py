# Generated by Django 3.0.7 on 2020-11-20 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('routes', '0010_route_consignment_note'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='options',
            field=models.CharField(choices=[('no equipment', 'No tiene equipo'), ('paid problem', 'Problemas de pago'), ('disponibility check', 'Revisa disponibilidad'), ('no route', 'No maneja esa ruta'), ('put equipment', 'Coloca equipo'), ('other', 'Otros')], default='', max_length=100, verbose_name='Opciones'),
        ),
    ]
