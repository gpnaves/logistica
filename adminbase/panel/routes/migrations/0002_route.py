# Generated by Django 3.0.6 on 2020-05-06 23:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_auto_20200428_2238'),
        ('carriers', '0003_auto_20200428_1938'),
        migrations.swappable_dependency(settings.CITIES_CITY_MODEL),
        ('routes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(null=True)),
                ('carrier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='carrier_route', to='carriers.Carriers', verbose_name='Transportista')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer_route', to='customers.Customer', verbose_name='Cliente')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='source_route', to=settings.CITIES_CITY_MODEL, verbose_name='Origen')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='target_route', to=settings.CITIES_CITY_MODEL, verbose_name='Destino')),
            ],
            options={
                'verbose_name': 'Ruta',
                'verbose_name_plural': 'Rutas',
                'db_table': 'route',
            },
        ),
    ]
