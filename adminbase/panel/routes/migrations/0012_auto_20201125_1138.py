# Generated by Django 3.0.7 on 2020-11-25 18:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.CITIES_CITY_MODEL),
        ('routes', '0011_auto_20201120_1006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lastfilterroute',
            name='target_path',
            field=models.ForeignKey(max_length=100, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='last_target_path', to=settings.CITIES_CITY_MODEL, verbose_name='Ruta destino'),
        ),
    ]
