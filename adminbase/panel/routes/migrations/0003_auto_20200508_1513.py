# Generated by Django 3.0.6 on 2020-05-08 21:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_auto_20200428_2238'),
        ('routes', '0002_route'),
    ]

    operations = [
        migrations.AlterField(
            model_name='route',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer_route', to='customers.Customer', verbose_name='Cliente'),
        ),
    ]
