# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os


from django.db import models
from django_extensions.db.models import TimeStampedModel

from cities.models import City, Region
from panel.accounts.models import User
from panel.customers.models import Customer
from panel.carriers.models import Carriers
from panel.notifications.models import Notification


NOTE_OPTIONS = [
    ('no equipment', 'No tiene equipo'),
    ('paid problem', 'Problemas de pago'),
    ('disponibility check', 'Revisa disponibilidad'),
    ('no route', 'No maneja esa ruta'),
    ('put equipment', 'Coloca equipo'),
    ('other', 'Otros')
]

# Create your models here.

class Route(models.Model):
    date = models.DateField(null=True)
    consignment_note = models.CharField(verbose_name="Carta porte", max_length=10, unique=True, null=True)
    customer = models.ForeignKey(Customer, related_name = 'customer_route', verbose_name = 'Cliente', null=True, on_delete=models.CASCADE)
    carrier = models.ForeignKey(Carriers, related_name = 'carrier_route', verbose_name = 'Transportista', on_delete=models.CASCADE)
    source = models.ForeignKey(City, related_name = 'source_route', verbose_name = 'Origen', on_delete=models.CASCADE)
    target = models.ForeignKey(City, related_name = 'target_route', verbose_name = 'Destino', on_delete=models.CASCADE)
    

    class Meta:
        db_table = 'route'
        verbose_name = 'Ruta'
        verbose_name_plural = 'Rutas'

    def __str__(self):
        return self.carrier.alias + ' - ' + self.source.name + ' a ' + self.target.name

class LastFilterRoute(TimeStampedModel):
    source_region = models.ForeignKey(Region, related_name="last_source_region", verbose_name="Estado origen", max_length=100, default='', null=True, on_delete=models.DO_NOTHING)
    source_path = models.ForeignKey(City, related_name="last_source_path", verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING)
    target_region = models.ForeignKey(Region, related_name="last_target_region", verbose_name="Estado destino", max_length=100, default='', null=True, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name="last_target_path", verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING, null=True)
    source_radio = models.PositiveSmallIntegerField(verbose_name='Radio origen',default=0)
    target_radio = models.PositiveSmallIntegerField(verbose_name='Radio destino',default=0)
    
    user = models.ForeignKey(User, related_name = 'route_filter_user', verbose_name = 'Filtro de rutas de usuario', on_delete=models.CASCADE)

    class Meta:
        db_table = 'filter_route'
        verbose_name = 'Filtro'
        verbose_name_plural = 'Filtros'

    def __unicode__(self):
        return self.source_pat.name + ' a ' + self.target_path.name   
    
    def __str__(self):
        return self.source_path.name + ' a ' + (self.target_path.name if self.target_path else 'nada')

class Note(TimeStampedModel):
    user = models.ForeignKey(User, related_name='user_notes', verbose_name="Notas de usuario", on_delete=models.CASCADE)
    carrier = models.ForeignKey(Carriers, related_name='notes', verbose_name='Transportista', on_delete=models.CASCADE)
    source = models.ForeignKey(City, related_name="source_note", verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING, null=True)
    target = models.ForeignKey(City, related_name="target_note", verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING, null=True)
    note = models.CharField(verbose_name='Nota', max_length=1000, null=True)
    options = models.CharField(max_length=100, choices=NOTE_OPTIONS, default='', verbose_name='Opciones')
    mail = models.ForeignKey(Notification, related_name='mail_notes', verbose_name='Mensaje', null=True, on_delete=models.DO_NOTHING)
    
    is_reported = models.BooleanField(verbose_name="status", null=True)


    class Meta:
        db_table = 'route_notes'
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'