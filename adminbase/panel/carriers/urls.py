
from django.conf.urls import url
from panel.carriers.views import carriers

urlpatterns = [
    url(r'^$', carriers.carrier_list, name='carrier_list'),
	url(r'^add/$', carriers.carrier_add, name='carrier_add'),
	url(r'^edit/(?P<pk>[0-9]+)/$', carriers.carrier_edit, name='carrier_edit'),
	url(r'^detail/(?P<pk>[0-9]+)/$', carriers.carrier_detail, name='carrier_detail'),
	url(r'^delete/(?P<pk>[0-9]+)/$', carriers.carrier_delete, name='carrier_delete'),
	url(r'^sendmail/(?P<pk>[0-9]+)/(?P<type>[a-z]+)$', carriers.carrier_sendmail, name='carrier_sendmail'),
	url(r'^routes/(?P<pk>[0-9]+)/(?P<source>[0-9]+)/(?P<target>[0-9]+)/$', carriers.carrier_routes, name='carrier_routes'),
	url(r'^routes/(?P<pk>[0-9]+)/$', carriers.carrier_routes, name='carrier_routes'),

	url(r'^equipments/delete/(?P<pk>[0-9]+)/$', carriers.carrier_equipment_delete, name='carrier_equipment_delete'),
	url(r'^routes/delete/(?P<pk>[0-9]+)/$', carriers.carrier_route_delete, name='carrier_route_delete'),
]