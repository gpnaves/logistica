from django import forms

from panel.carriers.models import Carriers, CarrierEquipment, InterestRoutes
from panel.equipments.models import Equipment
from panel.employees.models import Employee

from cities.models import City, Region

class CarrierForm(forms.ModelForm):
	business_name = forms.CharField(
		label = 'Razon social',
		error_messages = {'required': 'Debe capturar la razon social'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	alias = forms.CharField(
		label = 'Alias',
		error_messages = {'required': 'Debe capturar el alia'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	address = forms.CharField(
		label = 'Direccion',
		error_messages = {'required': 'Debe capturar la direccion'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	postal_code = forms.CharField(
		label = 'Codigo postal',
		error_messages = {'required': 'Debe capturar el codigo postal'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'touchspin',
			}
		)
	)

	subregion = forms.CharField(
		label = 'Municipio',
		error_messages = {'required': 'Debe capturar el municipio'},
		widget = forms.TextInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)

	city = forms.CharField(
		label = 'Ciudad',
		error_messages = {'required': 'Debe capturar la ciudad'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		)
	)

	region = forms.CharField(
		label = 'Estado',
		error_messages = {'required': 'Debe capturar el estado'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		)
	)

	country = forms.CharField(
		label = 'Pais',
		error_messages = {'required': 'Debe capturar el pais'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		)
	)

	tax_number = forms.CharField(
		label = 'RFC',
		error_messages = {'required': 'Debe capturar el RFC'},
		widget = forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		)
	)

	is_national = forms.BooleanField(
		label = 'A toda la republica',
		required = False,
		widget = forms.CheckboxInput(
			attrs = {
				'class':'form-control'
			}
		)
	)

	coordinator = forms.ModelChoiceField(
		label='Coordinador',
		required = False,
		queryset = Employee.objects.filter(user__user_type__name__icontains='Coordinador de ventas'),
		empty_label = 'Seleccione al coordinador...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	base_city = forms.ModelChoiceField(
		label='Ciudad base',
		required = False,
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione la ciudad...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	base_city_region = forms.ModelChoiceField(
		label='Estado base',
		required= False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	picture = forms.ImageField(
		label = 'Foto',
		required = False,
		widget = forms.FileInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
	class Meta:
		model = Carriers
		fields = ('picture', 'business_name', 'alias', 'address', 'postal_code', 'city', \
			'region', 'subregion', 'country', 'tax_number', 'is_national', 'base_city', 'coordinator')


class EquipmentForm(forms.ModelForm):
	
	equipment = forms.ModelChoiceField(
		label='Equipos',
		required = False,
		queryset = Equipment.objects.filter(status=True),
		empty_label = 'Seleccione un equipo...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	quantity = forms.CharField(
		label = 'Cantidad',
		required = False,
		widget = forms.TextInput(
			attrs = {
				'class': 'touchspin',
			}
		)
	 )
	class Meta:
		model = CarrierEquipment
		fields = ('equipment', 'quantity')


class InterestRoutesForm(forms.ModelForm):
	source_path = forms.ModelChoiceField(
		label='Ciudad origen',
		error_messages = {'required' : 'Debe capturar la ruta origen'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el origen...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	source_path_region = forms.ModelChoiceField(
		label='Estado origen',
		required= False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)

	target_path = forms.ModelChoiceField(
		label='Ciudad destino',
		error_messages = {'required' : 'Debe capturar la ruta destino'},
		queryset = City.objects.filter(country_id=3996063),
		empty_label = 'Seleccione el destino...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	target_path_region = forms.ModelChoiceField(
		label='Estado destino',
		required= False,
		queryset = Region.objects.filter(country_id=3996063).order_by('name'),
		empty_label = 'Seleccione el estado...',
		widget = forms.Select(
			attrs = {
				'class': 'form-control select2',
			}
		)
	)
	class Meta:
		model = InterestRoutes
		fields = ('source_path', 'source_path_region', 'target_path', 'target_path_region',)
