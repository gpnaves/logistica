# Generated by Django 3.0.7 on 2020-08-20 17:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.CITIES_CITY_MODEL),
        ('carriers', '0009_auto_20200820_0828'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='interestroutes',
            name='nacional',
        ),
        migrations.AddField(
            model_name='carriers',
            name='base_city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='base_city', to=settings.CITIES_CITY_MODEL, verbose_name='Ciudad base'),
        ),
        migrations.AddField(
            model_name='carriers',
            name='is_national',
            field=models.BooleanField(default=False, verbose_name='Nacional'),
        ),
    ]
