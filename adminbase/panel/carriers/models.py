# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from cities.models import City, Region
from django.db import models
from django.urls import reverse
from panel.accounts.models import User
from panel.contacts.models import Contact
from panel.core.utils import get_filename
from panel.core.validators import validate_file_extension
from panel.employees.models import Employee
from panel.equipments.models import Equipment


# Create your models here.
def get_image(instance, filename):
    name, ext = os.path.splitext(filename)

    return 'carriers/%s' % get_filename(ext)

def get_file(intance, filename):
	name, ext = os.path.splitext(filename)

	return 'carriers/documents/%s' % get_filename(ext)

class Carriers(models.Model):
    picture = models.ImageField(upload_to=get_image,validators=[validate_file_extension],verbose_name='Imagen', null=True, blank=True)
    business_name = models.CharField(verbose_name='Razón Social', max_length=200)
    alias = models.CharField(verbose_name="Alias", max_length=200)
    address = models.CharField(verbose_name="Direccion", max_length=500)
    postal_code = models.CharField(verbose_name="Codigo postal", max_length=5)
    city = models.CharField(verbose_name="Ciudad", max_length=100, default="")
    subregion = models.CharField(verbose_name="Municipio", max_length=100, null=True)
    region = models.CharField(verbose_name="Estado", max_length=100)
    country = models.CharField(verbose_name="Pais", max_length=100)
    tax_number = models.CharField(verbose_name="RFC", max_length=13)
    
    user = models.ForeignKey(User, related_name = 'carrier_user', verbose_name = 'Usuario transportista', null=True, on_delete=models.DO_NOTHING)
    operative_contact = models.ForeignKey(Contact, verbose_name="Contacto operativo", null=True, related_name="operative_contact", on_delete=models.SET_NULL)
    administrative_contact = models.ForeignKey(Contact, verbose_name="Contacto administrativo", null=True, related_name="administrative_contact", on_delete=models.SET_NULL)
    
    equipments = models.ManyToManyField(Equipment, through='CarrierEquipment')
    coordinator = models.ForeignKey(Employee, related_name='coordinator', verbose_name='Coordinador', on_delete=models.DO_NOTHING, null=True)
    # Document
    tax_number_doc = models.FileField(upload_to=get_file, verbose_name='RFC', null=True)
    oficial_id = models.FileField(upload_to=get_file, verbose_name='Identificacion', null=True)
    transport_allowance = models.FileField(upload_to=get_file, verbose_name='Permiso de transporte', null=True)
    proof_address = models.FileField(upload_to=get_file, verbose_name='Comprobante de domicilio', null=True)
    e_billin_demo = models.FileField(upload_to=get_file, verbose_name='Factura electronica demo', null=True)
    bank_statement = models.FileField(upload_to=get_file, verbose_name='Estado de cuenta', null=True)
    unity_list = models.FileField(upload_to=get_file, verbose_name='Relacion de unidades', null=True)
    contract = models.FileField(upload_to=get_file, verbose_name='Contrato', null=True)
    constitutive_act = models.FileField(upload_to=get_file, verbose_name='Acta constitutiva', null=True)
    power_attorney = models.FileField(upload_to=get_file, verbose_name='Poder de los representante', null=True)
    payment_authorization = models.FileField(upload_to=get_file, verbose_name='Autorizacion para pagos', null=True)
    
    # 
    driving_license = models.FileField(upload_to=get_file, verbose_name='Licencia', null=True)
    insurance = models.FileField(upload_to=get_file, verbose_name='Seguro de la unidad', null=True)
    registration = models.FileField(upload_to=get_file, verbose_name='Tarjeta de circulacion', null=True)
    gps = models.FileField(upload_to=get_file, verbose_name='GPS', null=True)
    mirror_account = models.FileField(upload_to=get_file, verbose_name='Cuenta espejo', null=True)
    # --

    is_national = models.BooleanField(verbose_name='Nacional', default=False)
    base_city = models.ForeignKey(City, related_name='base_city', verbose_name='Ciudad base', null=True, on_delete=models.CASCADE)

    person_type = models.CharField(verbose_name='Tipo de persona', max_length=10, null=True)
    status = models.BooleanField(verbose_name="status", default=True)

    class Meta:
        db_table = 'carriers'
        verbose_name = 'Transportista'
        verbose_name_plural = 'Transportistas'

    def __str__(self):
        return self.alias

class CarrierEquipment(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    carrier = models.ForeignKey(Carriers, related_name='carrier_equipment' ,on_delete=models.CASCADE)
    quantity = models.SmallIntegerField(verbose_name="cantidad", default=1)

    def get_absolute_url(self):
        return reverse('carriers:carrier_equipment_delete', kwargs={'pk': self.pk})

    def __str__(self):
        return self.equipment.name

class InterestRoutes(models.Model):
    carrier = models.ForeignKey(Carriers, related_name='interest_routes', on_delete=models.CASCADE)
    source_path = models.ForeignKey(City, related_name="source_path", verbose_name="Ruta origen", max_length=100, on_delete=models.DO_NOTHING)
    source_path_region = models.ForeignKey(Region, related_name="source_path_region", verbose_name="Estado origen", max_length=100, on_delete=models.DO_NOTHING)
    target_path = models.ForeignKey(City, related_name="target_path", verbose_name="Ruta destino", max_length=100, on_delete=models.DO_NOTHING, null=True)
    target_path_region = models.ForeignKey(Region, related_name="target_path_region", verbose_name="Estado destino", max_length=100, on_delete=models.DO_NOTHING, null=True)

    def get_absolute_url(self):
        return reverse('carriers:carrier_route_delete', kwargs={'pk': self.pk})
