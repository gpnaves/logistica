
from ..models import Carriers, CarrierEquipment, InterestRoutes
from ..forms import CarrierForm, EquipmentForm, InterestRoutesForm
from panel.core.forms import MailForm
from panel.equipments.models import Equipment

from panel.contacts.models import Contact
from panel.contacts.forms import ContactForm

from panel.routes.models import Route

from cities.models import City

from django.forms import formset_factory
from django.forms.models import model_to_dict
from django.db.models import Sum

from django.contrib import messages
from panel.core.utils import user_logs, delete_record, delete_item, sendmail
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse

from panel.core.decorators import has_permissions
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page
# Create your views here.

app_name = __package__.rsplit('.')[1]
@login_required
@has_permissions('can_view_' + app_name)
# @cache_page(60 * 15)
def carrier_list(request):
	context = {}

	context['filter'] = request.GET.get('filter')
	context['app_name'] = app_name
	context['permissions'] = request.user.get_user_permissions(request.user)
	context['carriers_list'] = Carriers.objects.filter(status=True).order_by('alias')

	return render(request, 'carriers/carrier_list.html', context)

@login_required
@has_permissions('can_add_' + app_name)
def carrier_add(request):
	context = {}
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'
	context["prefix_operative"] = "operative_contact"
	context["prefix_administrative"] = "administrative"

	context['prefix_routes'] = 'routes'
	context['prefix_equipment'] = 'equipment'

	equipment_formset = formset_factory(EquipmentForm)
	routes_formset = formset_factory(InterestRoutesForm)

	if request.method == 'POST':
		context['carrier_form'] = CarrierForm(request.POST,request.FILES)
		context['equipment_form'] = equipment_formset(request.POST, prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(request.POST, prefix=context['prefix_routes'])
		context['admin_contact_form'] = ContactForm(request.POST, prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(request.POST, prefix=context["prefix_operative"])

		if context['carrier_form'].is_valid() and context['admin_contact_form'].is_valid() and context['operative_contact_form'].is_valid() and context['routes_form'].is_valid() and context['equipment_form'].is_valid():
			carrier_obj = context['carrier_form'].save(commit=False)

			admin_contact = context['admin_contact_form'].save(commit=False)
			admin_contact.contact_type = 'administrative'
			admin_contact.save()
			
			operative_contact = context['operative_contact_form'].save(commit=False)
			operative_contact.id = None
			operative_contact.contact_type = 'operative'
			operative_contact.save()

			carrier_obj.administrative_contact = admin_contact
			carrier_obj.operative_contact = operative_contact
			carrier_obj.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.carrier = carrier_obj
					equipment_obj.save()
			
			for route in context['routes_form']:
				if route.has_changed():
					route_obj = route.save(commit=False)
					route_obj.carrier = carrier_obj
					route_obj.save()

			#-- Message to beneficiary
			msg = messages.success(request, 'Transportista creado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Transportista creado satisfactorimente')

			return redirect('carriers:carrier_list')
	else:
		context['carrier_form'] = CarrierForm()
		context['equipment_form'] = equipment_formset(prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(prefix=context['prefix_routes'])

		context['admin_contact_form'] = ContactForm(prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(prefix=context["prefix_operative"])
	
	return render(request, 'carriers/carrier_form.html', context)

@login_required
@has_permissions('can_edit_' + app_name)
def carrier_edit(request, pk):
	context = {}
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'

	context["prefix_operative"] = "operative_contact"
	context["prefix_administrative"] = "administrative"

	context['carrier_obj'] = get_object_or_404(Carriers, pk=pk)
	context['admin_contact_obj'] = get_object_or_404(Contact, pk=context['carrier_obj'].administrative_contact.pk)
	context['operative_contact_obj'] = get_object_or_404(Contact, pk=context['carrier_obj'].operative_contact.pk)
	context['equipments'] = []
	equipments = CarrierEquipment.objects.filter(carrier_id=pk).values('equipment').annotate(quantity=Sum('quantity'))
	for item in equipments:
		dic = {}
		dic['equipment'] = get_object_or_404(CarrierEquipment, carrier_id=pk, equipment_id=item['equipment'])
		dic['quantity'] = item['quantity']
		context['equipments'].append(dic)
	
	context['interest_routes'] = InterestRoutes.objects.filter(carrier_id=pk)
	context['prefix_routes'] = 'routes'
	context['prefix_equipment'] = 'equipment'

	equipment_formset = formset_factory(EquipmentForm, can_delete=True)
	routes_formset = formset_factory(InterestRoutesForm)
	
	if request.method == 'POST':
		context['carrier_form'] = CarrierForm(request.POST,request.FILES, instance=context['carrier_obj'])
		context['admin_contact_form'] = ContactForm(request.POST, instance=context['admin_contact_obj'], prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(request.POST, instance=context['operative_contact_obj'], prefix=context["prefix_operative"])
		context['equipment_form'] = equipment_formset(request.POST, prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(request.POST, prefix=context['prefix_routes'])

		if context['carrier_form'].is_valid() and context['admin_contact_form'].is_valid() and context['operative_contact_form'].is_valid() and context['routes_form'].is_valid() and context['equipment_form'].is_valid():
			carrier_obj = context['carrier_form'].save(commit=False)
			
			admin_contact = context['admin_contact_form'].save(commit=False)
			admin_contact.contact_type = 'administrative'
			admin_contact.save()
			
			operative_contact = context['operative_contact_form'].save(commit=False)
			operative_contact.id = None
			operative_contact.contact_type = 'operative'
			operative_contact.save()

			for equipment in context['equipment_form']:
				if equipment.has_changed():
					equipment_obj = equipment.save(commit=False)
					equipment_obj.carrier = carrier_obj

					already_saved = False
					for item in context['equipments']:
						if equipment_obj.name == item['equipment'].name:
							already_saved = True

					if already_saved:
						msg = messages.warning(request, 'El equipo ' + equipment_obj.equipment.name + ' ya estaba registrado')
					else:
						dicc = {'equipment': equipment_obj.name, 'quantity': equipment_obj.quantity}
						context['equipments'].append(dicc)
						equipment_obj.save()

			for route in context['routes_form']:
				if route.has_changed():
					if route.is_valid():
						route_obj = route.save(commit=False)
						route_obj.carrier = carrier_obj
						route_obj.save()

			carrier_obj.administrative_contact = admin_contact
			carrier_obj.operative_contact = operative_contact
			carrier_obj.save()

			msg = messages.success(request, 'Transportista modificado satisfactorimente')

			#-- User Logs (Info, Access, Error)
			user_logs(request,None,'I','Transportista modificado satisfactorimente')

			return redirect('carriers:carrier_list')
	else:
		context['equipment_form'] = equipment_formset(prefix=context['prefix_equipment'])
		context['routes_form'] = routes_formset(prefix=context['prefix_routes'])

		context['carrier_form'] = CarrierForm(instance=context['carrier_obj'])
		context['admin_contact_form'] = ContactForm(instance=context['admin_contact_obj'], prefix=context["prefix_administrative"])
		context['operative_contact_form'] = ContactForm(instance=context['operative_contact_obj'], prefix=context["prefix_operative"])
	
	return render(request, 'carriers/carrier_form.html', context)

@login_required
def carrier_detail(request, pk):
	context = {}
	context['carrier_obj'] = get_object_or_404(Carriers, pk=pk)
	context['map_token'] = 'pk.eyJ1IjoiYmVycmV5b2wiLCJhIjoiY2s4cThsNGN0MDA5ZDNmcGgzOXY1NDBkeCJ9.1_0TqZta11NjNNfGgtJwWA'
	context['equipments'] = CarrierEquipment.objects.filter(carrier_id=pk).values('equipment__name').annotate(quantity=Sum('quantity'))
	context['documents_list'] = carrier_documents(pk)
	context['interest_routes'] = InterestRoutes.objects.filter(carrier_id=pk)
	data = dict()
	context['mail_form'] = MailForm()
	data['html_form'] = render_to_string('carriers/carrier_detail.html', context, request=request)
	
	return JsonResponse(data)

@login_required
@has_permissions('can_delete_' + app_name)
def carrier_delete(request, pk):
	carrier = get_object_or_404(Carriers, pk=pk)

	data = delete_record(request, carrier, '/panel/carriers/', 'carriers:carrier_delete')
	return JsonResponse(data)

@login_required
def carrier_sendmail(request, pk, type):
	context = {}
	data = dict()

	carrier = get_object_or_404(Carriers, pk=pk)

	context["from_mail"] = request.user.email
	if type=="operative":
		context["to_mail"] = carrier.operative_contact.email
	elif type=="administrative":
		context["to_mail"] = carrier.administrative_contact.email

	if request.is_ajax and request.method == 'POST':

		context['mail_form'] = MailForm(request.POST)

		if context['mail_form'].is_valid():
			mail_obj = context['mail_form'].save(commit=False)

			if carrier.user:
				mail_obj.user = carrier.user
			
			sended = sendmail(mail_obj.subject, mail_obj.message, context["from_mail"], context["to_mail"])

			if sended:
				mail_obj.save()

				messages.success(request, 'Mensaje enviado satisfactoriamente')
				user_logs(request, None, 'I', 'Mensaje enviado satisfactoriamente')
			else:
				messages.success(request, 'Mensaje no enviado')


			data['form_is_valid'] = True

			data['url_redirect'] = '/panel/carriers/'
		else:
			data['form_is_valid'] = False

			
	else:
		context['mail_form'] = MailForm()

	data['html_form'] = render_to_string('core/snippets/modal_mail.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
def carrier_routes(request, pk, source=None, target=None):
	context = {}
	data = dict()

	if source and target:
		routes = Route.objects.filter(carrier_id=pk, source_id=source, target_id=target).order_by('-date')
	else:
		routes = Route.objects.filter(carrier_id=pk).order_by('-date')
	context['routes_list'] = routes

	data['html_form'] = render_to_string('carriers/carrier_routes.html', context, request=request)
	
	return JsonResponse(data) 

@login_required
def carrier_equipment_delete(request, pk):
	equipment_obj = CarrierEquipment.objects.get(pk=pk)
	url_redirect = '/panel/carriers/edit/' + str(equipment_obj.carrier.id)
	data = delete_item(request, equipment_obj, 'carriers:carrier_equipment_delete','El equio del transportista fue eliminado', url_redirect)
	return JsonResponse(data)

@login_required
def carrier_route_delete(request, pk):
	obj = InterestRoutes.objects.get(pk=pk)
	url_redirect = '/panel/carriers/edit/' + str(obj.carrier.id)
	data = delete_item(request, obj, 'carriers:carrier_route_delete','La ruta del transportista fue eliminado', url_redirect)
	return JsonResponse(data)

def carrier_documents(pk):
	context = {}

	carrier_obj = get_object_or_404(Carriers, pk=pk)

	documents = []

	if carrier_obj.person_type == 'legal':
		if carrier_obj.constitutive_act:
			documents.append({ 'title': 'Acta constitutiva', 'file': carrier_obj.constitutive_act, 'is_pdf': carrier_obj.constitutive_act.url.endswith('.pdf') })
		if carrier_obj.power_attorney:
			documents.append({ 'title': 'Poder de los representantes', 'file': carrier_obj.power_attorney, 'is_pdf': carrier_obj.power_attorney.url.endswith('.pdf') })
		if carrier_obj.payment_authorization:
			documents.append({ 'title': 'Autorizacion de pago', 'file': carrier_obj.payment_authorization, 'is_pdf': carrier_obj.payment_authorization.url.endswith('.pdf') }) 

	if carrier_obj.tax_number_doc:
		documents.append({ 'title':'RFC', 'file': carrier_obj.tax_number_doc, 'is_pdf': carrier_obj.tax_number_doc.url.endswith('.pdf')	 })
	if carrier_obj.oficial_id:
		documents.append({ 'title':'Identificacion Oficial', 'file': carrier_obj.oficial_id, 'is_pdf': carrier_obj.oficial_id.url.endswith('.pdf') })
	if carrier_obj.transport_allowance:
		documents.append({ 'title': 'Permiso de transporte',	'file': carrier_obj.transport_allowance, 'is_pdf': carrier_obj.transport_allowance.url.endswith('.pdf') })
	if carrier_obj.proof_address:
		documents.append({ 'title': 'Comprobante de domicilio', 'file': carrier_obj.proof_address, 'is_pdf': carrier_obj.proof_address.url.endswith('.pdf') })
	if carrier_obj.e_billin_demo:
		documents.append({ 'title': 'Demo de factura electronica',	'file': carrier_obj.e_billin_demo, 'is_pdf': carrier_obj.e_billin_demo.url.endswith('.pdf') })
	if carrier_obj.bank_statement:
		documents.append({ 'title': 'Estado de cuenta', 'file': carrier_obj.bank_statement, 'is_pdf': carrier_obj.bank_statement.url.endswith('.pdf') })
	if carrier_obj.unity_list:
		documents.append({ 'title': 'Relacion de unidades',	'file': carrier_obj.unity_list, 'is_pdf': carrier_obj.unity_list.url.endswith('.pdf') })
	if carrier_obj.contract:
		documents.append({ 'title': 'Contrato', 'file': carrier_obj.contract, 'is_pdf': carrier_obj.contract.url.endswith('.pdf') })
	
	#-- Documentos para asignar viajes
	if carrier_obj.driving_license:
		documents.append({ 'title': 'Licencia de conducir', 'file': carrier_obj.driving_license, 'is_pdf': carrier_obj.driving_license.url.endswith('.pdf') })
	if carrier_obj.insurance:
		documents.append({ 'title': 'Seguro de la unidad', 'file': carrier_obj.insurance, 'is_pdf': carrier_obj.insurance.url.endswith('.pdf') })
	if carrier_obj.registration:
		documents.append({ 'title': 'Tarjeta de circulacion', 'file': carrier_obj.registration, 'is_pdf': carrier_obj.registration.url.endswith('.pdf') })
	if carrier_obj.gps:
		documents.append({ 'title': 'GPS', 'file': carrier_obj.gps, 'is_pdf': carrier_obj.gps.url.endswith('.pdf') })
	if carrier_obj.mirror_account:
		documents.append({ 'title': 'Cuenta espejo', 'file': carrier_obj.mirror_account, 'is_pdf': carrier_obj.mirror_account.url.endswith('.pdf') })

	return documents
	
