from django.db import models

# Create your models here.
from django.db import models

ZONES = {
	'southeast': {
        'regions': ['OA', 'VE', 'CP', 'TB', 'CM', 'QR', 'YU'],
        'color': '#FFCDD2',
        'mouse_over_color': '#EF9A9A'
    },
	'north': {
        'regions': ['TM', 'NL', 'CA', 'CH'],
        'color': '#FFE0B2',
        'mouse_over_color': '#FFB74D'
    },
	'center': {
        'regions': ['DU', 'ZA', 'SL', 'GJ', 'AG', 'QE', 'HI', 'TL', 'MX', 'DF', 'MR', 'PU'],
        'color': '#C8E6C9',
        'mouse_over_color': '#81C784'
    },
	'pacific': {
        'regions': ['BS', 'BN', 'SO', 'SI'],
        'color': '#BBDEFB',
        'mouse_over_color': '#64B5F6'
    },
    'occident': {
        'regions': ['NA', 'JA', 'CL', 'MC', 'GR'],
        'color': '#EEA4FA',
        'mouse_over_color': '#DC6BFA' 
    },
}
# from cities.models import BaseRegion


# class CustomRegionModel(BaseRegion, models.Model):
#     code_name = models.CharField(verbose_name="Codigo", max_length=2)

#     class Meta(BaseRegion.Meta):
#         pass

#     def __str__(self):
#         return self.name + ', ' + self.region.name 