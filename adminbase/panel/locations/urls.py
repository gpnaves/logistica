from django.conf.urls import url
from panel.locations.views import locations

urlpatterns = [
    url(r'^get_cities_info/$', locations.get_cities_info, name='get_cities_info'),
    url(r'^cities_per_region/$', locations.city_per_region, name='city_per_region'),
    url(r'^get_region_name/$', locations.get_region_name, name='get_region_name'),
]