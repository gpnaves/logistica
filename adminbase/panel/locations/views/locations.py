from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.core import serializers

from cities.models import PostalCode, Subregion, Country, Region, City
# from panel.locations.models import CustomCityModel

# Create your views here.


def get_cities_info(request):
	if(request.is_ajax and request.method == 'GET'):       
		cities_info = {}
		postal_code = PostalCode.objects.filter(code=request.GET.get('postal_code'), country_id=3996063)[0]
		if postal_code:
			region = ''
			subregion = ''
			if postal_code.region_id:
				region = Region.objects.get(pk=postal_code.region_id)
			else:
				region = Region.objects.get(name=postal_code.region_name)
			if postal_code.subregion_id:
				subregion = Subregion.objects.get(pk=postal_code.subregion_id)
			if postal_code.subregion_name:
				subregion = Subregion()
				subregion.name = postal_code.subregion_name
			else:
				subregion = Subregion.objects.filter(name=postal_code.subregion_name)[0]
			country = Country.objects.get(id=region.country_id)

			cities_info['city'] = postal_code.district_name
			cities_info['region'] = region.name
			cities_info['subregion'] = subregion.name
			cities_info['country'] = country.name
			cities_info['success'] = True

		return JsonResponse(cities_info)

def get_region_name(request):
	if(request.is_ajax and request.method == 'GET'):
		json_data = {}
		region = Region.objects.filter(pk=request.GET.get('pk'))
		json_data['region'] = serializers.serialize("json", region)
		return JsonResponse(json_data,safe=False)

def city_per_region(request):
	if(request.is_ajax and request.method == 'GET'):
		
		json_data = {}
		cities = City.objects.filter(region_id=request.GET.get('region_id'), country_id=3996063).order_by('name')
		json_data['cities'] = serializers.serialize("json", cities)
		
		return JsonResponse(json_data,safe=False)

	