from .settings import *

DEBUG=False

STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), 'staticfiles')
COMPRESS_ROOT = STATIC_ROOT