"""adminbase URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from django.conf.urls import url, include
from django.views.generic import TemplateView, RedirectView
from django.urls import reverse_lazy

urlpatterns = [
    url(r'^$',RedirectView.as_view(url=reverse_lazy('authorization:login'))),
    url(r'^admin/', admin.site.urls),
    #-- Login/Dashboard
    url(r'^panel/', include(('panel.login.urls','login'), namespace='authorization')),
    #-- Config
    url(r'^panel/config/', include(('panel.config.urls','config'), namespace='config')),
    #-- Usuarios
    url(r'^panel/accounts/', include(('panel.accounts.urls','accounts'), namespace='usuarios')),
    #-- Transportistas
    url(r'^panel/carriers/', include(('panel.carriers.urls','carriers'), namespace='transportista')),
    #-- Personal
    url(r'^panel/profile/', include(('panel.profile.urls','personal'), namespace='perfil')),
    #-- Personal
    url(r'^panel/routes/', include(('panel.routes.urls','routes'), namespace='rutas')),
    #-- Ubicaciones
    url(r'^panel/locations/', include(('panel.locations.urls','locations'), namespace='ubicacion')),
    #-- Clientes
    url(r'^panel/customers/', include(('panel.customers.urls','customers'), namespace='clientes')),
    #-- Mercancias
    url(r'^panel/commodities/', include(('panel.commodities.urls','commodities'), namespace='mercancias')),
    #-- Equipos
    url(r'^panel/equipments/', include(('panel.equipments.urls','equipments'), namespace='equipos')),
    #-- Posicion - Empleados
    url(r'^panel/position/', include(('panel.position.urls','position'), namespace='Posicion')),
    #-- Empleado - Empleados
    url(r'^panel/employees/', include(('panel.employees.urls','employees'), namespace='Empleados')),
    #-- Fletes
    url(r'^panel/freight/', include(('panel.freight.urls','freight'), namespace='Fletes')),
    #-- Matchs
    url(r'^panel/matchs/', include(('panel.matchs.urls','matchs'), namespace='Matchs')),
    #-- Prospects
    url(r'^panel/prospects/', include(('panel.prospects.urls','prospects'), namespace='Prospectos')),
    #-- Reortes
    url(r'^panel/reports/', include(('panel.reports.urls','reports'), namespace='Reportes')),

    #-- Recovery Password
    url(r'^recovery/', include('django.contrib.auth.urls')),
    #-- Summernote texteditor
    url(r'^summernote/', include('django_summernote.urls')),
    #-- Django Select2
    url(r'^select2/', include('django_select2.urls')),

]
if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

